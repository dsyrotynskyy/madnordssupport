﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    class MerchantDAO : TemplateDAO<Merchant>
    {
        private static MerchantDAO instance;
        private MerchantDAO() { Prepare(); }

        private void Prepare()
        {
        }

        public static MerchantDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MerchantDAO();
                }

                return instance;
            }
        }

        public override string DataSourceName
        {
            get
            {
                return MadNordsSettings.Default.MerchantFileName;
            }

            set
            {
                MadNordsSettings.Default.MerchantFileName = value;
                MadNordsSettings.Default.Save();
            }
        }

        public Merchant GetCreateMerchantInstance (string id)
        {
            if (allData.ContainsKey (id))
            {
                return allData[id];
            }

            Merchant m = CreateNew();
            m.Id = id;
            allData.Add(id, m);
            return m;
        }

        public override Merchant CreateNew()
        {
            Merchant m = new Merchant();
            return m;
        }

        protected override bool ReadDataFromXml(XDocument doc)
        {
            var query = from c in doc.Root.Descendants("merchant")
                        select c;

            if (query == null)
            {
                return false;
            }

            allData.Clear();
            foreach (XElement q in query)
            {
                ReadMerchant (q);
            }

            return true;
        }

        private void ReadMerchant (XElement x)
        {
            string mid = x.TryAttributeValue("id");

            if (string.IsNullOrEmpty (mid))
            {
                return;
            }

            Merchant m = GetCreateMerchantInstance(mid);
            m._answer = x.TryElementValue("answer");

            XElement wares = x.Element("wares");
            var wareQuery = from c in wares.Descendants("ware")
                        select c;
            foreach (XElement ware in wareQuery)
            {
                try
                {
                    int id = int.Parse(ware.TryAttributeValue("id"));
                    string item = ware.TryAttributeValue("item");

                    if (string.IsNullOrEmpty(item))
                    {
                        continue;
                    }

                    try
                    {
                        m.AddUpdateWare(id, item);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                    
                } catch (Exception e)
                {
                    Console.WriteLine(e.ToString ());
                }
            }
        }

        public override void Delete(string id)
        {
            if (currentXDocument == null)
            {
                return;
            }

            if (allData.ContainsKey(id))
            {
                XElement desE = currentXDocument.Element("merchants").Elements("merchant")
                       .Where(e => (string)e.Attribute("id") == id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
                    desE.Remove();
                }

                allData.Remove(id);
            }
        }

        public override bool AddUpdate(Merchant m)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            XElement merchantElement = null;
            if (allData.ContainsKey(m.Id))
            {
                XElement desE = currentXDocument.Element("merchants").Elements("merchant")
                       .Where(e => (string)e.Attribute("id") == m.Id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
 //                   desE.Remove();
                    merchantElement = desE;
                }
            }
            if (merchantElement == null)
            {
                merchantElement = new XElement("merchant");
                currentXDocument.Element("merchants").Add(merchantElement);
            }
            merchantElement.Add(new XAttribute("id", m.Id));
            merchantElement.CreateChildBody("answer", m._answer);

            XElement waresParent = merchantElement.CreateChild("wares");
            foreach (int wareId in m.wares.Keys)
            {
                XElement ware = waresParent.CreateChild("ware");
                ware.CreateAttributeValue("id", wareId + "");
                ware.CreateAttributeValue("item", m.wares[wareId] + "");
            }

            SaveDataInDictionary(m);

            XMLDao.SaveDocument(currentXDocument, DataSourceName);

            return true;
        }
    }

    class Merchant : TemplateItem
    {
        public string _answer;

        public Dictionary<int, Ware> wares = new Dictionary<int, Ware> ();

        public void AddUpdateWare(int id, string wareToString)
        {
            if (wares.ContainsKey(id))
            {
                wares[id] = Ware.FromString (wareToString);
            }
            else
            {
                wares.Add(id, Ware.FromString(wareToString));
            }
        }

        public void AddUpdateWare(int id, string wareType, int item)
        {
            if (wares.ContainsKey(id))
            {
                wares[id] = new Ware (wareType, item);
            }
            else
            {
                wares.Add(id, new Ware(wareType, item));
            }
        }

        public struct Ware
        {
            private string _type;
            private int _item;

            public string Type
            {
                get
                {
                    return _type;
                }

                private set
                {
                    _type = value;
                }
            }

            public int Item
            {
                get
                {
                    return _item;
                }

                private set
                {
                    _item = value;
                }
            }

            public bool IsEmpty
            {
                get
                {
                    return string.IsNullOrEmpty(_type) || _item < 0;
                }
            }

            public Ware(string type, int item)
            {
                _type = type;
                _item = item;
            }

            public static string GetWareType(string ware)
            {
                string[] parts = ware.Split('|');
                if (parts.Length >= 2)
                {
                    return parts[0];
                }
                return null;
            }

            public static string GetWareItem(string ware)
            {
                string[] parts = ware.Split('|');
                if (parts.Length >= 2)
                {
                    return parts[1];
                }
                return null;
            }

            public override string ToString ()
            {
                return MakeWareString(Type, Item);
            }

            public static Ware FromString (string wareString)
            {
                string item = GetWareItem(wareString);
                string type = GetWareType(wareString);
                int itemId = int.Parse(item);                

                return new Ware(type, itemId);
            }

            public static string MakeWareString(string type, int itemId)
            {
                return type + "|" + itemId + "|";
            }
        }
    }
}
