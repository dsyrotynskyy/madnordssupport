﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    class QuestDAO
    {
        private static QuestDAO instance;
        private QuestDAO() { }

        public static QuestDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new QuestDAO();
                }
                return instance;
            }
        }

        public Dictionary<string, Quest> allQuests = new Dictionary<string, Quest>();
        public string targetFileName
        {
            get
            {
                return MadNordsSettings.Default.QuestFileName;
            }
        }

        XDocument currentXDocument;

        private bool ReadFromFile(string file)
        {
            var xml = XDocument.Load(file);
            if (xml == null)
            {
                return false;
            }
            var query = from c in xml.Root.Descendants("quest")
                        select c;

            if (query == null)
            {
                return false;
            }

            allQuests.Clear();
            foreach (XElement q in query)
            {
                CreateQuest(q);
            }

            this.currentXDocument = xml;

            return true;
        }

        void CreateQuest(XElement q)
        {
            Quest newQuest = new Quest();
            newQuest.id = q.Attribute("id").Value + "";

            if (newQuest.id == "")
            {
                return;
            }

            if (q.Element("name") != null)
            {
                newQuest.name = q.Element("name").Value + "";
            }

            XElement xp = q.Element("xp");
            if (xp != null)
            {
                newQuest.XPValue = int.Parse (xp.Value);
            }          

            newQuest.type = q.Parent.Attribute("id").Value;

            XElement descriptions = q.Element("descriptions");
            if (descriptions != null)
            {
                foreach (XElement descr in q.Element("descriptions").Elements("description"))
                {
                    if (descr.Attribute("id") == null)
                    {
                        continue;
                    }
                    string id = descr.Attribute("id").Value + "";
                    string val = descr.Value;
                    string[] vals = new string[2] { id, val };
                    newQuest.descriptions.Add(vals);
                }
            }

            XElement markers = q.Element("markers");
            if (markers != null)
            {
                foreach (XElement marker in markers.Elements("marker"))
                {
                    if (marker.Attribute("id") == null)
                    {
                        continue;
                    }
                    string id = marker.Attribute("id").Value + "";
                    string body = null;
                    body = marker.Value;
                    XAttribute create = marker.Attribute("create");
                    if (create != null && create.Value == "yes")
                    {
                        newQuest.marks.Add(new string[] { id, body, "true" });
                    }
                    else
                    {
                        newQuest.marks.Add(new string[] { id, body });
                    }
                }
            }

            if (allQuests.ContainsKey(newQuest.id)) { allQuests[newQuest.id] = newQuest; }
            else { allQuests.Add(newQuest.id, newQuest); }
        }

        internal bool TryReadFrom(string file)
        {
            bool res = ReadFromFile(file);
            if (res)
            {
                MadNordsSettings.Default.QuestFileName = file;
                MadNordsSettings.Default.Save();
            }
            return res;
        }

        internal bool AddUpdateQuest(Quest q)
        {
            XDocument xml = currentXDocument;
            if (xml == null)
            {
                return false;
            }

            XElement questElement = null;
            if (allQuests.ContainsKey(q.id))
            {
                foreach (XElement type in xml.Element("quests").Elements("type"))
                {
                    XElement desE = type.Elements("quest")
                       .Where(e => (string)e.Attribute("id") == q.id)
                       .FirstOrDefault();

                    if (desE != null)
                    {
                        desE.RemoveAll();
                        questElement = desE;
                        break;
                    }
                }
            }
            if (questElement == null)
            {
                questElement = new XElement("quest");
                xml.Element("quests").Elements("type")
                    .First(c => c.Attribute("id").Value == q.type).Add
                    (
                        questElement
                    );
            }

            XElement rewardElement = new XElement("xp", q.XPValue
                );

            XElement marks = new XElement("markers");
            XElement descriptions = new XElement("descriptions");

            questElement.Add(new XAttribute("id", q.id),
                new XElement("name", q.name),
                rewardElement, marks, descriptions
            );

            q.marks.ForEach(s =>
            {
                if (s.Length > 2)
                {
                    XElement mark = new XElement(
                        new XElement("marker"
                            , new XAttribute("id", s[0])
                            , new XAttribute("create", "yes")
                            , s[1]
                        )
                    );

                    marks.Add(mark);
                }
                else
                {
                    marks.Add(
                        new XElement("marker", new XAttribute("id", s[0]), s[1])
                    );
                }
            });

            q.descriptions.ForEach(s => descriptions.Add(
                new XElement("description", new XAttribute("id", s[0]), s[1])
            ));

            XMLDao.SaveDocument(xml, targetFileName);

            if (allQuests.ContainsKey(q.id)) { allQuests[q.id] = q; }
            else { allQuests.Add(q.id, q); }

            return true;
        }

        internal void DeleteQuest(string id)
        {
            allQuests.Remove(id);

            foreach (XElement type in currentXDocument.Element("quests").Elements("type"))
            {
                XElement desE = type.Elements("quest")
                   .Where(e => (string)e.Attribute("id") == id)
                   .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
                    desE.Remove();
                    break;
                }
            }

            XMLDao.SaveDocument(currentXDocument, targetFileName);
        }
    }

    public class Quest
    {
        public string id = "";
        public string name = "";
        public int XPValue = 1;
   
        public List<string[]> marks = new List<string[]>();
        public List<string[]> descriptions = new List<string[]>();

        public string type = "";
    }
}
