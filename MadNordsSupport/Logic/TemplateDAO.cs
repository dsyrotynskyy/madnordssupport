﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    public abstract class TemplateDAO<T> where T : TemplateItem
    {
        public Dictionary<string, T> allData = new Dictionary<string, T>();

        public bool TryReadFrom(string file)
        {
            bool res = false;
            XDocument xml = null;
            try
            {
                xml = XDocument.Load(file);
            } catch (Exception e)
            {

            }
            
            if (xml == null)
            {
                return false;
            }

            if (ReadDataFromXml(xml))
            {
                this.currentXDocument = xml;
                DataSourceName = file;
                res = true;
            } else
            {
                Console.Error.WriteLine("Error reading: " + file );
            }

            return res;
        }

        public abstract string DataSourceName{ get; set; }

        protected XDocument currentXDocument;

        protected abstract bool ReadDataFromXml(XDocument doc);
        public abstract T CreateNew();

        public abstract void Delete(string text);
        public abstract bool AddUpdate(T d);

        protected void SaveDataInDictionary (T d)
        {
            if (allData.ContainsKey(d.Id))
            {
                allData[d.Id] = d;
            }
            else
            {
                allData.Add(d.Id, d);
            }
        }

        public T TryGet(string id)
        {
            if (allData.ContainsKey(id))
            {
                return allData[id];
            }
            return null;
        }
    }

    public static class XMLDao
    {
        public static void SaveDocument(XDocument document, string filePath)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";

            using (XmlWriter writer = XmlTextWriter.Create(filePath, settings))
            {
                document.Save(writer);
            }
        }
    }

    public abstract class TemplateItem
    {
        public static readonly string UNDEFINED_ID = "UNDEFINED_ID";

        private string id = UNDEFINED_ID;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}
