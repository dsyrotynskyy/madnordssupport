﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    public class LocationDAO :TemplateDAO<Location>
    {
        private static LocationDAO instance;
        private LocationDAO() { }

        public static LocationDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LocationDAO();
                }
                return instance;
            }
        }

        public override string DataSourceName
        {
            get
            {
                return MadNordsSettings.Default.LocationFileName;
            }

            set
            {
                MadNordsSettings.Default.LocationFileName = value;
                MadNordsSettings.Default.Save();
            }
        }

        public override Location CreateNew()
        {
            return new Location();
        }

        protected override bool ReadDataFromXml(XDocument doc)
        {
            var query = from c in doc.Root.Descendants("location")
                        select c;

            if (query == null)
            {
                return false;
            }

            allData.Clear();
            foreach (XElement q in query)
            {
                ReadLocation(q);
            }

            return true;
        }

        private void ReadLocation(XElement x)
        {
            Location l = new Location();
            l.Id = x.Attribute("id").Value + "";
            l.name = x.Attribute("name").Value + "";
            l.location = x.Attribute("location").Value + "";
            float.TryParse(x.Attribute("X").Value, out l.x);
            float.TryParse(x.Attribute("Y").Value, out l.y);

            SaveDataInDictionary(l);
        }

        public override bool AddUpdate(Location d)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            XElement xdialogElement = null;
            if (allData.ContainsKey(d.Id))
            {
                XElement desE = currentXDocument.Element("locations").Elements("location")
                       .Where(e => (string)e.Attribute("id") == d.Id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
 //                   desE.Remove();
                    xdialogElement = desE;
                }
            }
            if (xdialogElement == null)
            {
                xdialogElement = new XElement("location");
                currentXDocument.Element("locations").Add(xdialogElement);
            }
            xdialogElement.Add(new XAttribute("id", d.Id));
            xdialogElement.Add(new XAttribute("name", d.name));
            xdialogElement.Add(new XAttribute("location", d.location));
            xdialogElement.Add(new XAttribute("X", d.x));
            xdialogElement.Add(new XAttribute("Y", d.y));

            SaveDataInDictionary(d);

            XMLDao.SaveDocument(currentXDocument, DataSourceName);

            return true;
        }

        public override void Delete(string id)
        {
            allData.Remove(id);

            XElement desE = currentXDocument.Element("locations").Elements("location")
                   .Where(e => (string)e.Attribute("id") == id)
                   .FirstOrDefault();

            if (desE != null)
            {
                desE.RemoveAll();
                desE.Remove();
            }

            XMLDao.SaveDocument(currentXDocument, DataSourceName);
        }
    }

    public class Location : TemplateItem
    {
        public Location ()
        {
            location = "";
            x = 0f;
            y = 0f;
        }

        public string location;
        public float x;
        public float y;
        public string name;
    }     
}
