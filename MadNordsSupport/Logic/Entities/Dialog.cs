﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadNordsSupport.Logic.Entities
{
    class Dialog
    {
        public string id;
        public string name;
        public readonly List<Tier> tiers = new List<Tier>();

        public bool HasTier(string id)
        {
            foreach (Tier t in tiers)
            {
                if (t.id == id)
                {
                    return true;
                }
            }

            return false;
        }

        public Tier GetTier(string id)
        {
            foreach (Tier t in tiers)
            {
                if (t.id == id)
                {
                    return t;
                }
            }

            return null;
        }

        public void RemoveTier(string id)
        {
            Tier t = GetTier(id);
            if (t != null)
                this.tiers.Remove(t);
        }

        internal void CopyFrom(Dialog copyDialog)
        {
            this.name = copyDialog.name;
            this.tiers.Clear();
            foreach (Tier t in copyDialog.tiers)
            {
                Tier newT = t.Clone(this);
                newT.id = t.id;
                this.tiers.Add(newT);
            }
        }

        internal Tier AddNewTier(string p, int index)
        {
            Tier t = null;
            {
                t = Tier.CreateEmpty(p, this);
            }
            if (this.tiers.Count < index)
            {
                this.tiers.Add(t);
            }
            else
            {
                this.tiers.Insert(index, t);
            }
            return t;
        }

        public bool CheckErrors
        {
            get
            {
                if (this.tiers.Count <= 0)
                {
                    return false;
                }

                foreach (Tier t in this.tiers)
                {
                    if (t.CheckErrors)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public class Tier
        {
            public string id = "";
            public string comment = "";
            public string NPCText = "";
            public readonly Answer[] answers = new Answer[3];
            Dialog owner;

            public Dialog Owner
            {
                get
                {
                    return owner;
                }
            }

            Tier(Dialog d)
            {
                this.owner = d;
                this.answers[0] = new Answer(this);
                this.answers[1] = new Answer(this);
                this.answers[2] = new Answer(this);
            }

            public static Tier CreateStub(string tid, Dialog d)
            {
                Tier t = new Tier(d);
                t.id = tid;
                return t;
            }

            internal static Tier CreateEmpty(string tid, Dialog d)
            {
                Tier t = new Tier(d);
                t.id = tid;
                return t;
            }

            internal Tier Clone(Dialog newOwner)
            {
                Tier t = new Tier(newOwner);
                t.id = this.id;

                CopyToFrom(t, this);

                return t;
            }

            static internal void CopyToFrom(Tier t, Tier source)
            {
                t.comment = source.comment;
                t.NPCText = source.NPCText;
                for (int i = 0; i < source.answers.Length; i++)
                {
                    Answer a = source.answers[i];
                    if (a != null)
                    {
                        Answer na = a.Clone(t);
                        t.answers[i] = na;
                    }
                }
            }

            internal void CopyFrom(Tier source)
            {
                CopyToFrom(this, source);
            }
          
            public string CheckForErrors ()
            {
                return null;
            }

            public bool CheckErrors
            {
                get
                {
                    if (string.IsNullOrEmpty(NPCText))
                    {
                        return true;
                    }

                    if (!this.answers[0].ValidnessState (false).HasError)
                    {
                        return false;
                    }

                    return true;
                }
            }

            public string TierBugsMessage
            {
                get
                {
                    string res = "";

                    if (string.IsNullOrEmpty (NPCText))
                    {
                        return "Empty NPC text";
                    }

                    res = answers[0].ValidnessState (true).BugsMessage;
                    if (res == null)
                    {
                        if (!answers[1].IsEmptyAnswer)
                        {
                            res = answers[1].ValidnessState (true).BugsMessage;

                            if (res == null)
                            {
                                if (!answers[2].IsEmptyAnswer)
                                {
                                    res = answers[2].ValidnessState (true).BugsMessage;
                                }
                            }
                        }
                    }
                    return res;
                }

            }

            public class Answer
            {
                private string _text;
                public string text
                {
                    set
                    {
                        _text = value;
                    }
                    get
                    {
                        return _text;
                    }
                }
                private Tier owner;

                public Tier Owner
                {
                    get
                    {
                        return this.owner;
                    }
                }

                public Answer(Tier newOwner)
                {
                    this.owner = newOwner;
                    rewardItems = new RewardItem[]
                    {
                        new RewardItem(this, 0),
                        new RewardItem(this, 1),
                        new RewardItem(this, 2),
                        new RewardItem(this, 3)
                    };
                }

                internal Answer Clone(Tier newOwner)
                {
                    Answer a = new Answer(newOwner);
                    a.text = this.text;
                    a.CopyAttributesFrom(this);
                    return a;
                }

                public AnswerValidnessState ValidnessState (bool checkWarnings)
                {
                    if (IsEmptyAnswer)
                    {
                        return AnswerValidnessState.CreateError(AnswerName, "Empty answer");
                    }

                    return VerifyCorrectActionOrCheck(checkWarnings); 
                }
                int _id = -1;
                int Id
                {
                    get
                    {
                        if (_id == -1)
                        {
                            for (int i = 0; i < owner.answers.Length; i++)
                            {
                                if (owner.answers[i] == this)
                                {
                                    _id = i + 1;
                                    break;
                                }
                            }
                        }
                        return _id;
                    }
                }
                string _answerName;
                private string AnswerName
                {
                    get
                    {
                        if (_answerName == null)
                        {
                            _answerName = "A" + Id;
                        }
                        return _answerName;
                    }
                }

                public bool IsEmptyAnswer
                {
                    get
                    {
                        return string.IsNullOrEmpty(text);
                    }
                }

                AnswerValidnessState VerifyCorrectActionOrCheck(bool checkWarnings)
                {
                    AnswerValidnessState v;
 
                    if (!string.IsNullOrEmpty(this.checkType))
                    { 
                        if (!TierExist (checkSucess, true))
                        {
                            v = AnswerValidnessState
                                .CreateWarning(AnswerName, "Invalid 'check success' tier " + checkSucess);
                        } else
                        if (!TierExist(checkFail, true))
                        {
                            v = AnswerValidnessState
                                .CreateWarning(AnswerName, "Invalid 'check fail' tier id" + checkFail);
                        } else
                        {
                            v = AnswerValidnessState.CorrectState;
                        }
                    }
                    else
                    {
                        switch (actionType)
                        {
                            case ("next"):
                                {                                    
                                    if (TierExist (actionTier, true))
                                    {
                                        v = AnswerValidnessState.CorrectState;
                                    } else
                                    {
                                        v = AnswerValidnessState
                                            .CreateWarning(AnswerName, "Invalid 'next' tier " + actionTier);
                                    }
                                }
                                break;
                            case ("merchant"):
                                {
                                    v = AnswerValidnessState.CorrectState;
                                }
                                break;
                            case ("combat"):
                                {
                                    v = AnswerValidnessState.CorrectState;
                                }
                                break;
                            case ("end"):
                                {
                                    v = AnswerValidnessState.CorrectState;
                                    break;
                                }                               
                            default:
                                v = AnswerValidnessState.CreateError(AnswerName, "Non correct action");
                                break;                                
                        }
                    }

                    return v;
                }

                private bool TierExist (string id, bool compareWithOurTier)
                {
                    if (owner.owner == null)
                    {
                        return false;
                    }
                    foreach (Tier t in owner.owner.tiers)
                    {
                        if (t.id == id)
                        {
                            if (compareWithOurTier && id == this.owner.id)
                            {
                                return false;
                            } else
                            {
                                return true;
                            }                            
                        }
                    }
                    return false;
                }

                public string actionType;
                public string actionTier;
                public string actionTerrain;
                public string actionMonster;

                public string branchNPCName;
                public string branchTier;
                public string destroyNPCName;
                public string destroyLocation;

                public bool questIsMainQuest;
                public string questQuestID;
                public string questStatus;
                public string questUpdate;

                public bool markerIsMainQuest;
                public string markerQuestId;
                public string markerActionId;
                public string markerId;

                public string checkType;
                public string checkValue;
                public string checkSucess;
                public string checkFail;

                public string rewardType;
                public string rewardValue;
                public RewardItem[] rewardItems;

                public string cutSceneId;
                public string giveItemType;
                public string giveItemId;

                public string spawnName;
                public string spawnLocation;

                public bool HasCorrectGiveItem
                {
                    get
                    {
                        bool typeGood = !string.IsNullOrEmpty(this.giveItemType);
                        bool idGood = !string.IsNullOrEmpty(this.giveItemId);
                        return typeGood && idGood;
                    }
                }

                public class RewardItem
                {
                    Answer owner;

                    internal RewardItem(Answer a, int arrayPosition)
                    {
                        owner = a;
                        this.arrayPosition = arrayPosition;
                    }

                    public int arrayPosition;
                    public string type;
                    public string rewardId;
                    public string rewardEnchant;

                    public bool IsCorrect
                    {
                        get
                        {
                            bool typeGood = !string.IsNullOrEmpty(this.type);
                            bool idGood = !string.IsNullOrEmpty(this.rewardId);
                            return typeGood && idGood;
                        }
                    }

                    public void CopyAttributesFrom(RewardItem source)
                    {
                        this.type = source.type;
                        this.rewardId = source.rewardId;
                        this.rewardEnchant = source.rewardEnchant;
                    }
                }

                internal void CopyAttributesFrom(Answer source)
                {
                    this.actionType = source.actionType;
                    this.actionTier = source.actionTier;
                    this.actionTerrain = source.actionTerrain;
                    this.actionMonster = source.actionMonster;

                    this.branchNPCName = source.branchNPCName;
                    this.branchTier = source.branchTier;
                    this.destroyNPCName = source.destroyNPCName;
                    this.destroyLocation = source.destroyLocation;

                    this.questIsMainQuest = source.questIsMainQuest;
                    this.questQuestID = source.questQuestID;
                    this.questStatus = source.questStatus;
                    this.questUpdate = source.questUpdate;

                    this.markerIsMainQuest = source.markerIsMainQuest;
                    this.markerQuestId = source.markerQuestId;
                    this.markerActionId = source.markerActionId;
                    this.markerId = source.markerId;

                    this.checkType = source.checkType;
                    this.checkValue = source.checkValue;
                    this.checkSucess = source.checkSucess;
                    this.checkFail = source.checkFail;

                    this.rewardType = source.rewardType;
                    this.rewardValue = source.rewardValue;
                    for (int i = 0; i < this.rewardItems.Length; i++)
                    {
                        this.rewardItems[i].CopyAttributesFrom(source.rewardItems[i]);
                    }

                    this.cutSceneId = source.cutSceneId;

                    this.giveItemId = source.giveItemId;
                    this.giveItemType = source.giveItemType;

                    this.spawnName = source.spawnName;
                    this.spawnLocation = source.spawnLocation;
                }
            }
        }
    }

}
