﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadNordsSupport.Logic.Entities
{
    public class Book : TemplateItem
    {
        public Book()
        {
        }

        public Book(string bookId)
        {
            this.Id = bookId;
        }

        public Dictionary<int, Page> pages = new Dictionary<int, Page> ();

        private int _xp;
        public int XP
        {
            get
            {
                if (_xp < 0)
                {
                    return 0;
                }
                return _xp;
            }

            set
            {
                _xp = value;
            }
        }

        internal Page GetCreatePageById(int id)
        {
            Page p = null;
            if (pages.TryGetValue (id, out p))
            {
                return p;
            } else
            {
                p = new Page(id, this);
                pages.Add(id, p);
                return p;
            }
        }

        public class Page
        {
            private readonly Book bookOwner;
            private readonly int _id;

            private string text;
            private string title;

            private int imageStartLine;

            public Page (int id, Book bookOwner)
            {
                this._id = id;
                this.bookOwner = bookOwner;
            }

            #region imgId

            string _imageType;
            string _imageAnimName;
            int _imageAnimFrame;

            public void ReadImageId(string source)
            {
                try
                {
                    string[] str = source.Split('|');
                    _imageType = str[0];
                    _imageAnimName = str[1];
                    _imageAnimFrame = int.Parse ( str[2]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString ());
                }
            }

            public string GetImageId()
            {
                return _imageType + "|" + _imageAnimName + "|" + _imageAnimFrame;
            }

            #endregion

            #region properties

            public Book BookOwner
            {
                get
                {
                    return bookOwner;
                }
            }

            public string Text
            {
                get
                {
                    return text;
                }

                set
                {
                    text = value;
                }
            }

            public int ImageStartLine
            {
                get
                {
                    return imageStartLine;
                }

                set
                {
                    imageStartLine = value;
                }
            }

            public string Title
            {
                get
                {
                    return title;
                }

                set
                {
                    title = value;
                }
            }

            internal static Page CreateTest()
            {
                return new Page(-1, null);
            }

            #endregion

            public int Id { get { return _id; }}

            public bool HasCorrectImage {
                get {
                    if (string.IsNullOrEmpty (_imageType) || string.IsNullOrEmpty (_imageAnimName))
                    {
                        return false;
                    }
                    return true;
                }
            }

            public bool IsSavable {
                get
                {
                    bool savable = false;
                    if (HasCorrectImage)
                    {
                        savable = true;
                    }

                    if (!string.IsNullOrEmpty(Text) || !string.IsNullOrEmpty(Title))
                    {
                        savable = true;
                    }
                    return savable;
                }
            }

            public string ImageType
            {
                get
                {
                    return _imageType;
                }

                set
                {
                    _imageType = value;
                }
            }

            public string ImageAnimName
            {
                get
                {
                    return _imageAnimName;
                }

                set
                {
                    _imageAnimName = value;
                }
            }

            public int ImageAnimFrame
            {
                get
                {
                    return _imageAnimFrame;
                }

                set
                {
                    _imageAnimFrame = value;
                }
            }
        }

        internal Page GetPage(int i)
        {
            Page p = null;
            this.pages.TryGetValue(i, out p);
            return p;
        }
    }
}

public class TextAnalyzer
{
    public static readonly int PAGE_WIDTH = 416;
    public static readonly int PAGE_HEIGHT = 480;
    public static readonly int PAGE_ROW_NUM = 15;
    public static readonly int ROW_HEIGHT = PAGE_HEIGHT / PAGE_ROW_NUM;

    public static readonly int IMG_SIZE_FULL_PAGE = 480;
    public static readonly int IMG_SIZE_MEDIUM = 128;
    public static readonly int IMG_SIZE_SMALL = 64;
    public static readonly int IMG_SIZE_EXTRA_SMALL = 32;

    private static TextAnalyzer instance;

    public static TextAnalyzer Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new TextAnalyzer();
            }

            return instance;
        }
    }

    private Dictionary<char, int> charactersWidth = new Dictionary<char, int>();
    private Dictionary<string, int> imagesHeight = new Dictionary<string, int>();

    private TextAnalyzer ()
    {
        Init();
    }

    private void Init ()
    {
        ReadCharactersWidth("Ii.:!'|`°Іі", 6);
        ReadCharactersWidth("jlt1,;()[]", 8);
        ReadCharactersWidth("fr=<>з", 10);
        ReadCharactersWidth("£ ", 11);
        ReadCharactersWidth("Js379?-/+*", 12);
        ReadCharactersWidth("ABCDEFGHKLNOPQRSUVXZabcdeghknopquvxyz024568$€ЙУКЕНГХЇВАПРОЛЄЭЯЧСИЬБйукеэнхївапролєясиьб", 13);
        charactersWidth['\"'] = 13;
        ReadCharactersWidth("TY_&ЦЪДТцът", 15);
        ReadCharactersWidth("M~#@%Мм", 17);
        ReadCharactersWidth("WmwШФЫЖЮшфыжю", 20);
        ReadCharactersWidth("Щщ", 22);

        PrepareImageSize("full_page", IMG_SIZE_FULL_PAGE);
        PrepareImageSize("medium", IMG_SIZE_MEDIUM);
        PrepareImageSize("small", IMG_SIZE_SMALL);
        PrepareImageSize("extra_small", IMG_SIZE_EXTRA_SMALL);
    }

    private void PrepareImageSize (string imageName, int height)
    {
        if (!this.imagesHeight.ContainsKey(imageName))
        {
            imagesHeight[imageName] = height;
        }
    }

    private void ReadCharactersWidth (string characters, int characherWidth)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            char c = characters[i];
            
            if (!this.charactersWidth.ContainsKey (c))
            {
                charactersWidth[c] = characherWidth + 1;
            }
        }
    }

    private string _title;
    private string _body;
    private string _imageType;

    public void ClearData ()
    {
        _title = null;
        _body = null;
        _imageType = null;
        currentRow = PAGE_ROW_NUM;
    }

    public void SetupTitle (string title)
    {
        _title = title;
    }

    public void SetupBody (string body)
    {
        _body = body;
    }

    public void SetupImageType (string imageType)
    {
        this._imageType = imageType;
    }

    public string Analyze ()
    {
        currentRow = 0;
        lastCarretPosition = 0;
        AnalyzeTitleSize();
        AnalyzeBodySize();
        AnalyzeImageSize();
        
        string pageWidth = "Page width = " + PAGE_WIDTH;
        string titleData = "Title width = " + titleCarretPosition;
        string bodyData = "Body width = " + bodyCarretPosition;

        string rowNum = "Page row num = " + PAGE_ROW_NUM;
        string usedRows = "Used rows = " + currentRow;
        if (currentRow > PAGE_ROW_NUM)
        {
            usedRows = "WARNING!!!!! too much rows used = " + currentRow;
        }
        string sSymbolCount = "Last word width = " + lastWordWidth;
        string imageHeight = "";
        if (curImageHeight != 0)
        {
            imageHeight = "Image Height = " + this.curImageHeight;
        } else
        {
            if (!string.IsNullOrEmpty (this._imageType))
            {
                imageHeight = "Warning! Error Image Type!";
            }
        }

        string detailedRes = usedRows + "; " + rowNum + "; " + imageHeight + "\n"
            + pageWidth + "; " + titleData + "; " + bodyData + "; " + sSymbolCount;

        return detailedRes;
    }

    int currentRow = 0;
    int lastCarretPosition = 0;
    int titleCarretPosition = 0;
    int bodyCarretPosition = 0;
    int curImageHeight = 0;

    int prevSpacePosition = 0;
    int lastWordWidth = 0;

    public void AnalyzeTextBlockSize (string text)
    {
        int positionInRow = 0;

        lastWordWidth = 0;
        prevSpacePosition = 0;

        for (int i = 0; i < text.Length; i++)
        {
            char c = text[i];

            if (c == '\n')
            {
                lastWordWidth = 0;
                positionInRow = 0;
                currentRow += 1;
            } else
            {
                int charWidth = 0;
                this.charactersWidth.TryGetValue(c, out charWidth);

                if (c == ' ' || c == '-')
                {
                    prevSpacePosition = positionInRow;
                    lastWordWidth = 0;
                } else
                {
                    lastWordWidth += charWidth;
                }

                int nextPosition = positionInRow + charWidth;
                if (nextPosition > PAGE_WIDTH) 
                {                  
                    currentRow += 1;
                    positionInRow = lastWordWidth;
                }
                else
                {
                    positionInRow = nextPosition;
                }
            }
        }

        lastCarretPosition = positionInRow;
    }

    public string RefactorTextBlock(string text)
    {
        int positionInRow = 0;

        lastWordWidth = 0;
        prevSpacePosition = 0;

        for (int i = 0; i < text.Length; i++)
        {
            char c = text[i];

            if (c == '\n')
            {
                lastWordWidth = 0;
                positionInRow = 0;
            }
            else
            {
                int charWidth = 0;
                this.charactersWidth.TryGetValue(c, out charWidth);

                if (c == ' ' || c == '-')
                {
                    prevSpacePosition = i;
                    lastWordWidth = 0;
                }
                else
                {
                    lastWordWidth += charWidth;
                }

                int nextPosition = positionInRow + charWidth;
                if (nextPosition > PAGE_WIDTH)
                {
                    char prevC = text[prevSpacePosition];
                    if (prevC == ' ')
                    {
                        text = text.Remove(prevSpacePosition, 1);
                        text = text.Insert(prevSpacePosition, "\n");
                    } else
                    {
                        text = text.Insert(prevSpacePosition + 1, "\n");
                    }

                    positionInRow = lastWordWidth;
                }
                else
                {
                    positionInRow = nextPosition;
                }
            }
        }

        lastCarretPosition = positionInRow;
        return text;
    }

    public void AnalyzeTitleSize()
    {
        if (!string.IsNullOrEmpty (_title))
        {
            AnalyzeTextBlockSize(_title);
            currentRow += 1;
            this.titleCarretPosition = lastCarretPosition;
        }        
    }

    public int AnalyzeTextSize (string text)
    {
        AnalyzeTextBlockSize(text);
        return lastCarretPosition;
    }

    public void AnalyzeBodySize()
    {
        if (!string.IsNullOrEmpty(_body))
        {
            AnalyzeTextBlockSize(_body);
            currentRow += 1;

            this.bodyCarretPosition = lastCarretPosition;
        }
    }

    public void AnalyzeImageSize()
    {
        curImageHeight = 0;
        if(imagesHeight.TryGetValue(this._imageType, out curImageHeight))
        {
            float fRowUsed = curImageHeight / (float)ROW_HEIGHT;
            int iRowUsed = (int)fRowUsed;

            if (iRowUsed < fRowUsed)
            {
                currentRow += (iRowUsed + 1);
            }
            else
            {
                currentRow += iRowUsed;
            }
        }
    }

    public int CountAllCharacters()
    {
        int iSymbolCount = 0;
        if (!string.IsNullOrEmpty(_title))
        {
            iSymbolCount += _title.Length;
        }

        if (!string.IsNullOrEmpty(_body))
        {
            iSymbolCount += _body.Length;
        }
        return iSymbolCount;
    }
}
