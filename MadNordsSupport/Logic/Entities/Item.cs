﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadNordsSupport.Logic.Entities
{
    class ItemType : TemplateItem
    {
        public readonly Dictionary<int, Item> items = new Dictionary<int, Item>();
        int _biggestItemId = Int32.MinValue;

        public ItemType(string typeId)
        {
            this.Id = typeId;
        }

        public int BiggestItemId
        {
            get
            {
                return _biggestItemId;
            }
            private set
            {
                this._biggestItemId = value;
            }
        }

        public string[] AllItemTitles {
            get {
                int count = items.Count;
                string[] arr = new string[count];
                int i = 0;
                foreach (Item item in items.Values)
                {
                    arr[i] = item._title;
                    i++;
                }
                return arr;
            }
        }

        public bool HasItemNumericId(int num)
        {
            return items.ContainsKey(num);
        }

        public Item GetItemByNumericId(int numericId)
        {
            if (!HasItemNumericId (numericId))
            {
                return null;
            }

            return items[numericId];
        }

        public void RemoveItem(Item i)
        {
            if (this.items.ContainsKey(i._numericId))
            {
                this.items.Remove(i._numericId);
            }
        }

        internal Item GetCreateItemById(int numericId, string title)
        {
            if (numericId > this.BiggestItemId)
            {
                this.BiggestItemId = numericId;
            }

            Item i = GetItemByNumericId(numericId);
            if (i == null)
            {
                i = Item.CreateItem(this, numericId, title);
                this.items.Add(numericId, i);

                string num = numericId.ToString("D3");
                string newS = num + " - " + title;
                itemsNumTitlesNames.Add(newS);
            }
            return i;
        }

        public List<string> itemsNumTitlesNames = new List<string>();

        internal string GetItemNameByNumericId(string giveItemId)
        {
            List<string> items = itemsNumTitlesNames;
            int id = int.Parse(giveItemId);
            string str = items[id];
            return str;
        }

        internal bool HasItemWithTitle(string text)
        {
            foreach (Item i in this.items.Values)
            {
                if (i._title == text)
                {
                    return true;
                }
            }
            return false;
        }

        internal Item GetItemByTitle(string text)
        {
            foreach (Item i in this.items.Values)
            {
                if (i._title == text)
                {
                    return i;
                }
            }
            return null;
        }
    }

    class Item
    {
        public readonly ItemType myOwner;

        public int _numericId;
        public string _title;
        public string _description;

        public int _st;
        public int _ag;
        public int _in;
        public int _vi;
        public int _wi;
        public int _ac;
        public int _dc;
        public int _cc;
        public int _pr;
        public int _mr;

        public int _cost;

        public int _hp;
        public int _mp;

        public void CleanAllFields ()
        {

        }
        
        private Item (ItemType ownerClass, int id, string title) {
            this.myOwner = ownerClass;
            this._numericId = id;
            this._title = title;
        }

        public static Item CreateItem (ItemType ownerClass, int id, string title)
        {
            return new Item(ownerClass, id, title);
        }

        public static string GetIdFromItemName(string str)
        {
            string ch = str.Substring(0, Math.Min(str.Length, 3));
            int i = Int32.Parse(ch);
            return i + "";
        }
    }
}
