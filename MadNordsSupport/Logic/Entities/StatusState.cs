﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadNordsSupport.Logic.Entities
{
    struct AnswerValidnessState
    {
        private AnswerValidnessState(string error, string warning) {
            _errorName = error;
            _warning = warning;
        }

        public readonly static AnswerValidnessState CorrectState;

        public static AnswerValidnessState CreateError(string AnswerName, string errorName)
        {
            AnswerValidnessState t = new AnswerValidnessState(AnswerName + " " + errorName, null);
            return t;
        }

        public static AnswerValidnessState CreateWarning(string AnswerName, string warning)
        {
            AnswerValidnessState t = new AnswerValidnessState(null, AnswerName + " " + warning); 
            return t;
        }

        //public bool IsCorrect
        //{
        //    get
        //    {
        //        return _errorName == null && _warning == null;
        //    }
        //}

        public bool HasError
        {
            get
            {
                return !string.IsNullOrEmpty(ErrorName);
            }
        }
        string _errorName;

        public bool HasWarning
        {
            get
            {
                return !string.IsNullOrEmpty(Warning);
            }
        }

        string ErrorName
        {
            get
            {
                return _errorName;
            }

            set
            {
                _errorName = value;
            }
        }

        string Warning
        {
            get
            {
                return _warning;
            }

            set
            {
                _warning = value;
            }
        }

        string _warning;

        public string BugsMessage
        {
            get
            {
                if (ErrorName != null)
                {
                    return ErrorName;
                }

                if (Warning != null)
                {
                    return Warning;
                }

                return null;
            }
        }
    }    
}
