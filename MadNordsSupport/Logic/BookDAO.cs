﻿using MadNordsSupport.Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    public class BookDAO : TemplateDAO<Book>
    {
        private static BookDAO instance;
        private BookDAO() { }

        public static BookDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BookDAO();
                }
                return instance;
            }
        }

        public override string DataSourceName
        {
            get
            {
                return MadNordsSettings.Default.BookFileName;
            }

            set
            {
                MadNordsSettings.Default.BookFileName = value;
                MadNordsSettings.Default.Save();
            }
        }

        protected override bool ReadDataFromXml(XDocument xml)
        {
            var query = from c in xml.Root.Descendants("book")
                        select c;

            if (query == null)
            {
                return false;
            }

            allData.Clear();
            foreach (XElement b in query)
            {
                try
                {
                    ReadBook(b);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            return true;
        }

        private void ReadBook(XElement b)
        {
            string bookId = b.TryAttributeValue("id");
            if (this.allData.ContainsKey(bookId))
            {
                Console.WriteLine("Error, Book with exist, id = " + bookId);
            }
            else
            {
                Book book = new Book(bookId);
                int xp = 0;
                int.TryParse ( b.TryAttributeValue("xp"), out xp);
                var query = from c in b.Descendants("page")
                            select c;
                book.XP = xp;

                foreach (XElement x in query)
                {
                    ReadPage(x, book);
                }

                this.allData.Add(bookId, book);
            }
        }

        private void ReadPage(XElement x, Book owner)
        {
            int id = -1;            
            string idTXT = x.TryAttributeValue("id");

            if (!int.TryParse(idTXT, out id))
            {
                return;
            }

            Book.Page p = owner.GetCreatePageById(id);

            string title = x.TryElementValue("title");
            string text = x.TryElementValue("text");

            XElement img = x.Element("img");
            if (img != null)
            {
                string imgId = img.TryAttributeValue("id");
                string imgStartLine = img.TryAttributeValue("start_line");

                p.ReadImageId (imgId) ;
                try
                {
                    p.ImageStartLine = int.Parse (imgStartLine);
                } catch (Exception e)
                {
                    Console.Write(e.ToString ());
                }               
            }
                        
            p.Text = text;
            p.Title = title;
        }

        public override bool AddUpdate(Book book)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            XElement xbookElement = null;
            if (allData.ContainsKey(book.Id))
            {
                XElement desE = currentXDocument.Root.Elements("book")
                       .Where(e => (string)e.Attribute("id") == book.Id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
 //                   desE.Remove();
                    xbookElement = desE;
                }
            }
            if (xbookElement == null)
            {
                xbookElement = new XElement("book");
                currentXDocument.Root.Add(xbookElement);
            }
            xbookElement.Add(new XAttribute("id", book.Id));
            xbookElement.Add(new XAttribute("xp", book.XP)); 

            foreach (Book.Page page in book.pages.Values)
            {
                if (!page.IsSavable)
                {
                    continue;
                }

                XElement xpage = xbookElement.CreateChild("page");
                xpage.CreateAttributeValue("id", page.Id + "");
                if (!string.IsNullOrEmpty (page.Title))
                {
                    xpage.CreateChildBody("title", page.Title);
                }

                if (!string.IsNullOrEmpty(page.Text))
                {
                    xpage.CreateChildBody("text", page.Text);
                }

                if (page.HasCorrectImage)
                {
                    XElement img = xpage.CreateChild("img");
                    img.CreateAttributeValue("id", page.GetImageId ());
                    img.CreateAttributeValue("start_line", page.ImageStartLine + "");
                }
            }

            SaveDataInDictionary(book);

            XMLDao.SaveDocument(currentXDocument, DataSourceName);

            return true;
        }

        public override Book CreateNew()
        {
            return new Book();
        }

        public override void Delete(string id)
        {
            allData.Remove(id);

            XElement desE = currentXDocument.Root.Elements("book")
                   .Where(e => (string)e.Attribute("id") == id)
                   .FirstOrDefault();

            if (desE != null)
            {
                desE.RemoveAll();
                desE.Remove();
            }

            XMLDao.SaveDocument(currentXDocument, DataSourceName);
        }
    }
}
