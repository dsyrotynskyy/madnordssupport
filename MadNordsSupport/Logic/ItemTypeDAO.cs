﻿using MadNordsSupport.Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    class ItemTypeDAO : TemplateDAO<ItemType>
    {
        private static ItemTypeDAO instance;
        private ItemTypeDAO() { Prepare(); }

        public static ItemTypeDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ItemTypeDAO();
                }

                return instance;
            }
        }

        public override string DataSourceName
        {
            get
            {
                return MadNordsSettings.Default.InventoryFileName;
            }

            set
            {
                MadNordsSettings.Default.InventoryFileName = value;
                MadNordsSettings.Default.Save();
            }
        }

        public bool HasItemType (string type)
        {
            return allData.ContainsKey(type);
        } 

        public List<string> enchantTypes = new List<string>() {
            "stat_strength",
            "stat_agility",
            "stat_intellect",
            "stat_vitality",
            "stat_wisdom",
            "armor_class",
            "chance_dodge",
            "chance_crit",
            "resist_phys",
            "resist_mag"};

        void Prepare()
        {
        }

        public ItemType GetItemType(string type)
        {
            if (allData.ContainsKey (type))
            {
                return allData[type];
            }

            return null;
        }

        protected override bool ReadDataFromXml(XDocument xml)
        {
            var query = from c in xml.Root.Descendants("type")
                        select c;

            if (query == null)
            {
                return false;
            }

            allData.Clear();
            foreach (XElement q in query)
            {
                try
                {
                    ReadItemType(q);
                } catch (Exception e)
                {
                    Console.WriteLine(e.ToString ());
                }
            }

            return true;
        }

        private void ReadItemType(XElement q)
        {
            string typeId = q.TryAttributeValue("id");
            if (this.allData.ContainsKey (typeId))
            {
                Console.WriteLine("Error, ItemType with exist, id = " + typeId);
            } else
            {
                ItemType t = new ItemType(typeId);
                var query = from c in q.Descendants("item")
                            select c;
                foreach (XElement x in query)
                {
                    ReadItem(x, t);
                }

                this.allData.Add(typeId, t);
            }           
        }

        private void ReadItem (XElement x, ItemType owner)
        {
            int id = x.TryAttributeValueInt("id");
            string title = x.TryElementValue("title");

            if (id < 0)
            {
                Console.WriteLine("Error, uncorrect item id = " + id);
                return;
            } else
            {
                Item i = owner.GetCreateItemById(id, title);
                i._description = x.TryElementValue("description");

                i._mp = x.TryElementValueInt("mp");
                i._hp = x.TryElementValueInt("hp");

                i._st = x.TryElementValueInt("st");               
                i._ag = x.TryElementValueInt("ag");
                i._in = x.TryElementValueInt("in");
                i._vi = x.TryElementValueInt("vi");
                i._wi = x.TryElementValueInt("wi");
                i._ac = x.TryElementValueInt("ac");
                i._dc = x.TryElementValueInt("dc");
                i._cc = x.TryElementValueInt("cc");
                i._pr = x.TryElementValueInt("pr");
                i._mr = x.TryElementValueInt("mr");

                i._cost = x.TryElementValueInt("cost");
            }
        }

        public bool AddUpdate(Item item)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            ItemType itemType = item.myOwner;
            string itemId = item._numericId + "";

            XElement xitem = null;
            XElement xitemType = currentXDocument.Element("items").Elements("type")
                       .Where(e => (string)e.Attribute("id") == itemType.Id)
                       .FirstOrDefault();

            XElement desE2 = xitemType.Elements("item")
                       .Where(e => (string)e.Attribute("id") == itemId)
                       .FirstOrDefault();

            if (desE2 != null)
            {
                desE2.RemoveAll();
 //               desE2.Remove();
                xitem = desE2;
            }

            if (xitem == null)
            {
                xitem = new XElement("item");
                xitemType.Add(xitem);
            }
            xitem.Add(new XAttribute("id", itemId));

            xitem.SetElementValue("title", item._title);
            xitem.SetElementValue("description", item._description);

            xitem.CreateChildBodyIntBiggerZero("hp", item._hp);
            xitem.CreateChildBodyIntBiggerZero("mp", item._mp);

            xitem.CreateChildBodyIntBiggerZero("st", item._st);
            xitem.CreateChildBodyIntBiggerZero("ag", item._ag);
            xitem.CreateChildBodyIntBiggerZero("in", item._in);
            xitem.CreateChildBodyIntBiggerZero("vi", item._vi);
            xitem.CreateChildBodyIntBiggerZero("wi", item._wi);
            xitem.CreateChildBodyIntBiggerZero("ac", item._ac);
            xitem.CreateChildBodyIntBiggerZero("dc", item._dc);
            xitem.CreateChildBodyIntBiggerZero("cc", item._cc);
            xitem.CreateChildBodyIntBiggerZero("pr", item._pr);
            xitem.CreateChildBodyIntBiggerZero("mr", item._mr);

            xitem.CreateChildBodyIntBiggerZero("cost", item._cost);

            XMLDao.SaveDocument(currentXDocument, DataSourceName);
            return true;
        }

        public bool Delete(Item item)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            ItemType itemType = item.myOwner;
            string itemId = item._numericId + "";

            XElement xdialogElement = null;
            XElement desE1 = currentXDocument.Element("items").Elements("type")
                       .Where(e => (string)e.Attribute("id") == itemType.Id)
                       .FirstOrDefault();

            XElement desE2 = desE1.Elements("item")
                       .Where(e => (string)e.Attribute("id") == itemId)
                       .FirstOrDefault();

            if (desE2 != null)
            {
                desE2.RemoveAll();
                desE2.Remove();
                xdialogElement = desE2;
                item.myOwner.RemoveItem(item);
            } else
            {
                Console.WriteLine("Not found element");
                return false;
            }
            XMLDao.SaveDocument(currentXDocument, DataSourceName);
            return true;
        }

        #region UnsupportedMethods
        public override bool AddUpdate(ItemType d)
        {
            throw new NotSupportedException();
        }

        public override ItemType CreateNew()
        {
            throw new NotSupportedException();
        }

        public override void Delete(string text)
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
