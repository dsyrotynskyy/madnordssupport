﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MadNordsSupport.Logic
{
    class SignDAO : TemplateDAO<Sign>
    {
        private static SignDAO instance;
        private SignDAO() { }

        public static SignDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SignDAO();
                }

                return instance;
            }
        }

        public override string DataSourceName
        {
            get
            {
                return MadNordsSettings.Default.SignFileName;
            }

            set
            {
                MadNordsSettings.Default.SignFileName = value;
                MadNordsSettings.Default.Save();
            }
        }

        protected override bool ReadDataFromXml(XDocument xml)
        {
            var query = from c in xml.Root.Descendants("text")
                        select c;

            if (query == null)
            {
                return false;
            }

            allData.Clear();
            foreach (XElement b in query)
            {
                try
                {
                    ReadSign(b);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            return true;
        }

        private void ReadSign (XElement x)
        {
            string signId = x.TryAttributeValue("id");
            string body = x.Value;

            if (this.allData.ContainsKey(signId))
            {
                Console.WriteLine("Error, Book with exist, id = " + signId);
            }
            else
            {
                Sign s = new Sign(signId);
                s.text = body;

                this.allData.Add(signId, s);
            }
        }

        public override bool AddUpdate(Sign s)
        {
            if (currentXDocument == null)
            {
                return false;
            }

            XElement xElement = null;
            if (allData.ContainsKey(s.Id))
            {
                XElement desE = currentXDocument.Root.Elements("text")
                       .Where(e => (string)e.Attribute("id") == s.Id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
                    xElement = desE;
                }
            }
            if (xElement == null)
            {
                xElement = new XElement("text");
                currentXDocument.Root.Add(xElement);
            }
            xElement.Add(new XAttribute("id", s.Id));
            xElement.Value = s.text;



            SaveDataInDictionary(s);

            XMLDao.SaveDocument(currentXDocument, DataSourceName);

            return true;
        }

        public override Sign CreateNew()
        {
            return new Sign();           
        }

        public override void Delete(string id)
        {
            allData.Remove(id);

            XElement desE = currentXDocument.Root.Elements("text")
                   .Where(e => (string)e.Attribute("id") == id)
                   .FirstOrDefault();

            if (desE != null)
            {
                desE.RemoveAll();
                desE.Remove();
            }

            XMLDao.SaveDocument(currentXDocument, DataSourceName);
        }
    }

    class Sign : TemplateItem
    {
        public string text;

        public Sign(string signId)
        {
            this.Id = signId;
        }

        public Sign ()
        {
        }
    }
}
