﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport.Logic
{
    class DialogDAO
    {
        private static DialogDAO instance;

        private DialogDAO() { }

        public static DialogDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DialogDAO();
                }
                return instance;
            }
        }

        public Dictionary<string, Dialog> allDialogs = new Dictionary<string, Dialog>();
        public string targetFileName
        {
            get
            {
                return MadNordsSettings.Default.DialogFiIeName;
            }
        }

        XDocument currentXDocument;

        internal bool TryReadFrom(string file)
        {
            bool res = ReadFromFile(file);
            if (res)
            {
                MadNordsSettings.Default.DialogFiIeName = file;
                MadNordsSettings.Default.Save();
            }
            return res;
        }

        private bool ReadFromFile(string file)
        {
            var xml = XDocument.Load(file);
            if (xml == null)
            {
                return false;
            }
            var query = from c in xml.Root.Descendants("dialog")
                        select c;

            if (query == null)
            {
                return false;
            }

            allDialogs.Clear();
            foreach (XElement q in query)
            {
                ReadDialog(q);
            }

            this.currentXDocument = xml;

            return true;
        }

        void ReadDialog(XElement q)
        {
            Dialog newDialog = new Dialog();
            newDialog.id = q.Attribute("id").Value;
            XAttribute xname = q.Attribute("name");
            if (xname != null)
            {
                newDialog.name = xname.Value;
            } else
            {
                newDialog.name = newDialog.id;
            }

            if (newDialog.id == null || newDialog.id == "")
            {
                return;
            }

            int i = 0;
            foreach (XElement tElement in q.Elements("tier"))
            {
                XAttribute xid = tElement.Attribute("id");
                if (xid == null || newDialog.HasTier(xid.Value))
                {
                    continue;
                }

                Dialog.Tier t = newDialog.AddNewTier(xid.Value, i);

                ReadTier(t, tElement);
                i++;
            }

            if (allDialogs.ContainsKey(newDialog.id))
            {
                allDialogs[newDialog.id] = newDialog; 
            }
            else
            {
                allDialogs.Add(newDialog.id, newDialog);
            }
        }

        void ReadTier(Dialog.Tier t, XElement tierElement)
        {

            string txt = tierElement.Element("npc").Value;
            t.NPCText = txt;

            XAttribute comment = tierElement.Attribute("comment");
            if (comment != null)
            {
                t.comment = comment.Value;
            }

            IEnumerable<XElement> answers = tierElement.Element("player").Elements("answer");
            foreach (XElement aElement in answers)
            {
                string id = aElement.Attribute("id").Value;

                int num = int.Parse(id) - 1;
                ReadAnswer(t.answers[num], aElement);
            }
        }

        void ReadAnswer(Dialog.Tier.Answer a, XElement answerElement)
        {
            a.text = answerElement.Element("text").Value;

            XElement action = answerElement.Element("action");
            if (action != null) {
                a.actionType = action.TryAttributeValue("type");
                a.actionTier = action.TryAttributeValue("tier");
                a.actionTerrain = action.TryAttributeValue("terrain");
                a.actionMonster = action.TryAttributeValue("monster");
            }

            {
                XElement branch = answerElement.Element("branch");
                if (branch != null)
                {
                    a.branchNPCName = branch.TryAttributeValue("name");
                    a.branchTier = branch.TryAttributeValue("tier");
                }
            }

            {
                XElement spawn = answerElement.Element("spawn");
                if (spawn != null)
                {
                    a.spawnLocation = spawn.TryAttributeValue("location");
                    a.spawnName = (string)spawn.Value;
                }
            }

            {
                XElement destroy = answerElement.Element("destroy");
                if (destroy != null)
                {
                    a.destroyLocation = destroy.TryAttributeValue("location");
                    a.destroyNPCName = (string)destroy.Value;
                }
            }

            a.cutSceneId = answerElement.TryElementValue("cutscene");
            {
                XElement quest = answerElement.Element("quest");
                if (quest != null)
                {
                    a.questIsMainQuest = quest.TryAttributeValue("type") == "main";
                    a.questQuestID = quest.TryAttributeValue("id");
                    a.questStatus = quest.TryAttributeValue("status");
                    a.questUpdate = quest.TryAttributeValue("update");
                }
            }

            {
                XElement marker = answerElement.Element("marker");
                if (marker != null)
                {
                    a.markerIsMainQuest = marker.TryAttributeValue("type") == "main";
                    a.markerQuestId = marker.TryAttributeValue("id");
                    a.markerActionId = marker.TryAttributeValue("action");
                    a.markerId = marker.TryAttributeValue("N");
                }
            }            
            
            {
                XElement check = answerElement.Element("check");
                if (check != null)
                {
                    a.checkType = check.TryAttributeValue("type");
                    a.checkValue = check.TryAttributeValue("value");
                    a.checkSucess = check.TryAttributeValue("success");
                    a.checkFail = check.TryAttributeValue("fail");
                }
            }
            {
                XElement reward = answerElement.Element("reward");
                if (reward != null)
                {
                    a.rewardType = reward.TryAttributeValue("type");
                    a.rewardValue = reward.TryAttributeValue("value");
                    int i = 0;
                    foreach (XElement xitem in reward.Elements("item"))
                    {
                        string body = xitem.Value;
                        string[] itemData = body.Split('|');

                        string type = itemData[0];
                        string elementId = itemData[1];
                        string enchant = itemData[2];

                        a.rewardItems[i].type = type;
                        a.rewardItems[i].rewardId = elementId;
                        a.rewardItems[i].rewardEnchant = enchant;

                        i++;
                    }
                }               
            }

            {
                XElement giveItem = answerElement.Element("give");
                if (giveItem != null)
                {
                    a.giveItemType = giveItem.TryAttributeValue("type");           
                    a.giveItemId = giveItem.TryAttributeValue("id");
                }             
            }
        }

        internal void DeleteDialog(string id)
        {
            allDialogs.Remove(id);

            XElement desE = currentXDocument.Element("dialogs").Elements("dialog")
                   .Where(e => (string)e.Attribute("id") == id)
                   .FirstOrDefault();

            if (desE != null)
            {
                desE.RemoveAll();
                desE.Remove();
            }
            XMLDao.SaveDocument(currentXDocument, targetFileName);
        }

        internal bool AddUpdateDialog(Dialog source, bool saveXML = true)
        {
            if (source.CheckErrors)
            {
                Console.WriteLine("Dialog " + source.id + " has critical errors and can not be saved");
                return false;
            }

            Dialog d = new Dialog();
            d.id = source.id;
            d.CopyFrom(source);

            XDocument xml = currentXDocument;
            if (xml == null)
            {
                return false;
            }

            XElement xdialogElement = null;
            if (allDialogs.ContainsKey(d.id))
            {
                XElement desE = currentXDocument.Element("dialogs").Elements("dialog")
                       .Where(e => (string)e.Attribute("id") == d.id)
                       .FirstOrDefault();

                if (desE != null)
                {
                    desE.RemoveAll();
//                    desE.Remove();
                    xdialogElement = desE;
                }
            }
            if (xdialogElement == null)
            {
                xdialogElement = new XElement("dialog");
                xml.Element("dialogs").Add(xdialogElement);
            }
            xdialogElement.Add(new XAttribute("id", d.id));
            xdialogElement.Add(new XAttribute("name", d.name));

            WriteTiers(d, xdialogElement);

            if (allDialogs.ContainsKey(d.id))
            {
                allDialogs[d.id] = d; 
            }
            else 
            { 
                allDialogs.Add(d.id, d);
            }

            if (saveXML)
            {
                XMLDao.SaveDocument(xml, targetFileName);
            }

            return true;           
        }

        void WriteTiers(Dialog d, XElement xdialog)
        {
            foreach (Dialog.Tier t in d.tiers)
            {
                XElement xtier = new XElement("tier", new XAttribute("id", t.id));
                xdialog.Add(xtier);

                if (!string.IsNullOrEmpty(t.comment))
                {
                    xtier.Add(new XAttribute("comment", t.comment));
                }

                xtier.CreateChildBody("npc", t.NPCText);  
                
                XElement player = xtier.CreateChild("player");
               
                for (int i = 2; i >= 0; i--) {
                    Dialog.Tier.Answer a = t.answers[i];
                    
                    if (!a.ValidnessState (false).HasError) {
                        XElement xanswer = new XElement ("answer", new XAttribute("id", i + 1));
                        player.Add (xanswer);

                        WriteAnswerNode (a ,xanswer);                       
                    }                   
                }               
            }
        }

        void WriteAnswerNode (Dialog.Tier.Answer a, XElement xAnswer) {
            xAnswer.CreateChildBody("text", a.text);

            if (!string.IsNullOrEmpty(a.cutSceneId))
            {
                xAnswer.CreateChildBody("cutscene", a.cutSceneId);
            }

            if (!string.IsNullOrEmpty(a.actionType))
            {
                XElement xaction = xAnswer.CreateChild("action");
                xaction.CreateAttributeValue("type", a.actionType);
                xaction.CreateAttributeValue("tier", a.actionTier);
                xaction.CreateAttributeValue("terrain", a.actionTerrain);
                xaction.CreateAttributeValue("monster", a.actionMonster);
            }

            if (!string.IsNullOrEmpty(a.branchNPCName))
            {
                XElement xbranching = xAnswer.CreateChild("branch");
                xbranching.CreateAttributeValue("name", a.branchNPCName);
                xbranching.CreateAttributeValue("tier", a.branchTier);
            }

            if (!string.IsNullOrEmpty(a.destroyNPCName))
            {
                XElement xspawn = xAnswer.CreateChild("destroy");
                xspawn.CreateAttributeValue("location", a.destroyLocation);
                xspawn.Value = a.destroyNPCName;
            }

            if (!string.IsNullOrEmpty(a.spawnName))
            {
                XElement xspawn = xAnswer.CreateChild("spawn");
                xspawn.CreateAttributeValue("location", a.spawnLocation);
                xspawn.Value = a.spawnName;
            }

            if (!string.IsNullOrEmpty(a.questQuestID))
            {
                XElement xquest = xAnswer.CreateChild("quest");
                
                string qType = a.questIsMainQuest ? "main" : "side";
                xquest.CreateAttributeValue("type", qType);
                xquest.CreateAttributeValue("id", a.questQuestID);
                xquest.CreateAttributeValue("status", a.questStatus);
                if (!string.IsNullOrEmpty (a.questUpdate))
                    xquest.CreateAttributeValue("update", a.questUpdate);
            }

            if (!string.IsNullOrEmpty(a.markerQuestId))
            {
                XElement xmarker = xAnswer.CreateChild("marker");

                string qType = a.markerIsMainQuest ? "main" : "side";
                xmarker.CreateAttributeValue("type", qType);
                xmarker.CreateAttributeValue("id", a.markerQuestId);
                xmarker.CreateAttributeValue("action", a.markerActionId);
                xmarker.CreateAttributeValue("N", a.markerId);
            }

            if (!string.IsNullOrEmpty(a.checkType))
            {
                XElement xcheck = xAnswer.CreateChild("check");

                xcheck.CreateAttributeValue("type", a.checkType);
                xcheck.CreateAttributeValue("value", a.checkValue);
                xcheck.CreateAttributeValue("success", a.checkSucess);
                xcheck.CreateAttributeValue("fail", a.checkFail);
            }

            {
               if (!string.IsNullOrEmpty(a.rewardType))
                {
                    XElement xReward = xAnswer.CreateChild("reward");

                    xReward.CreateAttributeValue("type", a.rewardType);
                    xReward.CreateAttributeValue("value", a.rewardValue);

                    if (a.rewardType == "item")
                    {
                        int id = 0;
                        foreach (Dialog.Tier.Answer.RewardItem rewardItem in a.rewardItems)
                        {
                            if (!rewardItem.IsCorrect)
                            {
                                continue;
                            }

                            var xChild = xReward.CreateChild("item");
                            xChild.CreateAttributeValue("id", id + "");

                            string body = rewardItem.type + "|" + rewardItem.rewardId + "|" + rewardItem.rewardEnchant;
                            xChild.Value = body;

                            id++;
                        }
                        if (id <= 0)
                        {
                            xReward.Remove();
                        }
                    }
                }
            }

            if (a.HasCorrectGiveItem)
            {
                if (!string.IsNullOrEmpty(a.giveItemType))
                {
                    XElement xgive = xAnswer.CreateChild("give");

                    xgive.CreateAttributeValue("type", a.giveItemType);
                    xgive.CreateAttributeValue("id", a.giveItemId);
                }
            }
        }

        internal void SortXML()
        {
            currentXDocument.Element("dialogs").RemoveAll();

            string[] ids = allDialogs.Keys.ToArray<string>();
            Array.Sort(ids);
            foreach (string s in ids)
            {
                Dialog d = allDialogs[s];
                AddUpdateDialog(d, false);
            }

            XMLDao.SaveDocument(currentXDocument, targetFileName);
        }
    }

    public static class XMLExtensions
    {
        public static int TryElementValueInt(this XElement e, string name)
        {
            XElement e2 = e.Element(name);
            if (e2 != null)
            {
                int res = 0;
                if (int.TryParse(e2.Value, out res))
                {
                    Console.WriteLine("Error, cant parse " + e2.Value);
                }
                return res;
            }
            return 0;
        }

        public static string TryElementValue(this XElement e, string name)
        {
            XElement e2 = e.Element(name);
            if (e2 != null)
            {
                return e2.Value;
            }
            return "";
        }

        public static string TryAttributeValue(this XElement e, string name)
        {
            XAttribute xa = e.Attribute(name);
            if (xa != null)
            {
                return xa.Value;
            }
            return "";
        }

        public static int TryAttributeValueInt(this XElement e, string name)
        {
            XAttribute xa = e.Attribute(name);
            if (xa != null)
            {
                int res = 0;
                if (int.TryParse(xa.Value, out res))
                {
                    Console.WriteLine("Error, cant parse " + xa.Value);
                }
                return res;
            }
            return 0;
        }

        public static XElement CreateChildBody(this XElement me, string name, string value)
        
        {
            if (value == null)
            {
                throw new Exception();
            }

            XElement nx = new XElement(name);
            me.Add(nx);
            nx.Value = value;
            return nx;
        }

        public static XElement CreateChildBodyIntBiggerZero(this XElement me, string name, int val)

        {
            if (val <= 0)
            {
                return null;
            }

            XElement nx = new XElement(name);
            me.Add(nx);
            nx.Value = val + "";
            return nx;
        }

        public static XAttribute CreateAttributeValue(this XElement me, string name, string value)
        {
            if (string.IsNullOrEmpty (value))
            {
                return null;
            }

            if (value == null)
            {
                throw new Exception();
            }

            if (value == "")
            {
                return null;
            }

            XAttribute nx = new XAttribute(name, value);
            me.Add(nx);
            return nx;
        }

        public static XElement CreateChild(this XElement me, string name)
        {
            XElement nx = new XElement(name);
            me.Add(nx);
            return nx;
        }
    }  

}


