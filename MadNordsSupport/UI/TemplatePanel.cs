﻿using MadNordsSupport.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI
{
    public abstract class TemplatePanel<TData, TDao> : UserControl 
        where TData : TemplateItem
        where TDao : TemplateDAO<TData>
    {
        protected abstract TDao dao
        {
            get;
        }

        public abstract void UpdateDataByFields(TData data, bool includeId);
        protected abstract void DisplayData(TData l, bool includeId);

        protected abstract TextBox FileNameTextBox { get; }
        protected abstract Button AddUpdateButton { get;}
        protected abstract Button RemoveButton { get; }
        public abstract TextBox TextBoxDataId { get; }

        protected void TryFileOnStart()
        {
            string file = dao.DataSourceName;
            TryReadFile(file);
        }

        protected bool SelectDataFile(OpenFileDialog openFileDialog)
        {
            bool res = false;
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog.FileName;
                if (TryReadFile(file))
                {
                    res = true;
                }
            }
            return res;
        }

        protected bool TryReadFile(string file)
        {
            try
            {
                if (this.dao.TryReadFrom(file))
                {
                    this.FileNameTextBox.Text = file;
                    this.AddUpdateButton.Enabled = true;
                    RenewAllFieldsByData();

                    UpdateIdTextBoxAutoComplete();
                    return true;
                }
            }
            catch (IOException)
            {
            }
            return false;
        }

        protected bool isRenewingFields = false;
        protected bool isRenewingId = false;

        protected void RenewAllFieldsByData()
        {
            if (isRenewingFields) { return; }
            else { isRenewingFields = true; }

            TData l = null;
            if (this.dao.allData.ContainsKey(TextBoxDataId.Text))
            {
                l = dao.allData[TextBoxDataId.Text];
            }
            else
            {
                l = dao.CreateNew ();
                int questPlus = this.dao.allData.Count + 1;
                l.Id = "" + (questPlus) + "";
                if (dao.allData.ContainsKey(l.Id))
                {
                    questPlus = questPlus + 1;
                    l.Id = "quest" + (questPlus) + "";
                }
            }

            DisplayData(l, !isRenewingId);

            isRenewingFields = false;
        }

        protected virtual void UpdateIdTextBoxAutoComplete()
        {
            AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();
            allowedTypes.AddRange(this.dao.allData.Keys.ToArray());
            TextBoxDataId.AutoCompleteCustomSource = allowedTypes;
            TextBoxDataId.AutoCompleteMode = AutoCompleteMode.Suggest;
            TextBoxDataId.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

 
        protected void RemoveCurrentEntity()
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                dao.Delete(this.TextBoxDataId.Text);

                if (true)
                {
                    UpdateIdTextBoxAutoComplete();
                }
                this.TextBoxDataId.Text = "new one";
                this.RenewAllFieldsByData();
            }
        }

        protected bool AddUpdateCurrentData()
        {
            TData d = dao.CreateNew ();
            UpdateDataByFields(d, true);

            bool isNew = !this.dao.allData.ContainsKey(d.Id);

            if (this.dao.AddUpdate(d))
            {
                if (isNew)
                {
                    UpdateIdTextBoxAutoComplete();
                    TextBoxIdChanged();
                }
                 this.RenewAllFieldsByData();
                return true;
            }
            return false;
        }

        protected bool TextBoxIdChanged ()
        {
            bool newOne = false;
            isRenewingId = true;
            if (dao.allData.ContainsKey(TextBoxDataId.Text))
            {
                AddUpdateButton.Text = "Update";
                RemoveButton.Enabled = true;
            }
            else
            {
                AddUpdateButton.Text = "Add";
                RemoveButton.Enabled = false;
                newOne = true;
            }

            RenewAllFieldsByData();
            isRenewingId = false;
            return newOne;
        }
    }
}
