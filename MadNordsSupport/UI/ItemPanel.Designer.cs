﻿namespace MadNordsSupport.UI
{
    partial class ItemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EntityFileButton = new System.Windows.Forms.Button();
            this.textBoxLocationFileName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_parametersST = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersAG = new System.Windows.Forms.NumericUpDown();
            this.buttonAddUpdateEntity = new System.Windows.Forms.Button();
            this.buttonPasteEntity = new System.Windows.Forms.Button();
            this.buttonCopyEntity = new System.Windows.Forms.Button();
            this.buttonRemoveEntity = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxItemTitle = new System.Windows.Forms.TextBox();
            this.groupBoxParameters = new System.Windows.Forms.GroupBox();
            this.numericUpDown_parametersMR = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersPR = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersCC = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersDC = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersAC = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersWI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersVI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_parametersIN = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpItemId = new System.Windows.Forms.NumericUpDown();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelItemsCount = new System.Windows.Forms.Label();
            this.groupBoxMisc = new System.Windows.Forms.GroupBox();
            this.numericUpDown_miscCost = new System.Windows.Forms.NumericUpDown();
            this.groupBoxPotion = new System.Windows.Forms.GroupBox();
            this.numericUpDown_potionMP = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_potionHP = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_potionWI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_potionVI = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_potionIN = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.numericUpDown_potionST = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numericUpDown_potionAG = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersAG)).BeginInit();
            this.groupBoxParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersAC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersWI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersVI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpItemId)).BeginInit();
            this.groupBoxMisc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_miscCost)).BeginInit();
            this.groupBoxPotion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionWI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionVI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionAG)).BeginInit();
            this.SuspendLayout();
            // 
            // EntityFileButton
            // 
            this.EntityFileButton.Location = new System.Drawing.Point(23, 3);
            this.EntityFileButton.Name = "EntityFileButton";
            this.EntityFileButton.Size = new System.Drawing.Size(99, 23);
            this.EntityFileButton.TabIndex = 51;
            this.EntityFileButton.Text = "Select Item File";
            this.EntityFileButton.UseVisualStyleBackColor = true;
            this.EntityFileButton.Click += new System.EventHandler(this.EntityFileButton_Click);
            // 
            // textBoxLocationFileName
            // 
            this.textBoxLocationFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLocationFileName.Location = new System.Drawing.Point(128, 5);
            this.textBoxLocationFileName.Name = "textBoxLocationFileName";
            this.textBoxLocationFileName.ReadOnly = true;
            this.textBoxLocationFileName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxLocationFileName.Size = new System.Drawing.Size(678, 20);
            this.textBoxLocationFileName.TabIndex = 52;
            this.textBoxLocationFileName.Text = "E";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(103, 61);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 54;
            this.label6.Text = "Id";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxDescription.Location = new System.Drawing.Point(128, 110);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxDescription.Size = new System.Drawing.Size(687, 20);
            this.textBoxDescription.TabIndex = 55;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "ST";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "AG";
            // 
            // numericUpDown_parametersST
            // 
            this.numericUpDown_parametersST.Location = new System.Drawing.Point(34, 19);
            this.numericUpDown_parametersST.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numericUpDown_parametersST.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersST.Name = "numericUpDown_parametersST";
            this.numericUpDown_parametersST.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersST.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersST.TabIndex = 59;
            // 
            // numericUpDown_parametersAG
            // 
            this.numericUpDown_parametersAG.Location = new System.Drawing.Point(34, 45);
            this.numericUpDown_parametersAG.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersAG.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersAG.Name = "numericUpDown_parametersAG";
            this.numericUpDown_parametersAG.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersAG.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersAG.TabIndex = 60;
            // 
            // buttonAddUpdateEntity
            // 
            this.buttonAddUpdateEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateEntity.Location = new System.Drawing.Point(716, 454);
            this.buttonAddUpdateEntity.Name = "buttonAddUpdateEntity";
            this.buttonAddUpdateEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateEntity.TabIndex = 61;
            this.buttonAddUpdateEntity.Text = "Add Item";
            this.buttonAddUpdateEntity.UseVisualStyleBackColor = true;
            this.buttonAddUpdateEntity.Click += new System.EventHandler(this.buttonAddUpdateEntity_Click);
            // 
            // buttonPasteEntity
            // 
            this.buttonPasteEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteEntity.Enabled = false;
            this.buttonPasteEntity.Location = new System.Drawing.Point(117, 454);
            this.buttonPasteEntity.Name = "buttonPasteEntity";
            this.buttonPasteEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteEntity.TabIndex = 62;
            this.buttonPasteEntity.Text = "Paste Item";
            this.buttonPasteEntity.UseVisualStyleBackColor = true;
            // 
            // buttonCopyEntity
            // 
            this.buttonCopyEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyEntity.Enabled = false;
            this.buttonCopyEntity.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyEntity.Name = "buttonCopyEntity";
            this.buttonCopyEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyEntity.TabIndex = 63;
            this.buttonCopyEntity.Text = "Copy Item";
            this.buttonCopyEntity.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveEntity
            // 
            this.buttonRemoveEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveEntity.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveEntity.Name = "buttonRemoveEntity";
            this.buttonRemoveEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveEntity.TabIndex = 64;
            this.buttonRemoveEntity.Text = "Delete Item";
            this.buttonRemoveEntity.UseVisualStyleBackColor = true;
            this.buttonRemoveEntity.Click += new System.EventHandler(this.buttonRemoveEntity_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "ItemType";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Title";
            // 
            // textBoxItemTitle
            // 
            this.textBoxItemTitle.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxItemTitle.Location = new System.Drawing.Point(128, 84);
            this.textBoxItemTitle.Name = "textBoxItemTitle";
            this.textBoxItemTitle.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxItemTitle.Size = new System.Drawing.Size(227, 20);
            this.textBoxItemTitle.TabIndex = 68;
            this.textBoxItemTitle.TextChanged += new System.EventHandler(this.textBoxItemTitle_TextChanged);
            // 
            // groupBoxParameters
            // 
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersMR);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersPR);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersCC);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersDC);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersAC);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersWI);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersVI);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersIN);
            this.groupBoxParameters.Controls.Add(this.label14);
            this.groupBoxParameters.Controls.Add(this.label13);
            this.groupBoxParameters.Controls.Add(this.label12);
            this.groupBoxParameters.Controls.Add(this.label11);
            this.groupBoxParameters.Controls.Add(this.label10);
            this.groupBoxParameters.Controls.Add(this.label9);
            this.groupBoxParameters.Controls.Add(this.label8);
            this.groupBoxParameters.Controls.Add(this.label7);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersST);
            this.groupBoxParameters.Controls.Add(this.label2);
            this.groupBoxParameters.Controls.Add(this.label3);
            this.groupBoxParameters.Controls.Add(this.numericUpDown_parametersAG);
            this.groupBoxParameters.Location = new System.Drawing.Point(128, 136);
            this.groupBoxParameters.Name = "groupBoxParameters";
            this.groupBoxParameters.Size = new System.Drawing.Size(169, 284);
            this.groupBoxParameters.TabIndex = 70;
            this.groupBoxParameters.TabStop = false;
            this.groupBoxParameters.Text = "Parameters";
            // 
            // numericUpDown_parametersMR
            // 
            this.numericUpDown_parametersMR.Location = new System.Drawing.Point(34, 254);
            this.numericUpDown_parametersMR.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersMR.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersMR.Name = "numericUpDown_parametersMR";
            this.numericUpDown_parametersMR.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersMR.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersMR.TabIndex = 76;
            // 
            // numericUpDown_parametersPR
            // 
            this.numericUpDown_parametersPR.Location = new System.Drawing.Point(34, 228);
            this.numericUpDown_parametersPR.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersPR.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersPR.Name = "numericUpDown_parametersPR";
            this.numericUpDown_parametersPR.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersPR.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersPR.TabIndex = 75;
            // 
            // numericUpDown_parametersCC
            // 
            this.numericUpDown_parametersCC.Location = new System.Drawing.Point(34, 202);
            this.numericUpDown_parametersCC.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersCC.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersCC.Name = "numericUpDown_parametersCC";
            this.numericUpDown_parametersCC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersCC.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersCC.TabIndex = 74;
            // 
            // numericUpDown_parametersDC
            // 
            this.numericUpDown_parametersDC.Location = new System.Drawing.Point(34, 176);
            this.numericUpDown_parametersDC.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersDC.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersDC.Name = "numericUpDown_parametersDC";
            this.numericUpDown_parametersDC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersDC.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersDC.TabIndex = 73;
            // 
            // numericUpDown_parametersAC
            // 
            this.numericUpDown_parametersAC.Location = new System.Drawing.Point(34, 150);
            this.numericUpDown_parametersAC.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersAC.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersAC.Name = "numericUpDown_parametersAC";
            this.numericUpDown_parametersAC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersAC.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersAC.TabIndex = 72;
            // 
            // numericUpDown_parametersWI
            // 
            this.numericUpDown_parametersWI.Location = new System.Drawing.Point(34, 124);
            this.numericUpDown_parametersWI.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersWI.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersWI.Name = "numericUpDown_parametersWI";
            this.numericUpDown_parametersWI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersWI.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersWI.TabIndex = 71;
            // 
            // numericUpDown_parametersVI
            // 
            this.numericUpDown_parametersVI.Location = new System.Drawing.Point(34, 98);
            this.numericUpDown_parametersVI.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersVI.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersVI.Name = "numericUpDown_parametersVI";
            this.numericUpDown_parametersVI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersVI.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersVI.TabIndex = 70;
            // 
            // numericUpDown_parametersIN
            // 
            this.numericUpDown_parametersIN.Location = new System.Drawing.Point(34, 72);
            this.numericUpDown_parametersIN.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_parametersIN.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_parametersIN.Name = "numericUpDown_parametersIN";
            this.numericUpDown_parametersIN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_parametersIN.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_parametersIN.TabIndex = 69;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 256);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 68;
            this.label14.Text = "MR";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 230);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 13);
            this.label13.TabIndex = 67;
            this.label13.Text = "PR";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 204);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "CC";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 65;
            this.label11.Text = "DC";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 64;
            this.label10.Text = "AC";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "WI";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 62;
            this.label8.Text = "VI";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 61;
            this.label7.Text = "IN";
            // 
            // numericUpItemId
            // 
            this.numericUpItemId.Location = new System.Drawing.Point(128, 58);
            this.numericUpItemId.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpItemId.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpItemId.Name = "numericUpItemId";
            this.numericUpItemId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpItemId.Size = new System.Drawing.Size(227, 20);
            this.numericUpItemId.TabIndex = 71;
            this.numericUpItemId.ValueChanged += new System.EventHandler(this.numericUpItemId_ValueChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "money",
            "xp",
            "item",
            "stat_strength",
            "stat_agility",
            "stat_intellect",
            "stat_vitality",
            "stat_wisdom",
            "stat_HP",
            "stat_MP",
            "key"});
            this.comboBox1.Location = new System.Drawing.Point(128, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox1.Size = new System.Drawing.Size(227, 21);
            this.comboBox1.TabIndex = 72;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // labelItemsCount
            // 
            this.labelItemsCount.AutoSize = true;
            this.labelItemsCount.Location = new System.Drawing.Point(362, 34);
            this.labelItemsCount.Name = "labelItemsCount";
            this.labelItemsCount.Size = new System.Drawing.Size(75, 13);
            this.labelItemsCount.TabIndex = 73;
            this.labelItemsCount.Text = "Items Count = ";
            // 
            // groupBoxMisc
            // 
            this.groupBoxMisc.Controls.Add(this.numericUpDown_miscCost);
            this.groupBoxMisc.Location = new System.Drawing.Point(128, 136);
            this.groupBoxMisc.Name = "groupBoxMisc";
            this.groupBoxMisc.Size = new System.Drawing.Size(169, 50);
            this.groupBoxMisc.TabIndex = 77;
            this.groupBoxMisc.TabStop = false;
            this.groupBoxMisc.Text = "Cost";
            this.groupBoxMisc.Enter += new System.EventHandler(this.groupBoxMisc_Enter);
            // 
            // numericUpDown_miscCost
            // 
            this.numericUpDown_miscCost.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown_miscCost.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numericUpDown_miscCost.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_miscCost.Name = "numericUpDown_miscCost";
            this.numericUpDown_miscCost.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_miscCost.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_miscCost.TabIndex = 77;
            // 
            // groupBoxPotion
            // 
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionMP);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionHP);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionWI);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionVI);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionIN);
            this.groupBoxPotion.Controls.Add(this.label18);
            this.groupBoxPotion.Controls.Add(this.label19);
            this.groupBoxPotion.Controls.Add(this.label20);
            this.groupBoxPotion.Controls.Add(this.label21);
            this.groupBoxPotion.Controls.Add(this.label22);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionST);
            this.groupBoxPotion.Controls.Add(this.label23);
            this.groupBoxPotion.Controls.Add(this.label24);
            this.groupBoxPotion.Controls.Add(this.numericUpDown_potionAG);
            this.groupBoxPotion.Location = new System.Drawing.Point(128, 136);
            this.groupBoxPotion.Name = "groupBoxPotion";
            this.groupBoxPotion.Size = new System.Drawing.Size(169, 204);
            this.groupBoxPotion.TabIndex = 77;
            this.groupBoxPotion.TabStop = false;
            this.groupBoxPotion.Text = "Parameters";
            this.groupBoxPotion.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // numericUpDown_potionMP
            // 
            this.numericUpDown_potionMP.Location = new System.Drawing.Point(34, 45);
            this.numericUpDown_potionMP.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionMP.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionMP.Name = "numericUpDown_potionMP";
            this.numericUpDown_potionMP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionMP.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionMP.TabIndex = 73;
            // 
            // numericUpDown_potionHP
            // 
            this.numericUpDown_potionHP.Location = new System.Drawing.Point(34, 19);
            this.numericUpDown_potionHP.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionHP.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionHP.Name = "numericUpDown_potionHP";
            this.numericUpDown_potionHP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionHP.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionHP.TabIndex = 72;
            // 
            // numericUpDown_potionWI
            // 
            this.numericUpDown_potionWI.Location = new System.Drawing.Point(34, 176);
            this.numericUpDown_potionWI.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionWI.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionWI.Name = "numericUpDown_potionWI";
            this.numericUpDown_potionWI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionWI.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionWI.TabIndex = 71;
            // 
            // numericUpDown_potionVI
            // 
            this.numericUpDown_potionVI.Location = new System.Drawing.Point(34, 150);
            this.numericUpDown_potionVI.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionVI.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionVI.Name = "numericUpDown_potionVI";
            this.numericUpDown_potionVI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionVI.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionVI.TabIndex = 70;
            // 
            // numericUpDown_potionIN
            // 
            this.numericUpDown_potionIN.Location = new System.Drawing.Point(34, 124);
            this.numericUpDown_potionIN.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionIN.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionIN.Name = "numericUpDown_potionIN";
            this.numericUpDown_potionIN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionIN.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionIN.TabIndex = 69;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 65;
            this.label18.Text = "MP";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 13);
            this.label19.TabIndex = 64;
            this.label19.Text = "HP";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 178);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 13);
            this.label20.TabIndex = 63;
            this.label20.Text = "WI";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 152);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 13);
            this.label21.TabIndex = 62;
            this.label21.Text = "VI";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 126);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(18, 13);
            this.label22.TabIndex = 61;
            this.label22.Text = "IN";
            // 
            // numericUpDown_potionST
            // 
            this.numericUpDown_potionST.Location = new System.Drawing.Point(34, 71);
            this.numericUpDown_potionST.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numericUpDown_potionST.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionST.Name = "numericUpDown_potionST";
            this.numericUpDown_potionST.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionST.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionST.TabIndex = 59;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(21, 13);
            this.label23.TabIndex = 57;
            this.label23.Text = "ST";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 99);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 58;
            this.label24.Text = "AG";
            // 
            // numericUpDown_potionAG
            // 
            this.numericUpDown_potionAG.Location = new System.Drawing.Point(34, 97);
            this.numericUpDown_potionAG.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDown_potionAG.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDown_potionAG.Name = "numericUpDown_potionAG";
            this.numericUpDown_potionAG.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDown_potionAG.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_potionAG.TabIndex = 60;
            // 
            // ItemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxPotion);
            this.Controls.Add(this.groupBoxMisc);
            this.Controls.Add(this.labelItemsCount);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.numericUpItemId);
            this.Controls.Add(this.groupBoxParameters);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxItemTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonRemoveEntity);
            this.Controls.Add(this.buttonCopyEntity);
            this.Controls.Add(this.buttonPasteEntity);
            this.Controls.Add(this.buttonAddUpdateEntity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxLocationFileName);
            this.Controls.Add(this.EntityFileButton);
            this.Name = "ItemPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.ItemPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersAG)).EndInit();
            this.groupBoxParameters.ResumeLayout(false);
            this.groupBoxParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersAC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersWI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersVI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_parametersIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpItemId)).EndInit();
            this.groupBoxMisc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_miscCost)).EndInit();
            this.groupBoxPotion.ResumeLayout(false);
            this.groupBoxPotion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionWI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionVI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_potionAG)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EntityFileButton;
        private System.Windows.Forms.TextBox textBoxLocationFileName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersST;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersAG;
        private System.Windows.Forms.Button buttonAddUpdateEntity;
        private System.Windows.Forms.Button buttonPasteEntity;
        private System.Windows.Forms.Button buttonCopyEntity;
        private System.Windows.Forms.Button buttonRemoveEntity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxItemTitle;
        private System.Windows.Forms.GroupBox groupBoxParameters;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersMR;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersPR;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersCC;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersDC;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersAC;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersWI;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersVI;
        private System.Windows.Forms.NumericUpDown numericUpDown_parametersIN;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpItemId;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelItemsCount;
        private System.Windows.Forms.GroupBox groupBoxMisc;
        private System.Windows.Forms.GroupBox groupBoxPotion;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionMP;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionHP;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionWI;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionVI;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionIN;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionST;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDown_potionAG;
        private System.Windows.Forms.NumericUpDown numericUpDown_miscCost;
    }
}
