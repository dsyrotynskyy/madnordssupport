﻿namespace MadNordsSupport.UI
{
    partial class MerchantPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRemoveEntity = new System.Windows.Forms.Button();
            this.buttonCopyEntity = new System.Windows.Forms.Button();
            this.buttonPasteEntity = new System.Windows.Forms.Button();
            this.buttonAddUpdateEntity = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAnswer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEntityId = new System.Windows.Forms.TextBox();
            this.textBoxLocationFileName = new System.Windows.Forms.TextBox();
            this.entityFileButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBoxWares = new System.Windows.Forms.GroupBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox0 = new System.Windows.Forms.ComboBox();
            this.textBox0 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxWares.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRemoveEntity
            // 
            this.buttonRemoveEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveEntity.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveEntity.Name = "buttonRemoveEntity";
            this.buttonRemoveEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveEntity.TabIndex = 78;
            this.buttonRemoveEntity.Text = "Delete Item";
            this.buttonRemoveEntity.UseVisualStyleBackColor = true;
            this.buttonRemoveEntity.Click += new System.EventHandler(this.buttonRemoveEntity_Click);
            // 
            // buttonCopyEntity
            // 
            this.buttonCopyEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyEntity.Enabled = false;
            this.buttonCopyEntity.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyEntity.Name = "buttonCopyEntity";
            this.buttonCopyEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyEntity.TabIndex = 77;
            this.buttonCopyEntity.Text = "Copy Item";
            this.buttonCopyEntity.UseVisualStyleBackColor = true;
            // 
            // buttonPasteEntity
            // 
            this.buttonPasteEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteEntity.Enabled = false;
            this.buttonPasteEntity.Location = new System.Drawing.Point(117, 454);
            this.buttonPasteEntity.Name = "buttonPasteEntity";
            this.buttonPasteEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteEntity.TabIndex = 76;
            this.buttonPasteEntity.Text = "Paste Item";
            this.buttonPasteEntity.UseVisualStyleBackColor = true;
            // 
            // buttonAddUpdateEntity
            // 
            this.buttonAddUpdateEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateEntity.Location = new System.Drawing.Point(716, 454);
            this.buttonAddUpdateEntity.Name = "buttonAddUpdateEntity";
            this.buttonAddUpdateEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateEntity.TabIndex = 75;
            this.buttonAddUpdateEntity.Text = "Add Item";
            this.buttonAddUpdateEntity.UseVisualStyleBackColor = true;
            this.buttonAddUpdateEntity.Click += new System.EventHandler(this.buttonAddUpdateEntity_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "Answer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxAnswer
            // 
            this.textBoxAnswer.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxAnswer.Location = new System.Drawing.Point(128, 57);
            this.textBoxAnswer.Name = "textBoxAnswer";
            this.textBoxAnswer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxAnswer.Size = new System.Drawing.Size(319, 20);
            this.textBoxAnswer.TabIndex = 69;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(106, 34);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "Id";
            // 
            // textBoxEntityId
            // 
            this.textBoxEntityId.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxEntityId.Location = new System.Drawing.Point(128, 31);
            this.textBoxEntityId.Name = "textBoxEntityId";
            this.textBoxEntityId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxEntityId.Size = new System.Drawing.Size(131, 20);
            this.textBoxEntityId.TabIndex = 67;
            this.textBoxEntityId.TextChanged += new System.EventHandler(this.textBoxEntityId_TextChanged);
            // 
            // textBoxLocationFileName
            // 
            this.textBoxLocationFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLocationFileName.Location = new System.Drawing.Point(128, 5);
            this.textBoxLocationFileName.Name = "textBoxLocationFileName";
            this.textBoxLocationFileName.ReadOnly = true;
            this.textBoxLocationFileName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxLocationFileName.Size = new System.Drawing.Size(678, 20);
            this.textBoxLocationFileName.TabIndex = 66;
            this.textBoxLocationFileName.Text = "E";
            // 
            // entityFileButton
            // 
            this.entityFileButton.Location = new System.Drawing.Point(23, 3);
            this.entityFileButton.Name = "entityFileButton";
            this.entityFileButton.Size = new System.Drawing.Size(99, 23);
            this.entityFileButton.TabIndex = 65;
            this.entityFileButton.Text = "Merchant File";
            this.entityFileButton.UseVisualStyleBackColor = true;
            this.entityFileButton.Click += new System.EventHandler(this.entityFileButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 71;
            this.label2.Text = "1";
            // 
            // textBox1
            // 
            this.textBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox1.Location = new System.Drawing.Point(137, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox1.Size = new System.Drawing.Size(182, 20);
            this.textBox1.TabIndex = 79;
            // 
            // groupBoxWares
            // 
            this.groupBoxWares.Controls.Add(this.comboBox6);
            this.groupBoxWares.Controls.Add(this.comboBox5);
            this.groupBoxWares.Controls.Add(this.comboBox4);
            this.groupBoxWares.Controls.Add(this.comboBox3);
            this.groupBoxWares.Controls.Add(this.comboBox2);
            this.groupBoxWares.Controls.Add(this.comboBox1);
            this.groupBoxWares.Controls.Add(this.comboBox0);
            this.groupBoxWares.Controls.Add(this.textBox0);
            this.groupBoxWares.Controls.Add(this.label9);
            this.groupBoxWares.Controls.Add(this.textBox6);
            this.groupBoxWares.Controls.Add(this.label8);
            this.groupBoxWares.Controls.Add(this.textBox5);
            this.groupBoxWares.Controls.Add(this.label7);
            this.groupBoxWares.Controls.Add(this.textBox4);
            this.groupBoxWares.Controls.Add(this.label5);
            this.groupBoxWares.Controls.Add(this.textBox3);
            this.groupBoxWares.Controls.Add(this.label4);
            this.groupBoxWares.Controls.Add(this.textBox2);
            this.groupBoxWares.Controls.Add(this.label3);
            this.groupBoxWares.Controls.Add(this.textBox1);
            this.groupBoxWares.Controls.Add(this.label2);
            this.groupBoxWares.Location = new System.Drawing.Point(128, 83);
            this.groupBoxWares.Name = "groupBoxWares";
            this.groupBoxWares.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBoxWares.Size = new System.Drawing.Size(325, 204);
            this.groupBoxWares.TabIndex = 80;
            this.groupBoxWares.TabStop = false;
            this.groupBoxWares.Text = "Wares";
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(25, 173);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(106, 21);
            this.comboBox6.TabIndex = 98;
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(25, 147);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(106, 21);
            this.comboBox5.TabIndex = 97;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(25, 122);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(106, 21);
            this.comboBox4.TabIndex = 96;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(25, 96);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(106, 21);
            this.comboBox3.TabIndex = 95;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(25, 71);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(106, 21);
            this.comboBox2.TabIndex = 94;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(25, 45);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(106, 21);
            this.comboBox1.TabIndex = 93;
            // 
            // comboBox0
            // 
            this.comboBox0.FormattingEnabled = true;
            this.comboBox0.Location = new System.Drawing.Point(25, 18);
            this.comboBox0.Name = "comboBox0";
            this.comboBox0.Size = new System.Drawing.Size(106, 21);
            this.comboBox0.TabIndex = 92;
            // 
            // textBox0
            // 
            this.textBox0.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox0.Location = new System.Drawing.Point(137, 19);
            this.textBox0.Name = "textBox0";
            this.textBox0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox0.Size = new System.Drawing.Size(182, 20);
            this.textBox0.TabIndex = 91;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 90;
            this.label9.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox6.Location = new System.Drawing.Point(137, 174);
            this.textBox6.Name = "textBox6";
            this.textBox6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox6.Size = new System.Drawing.Size(182, 20);
            this.textBox6.TabIndex = 89;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 88;
            this.label8.Text = "6";
            // 
            // textBox5
            // 
            this.textBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox5.Location = new System.Drawing.Point(137, 148);
            this.textBox5.Name = "textBox5";
            this.textBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox5.Size = new System.Drawing.Size(182, 20);
            this.textBox5.TabIndex = 87;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "5";
            // 
            // textBox4
            // 
            this.textBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox4.Location = new System.Drawing.Point(137, 122);
            this.textBox4.Name = "textBox4";
            this.textBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox4.Size = new System.Drawing.Size(182, 20);
            this.textBox4.TabIndex = 85;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 84;
            this.label5.Text = "4";
            // 
            // textBox3
            // 
            this.textBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox3.Location = new System.Drawing.Point(137, 96);
            this.textBox3.Name = "textBox3";
            this.textBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox3.Size = new System.Drawing.Size(182, 20);
            this.textBox3.TabIndex = 83;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 82;
            this.label4.Text = "3";
            // 
            // textBox2
            // 
            this.textBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBox2.Location = new System.Drawing.Point(137, 71);
            this.textBox2.Name = "textBox2";
            this.textBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox2.Size = new System.Drawing.Size(182, 20);
            this.textBox2.TabIndex = 81;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 80;
            this.label3.Text = "2";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MerchantPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxWares);
            this.Controls.Add(this.buttonRemoveEntity);
            this.Controls.Add(this.buttonCopyEntity);
            this.Controls.Add(this.buttonPasteEntity);
            this.Controls.Add(this.buttonAddUpdateEntity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAnswer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxEntityId);
            this.Controls.Add(this.textBoxLocationFileName);
            this.Controls.Add(this.entityFileButton);
            this.Name = "MerchantPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.MerchantPanel_Load);
            this.groupBoxWares.ResumeLayout(false);
            this.groupBoxWares.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRemoveEntity;
        private System.Windows.Forms.Button buttonCopyEntity;
        private System.Windows.Forms.Button buttonPasteEntity;
        private System.Windows.Forms.Button buttonAddUpdateEntity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAnswer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxEntityId;
        private System.Windows.Forms.TextBox textBoxLocationFileName;
        private System.Windows.Forms.Button entityFileButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBoxWares;
        private System.Windows.Forms.TextBox textBox0;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox comboBox0;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}
