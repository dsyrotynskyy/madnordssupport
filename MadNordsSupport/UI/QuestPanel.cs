﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.Logic;
using System.IO;

namespace MadNordsSupport
{
    public partial class QuestPanel : UserControl
    {
        QuestDAO questDAO
        {
            get
            {
                return QuestDAO.Instance;
            }
        }

        Dictionary<int, string> markersDictionary = new Dictionary<int, string>();
        Dictionary<int, string> descriptionsDictionary
            = new Dictionary<int, string>();

        public QuestPanel()
        {
            InitializeComponent();
            this.markersTextBoxColumnId.Width = 100;

            dataGridDescriptions.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridDescriptions.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridDescriptions.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridDescriptions.Columns[1].CellTemplate = new CustomDataGridViewTextBoxCell();

            buttonAddUpdateQuest.Enabled = false;
            TryFileOnStart();
        }

        void TryFileOnStart()
        {
            string file = MadNordsSettings.Default.QuestFileName;
            TryReadFile(file);
        }

        bool TryReadFile(string file)
        {
            try
            {
                if (this.questDAO.TryReadFrom(file))
                {
                    this.questFileName.Text = file;
                    this.questCountLabel.Text = "Total quest count: " + this.questDAO.allQuests.Count + "";
                    buttonAddUpdateQuest.Enabled = true;
                    RenewAllFieldsByData();

                    UpdateQuestTextBoxAutoComplete();
                    return true;
                }
            }
            catch (IOException)
            {
            }
            return false;
        }

        void UpdateQuestTextBoxAutoComplete()
        {
            AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();
            allowedTypes.AddRange(this.questDAO.allQuests.Keys.ToArray());
            textBoxQuestId.AutoCompleteCustomSource = allowedTypes;
            textBoxQuestId.AutoCompleteMode = AutoCompleteMode.Suggest;
            textBoxQuestId.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        bool SelectQuestFile()
        {
            bool res = false;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                if (TryReadFile(file))
                {
                    res = true;
                }
            }
            return res;
        }

        bool isRenewingFields = false;
        bool isRenewingId = false;

        void RenewAllFieldsByData()
        {
            if (isRenewingFields) { return; }
            else { isRenewingFields = true; }

            Quest q = null;
            if (this.questDAO.allQuests.ContainsKey(textBoxQuestId.Text))
            {
                q = this.questDAO.allQuests[textBoxQuestId.Text];
            }
            else
            {
                q = new Quest();
                int questPlus = this.questDAO.allQuests.Count + 1;
                q.id = "quest" + (questPlus) + "";
                if (questDAO.allQuests.ContainsKey(q.id))
                {
                    questPlus = questPlus + 1;
                    q.id = "quest" + (questPlus) + "";
                }

                q.XPValue = 0;
 
                q.descriptions.Add(new string[2] { "start", ""});
                q.marks.Add (new string[2] {"", ""});                    
            }

            if (!isRenewingId)
            {
                textBoxQuestId.Text = q.id;                    
            }
            DisplayFieldsByData(q);

            isRenewingFields = false;
        }

        void DisplayFieldsByData(Quest q)
        {
            dataGridMarkers.Rows.Clear();
            dataGridDescriptions.Rows.Clear();

            textBoxQuestName.Text = q.name;

            textBoxXPValue.Value = q.XPValue;
   
            isMainQuest.Checked = (q.type == "main");

            foreach (string[] vals in q.marks)
            {
                if (vals.Length == 2)
                {
                    dataGridMarkers.Rows.Add(vals[0], vals[1]);
                }

                if (vals.Length == 3)
                {
                    dataGridMarkers.Rows.Add(vals[0], vals[1], true);
                }
            }

            foreach (string[] vals in q.descriptions)
            {
                dataGridDescriptions.Rows.Add(vals[0], vals[1]);
            }

   
            //foreach (DataGridViewRow row in dataGridMarkers.Rows)
            //{
            //    foreach (DataGridViewCell cell in row.Cells)
            //    {
            //        if (cell.OwningColumn.Name == "Value") 
            //        {

            //            //TextBox tx = (TextBox)cell;
                        
            //            //tx.AutoCompleteCustomSource = allowedTypes;
            //            //tx.AutoCompleteMode = AutoCompleteMode.Suggest;
            //            //tx.AutoCompleteSource = AutoCompleteSource.CustomSource;
            //        }
            //    }
            //}
        }

        AutoCompleteStringCollection _locationCollection = new AutoCompleteStringCollection();

        public AutoCompleteStringCollection GetLocationList()
        {
            if (_locationCollection.Count != LocationDAO.Instance.allData.Keys.Count)
            {
                _locationCollection = new AutoCompleteStringCollection();
                _locationCollection.AddRange(LocationDAO.Instance.allData.Keys.ToArray());
            }

            return _locationCollection;
        }

        void InitQuestByPanelFields(Quest q)
        {
            q.name = textBoxQuestName.Text;
            q.XPValue = Decimal.ToInt32(textBoxXPValue.Value);
   
            q.marks.Clear();
            q.descriptions.Clear();

            foreach (DataGridViewRow r in dataGridMarkers.Rows)
            {
                if (r.Cells[1].Value == null || r.Cells[0].Value == null)
                {
                    continue;
                }
                string[] vals;
                if (r.Cells[2].Value != null && (bool)r.Cells[2].Value == true)
                {
                    vals = new string[] { (string)r.Cells[0].Value, (string)r.Cells[1].Value, "true" };
                }
                else
                {
                    vals = new string[] { (string)r.Cells[0].Value, (string)r.Cells[1].Value };

                }
                q.marks.Add(vals);
            }

            foreach (DataGridViewRow r in dataGridDescriptions.Rows)
            {
                if (r.Cells[1].Value == null || r.Cells[0].Value == null)
                {
                    continue;
                }
                string[] vals = new string[] { (string)r.Cells[0].Value, (string)r.Cells[1].Value };

                q.descriptions.Add(vals);
            }

            if (isMainQuest.Checked) { q.type = "main"; }
            else { q.type = "side"; }
        }

        void AddUpdateQuest()
        {
            Quest q = new Quest ();
            q.id = textBoxQuestId.Text;
            InitQuestByPanelFields(q);

            bool isNewQuest = !this.questDAO.allQuests.ContainsKey(q.id);

            if (this.questDAO.AddUpdateQuest(q))
            {
                if (isNewQuest)
                {
                    UpdateQuestTextBoxAutoComplete();
                }
                this.RenewAllFieldsByData();
            }

            this.questCountLabel.Text = "Total quest count: " + this.questDAO.allQuests.Count + "";
        }

        private int descrRowIndex = 0;
        private int markersRowIndex = 0;

        void DisplayRowDeleteMenu(DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.dataGridMarkers.Rows[e.RowIndex].Selected = true;
                this.markersRowIndex = e.RowIndex;
                this.dataGridMarkers.CurrentCell = this.dataGridMarkers.Rows[e.RowIndex].Cells[1];
                this.markersContextMenu.Show(this.dataGridMarkers, e.Location);
                markersContextMenu.Show(Cursor.Position);
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void addQuestButton_Click(object sender, EventArgs e)
        {
            AddUpdateQuest();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (SelectQuestFile())
            {
                RenewAllFieldsByData();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void isMainQuest_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void questsTabPage_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void dataGridMarkers_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            DisplayRowDeleteMenu(e);
        }

        private void deleteRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.dataGridMarkers.Rows[this.markersRowIndex].IsNewRow)
            {
                this.dataGridMarkers.Rows.RemoveAt(this.markersRowIndex);
            }

            for (int i = 0; (i <= (dataGridMarkers.Rows.Count - 1)); i++)
            {
                if (dataGridMarkers.Rows[i].Cells[0] != null)
                    dataGridMarkers.Rows[i].Cells[0].Value = string.Format((i).ToString(), "0");
            }
        }

        private void dataGridMarkers_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int i = 0; (i <= (dataGridMarkers.Rows.Count - 1)); i++)
            {
                if (dataGridMarkers.Rows[i].Cells[0] != null)
                    dataGridMarkers.Rows[i].Cells[0].Value = string.Format((i).ToString(), "0");
            }
        }

        private void dataGridDescriptions_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                int col = dataGridDescriptions.CurrentCell.ColumnIndex;
                int row = dataGridDescriptions.CurrentCell.RowIndex;

                e.Handled = true;
            }
        }

        private void dataGridDescriptions_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridDescriptions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridDescriptions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!this.dataGridDescriptions.Rows[this.descrRowIndex].IsNewRow)
            {
                this.dataGridDescriptions.Rows.RemoveAt(this.descrRowIndex);
            }
        }

        private void dataGridDescriptions_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.dataGridDescriptions.Rows[e.RowIndex].Selected = true;
                this.descrRowIndex = e.RowIndex;
                this.dataGridDescriptions.CurrentCell = this.dataGridDescriptions.Rows[e.RowIndex].Cells[1];
                this.descriptionsContextMenu.Show(this.dataGridDescriptions, e.Location);
                descriptionsContextMenu.Show(Cursor.Position);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void textBoxQuestId_TextChanged(object sender, EventArgs e)
        {
            isRenewingId = true;
            if (questDAO.allQuests.ContainsKey(textBoxQuestId.Text))
            {
                labelCreateUpdate.Text = "Update Quest";
                buttonAddUpdateQuest.Text = "Update Quest";
                buttonRemoveQuest.Enabled = true;
            }
            else
            {
                labelCreateUpdate.Text = "Create New Quest";
                buttonAddUpdateQuest.Text = "Add Quest";
                buttonRemoveQuest.Enabled = false;
            }

            RenewAllFieldsByData();
            isRenewingId = false;
        }

        private void textBoxQuestId_ControlRemoved(object sender, ControlEventArgs e)
        {

        }

        private void rewardEnchanment_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void QuestPanel_Load(object sender, EventArgs e)
        {

        }

        private void buttonPasteQuest_Click(object sender, EventArgs e)
        {
            DisplayFieldsByData(copyQuest);
        }

        Quest copyQuest = new Quest ();

        private void buttonCopyQuest_Click(object sender, EventArgs e)
        {
            this.buttonPasteQuest.Enabled = true;
            InitQuestByPanelFields(copyQuest);
        }

        private void textBoxRewardValue_TextChanged(object sender, EventArgs e)
        {
        }

        private void buttonRemoveQuest_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                QuestDAO.Instance.DeleteQuest(this.textBoxQuestId.Text);

                if (true)
                {
                    UpdateQuestTextBoxAutoComplete();
                }
                this.textBoxQuestId.Text = "new Quest";
                this.RenewAllFieldsByData();
            }
            else
            {
                // If 'No', do something here.
            }
        }

        private void questCountLabel_Click(object sender, EventArgs e)
        {

        }

        private void markersContextMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void addRowUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.dataGridMarkers.Rows[this.markersRowIndex].IsNewRow)
            {
                this.dataGridMarkers.Rows.Insert(this.markersRowIndex);
            }
        }

        private void addRowDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.dataGridMarkers.Rows[this.markersRowIndex].IsNewRow)
            {
                this.dataGridMarkers.Rows.Insert(this.markersRowIndex + 1);
            }
        }

        private void toolStripMenuItemAddRowUp_Click(object sender, EventArgs e)
        {
            if (!this.dataGridDescriptions.Rows[this.descrRowIndex].IsNewRow)
            {
                this.dataGridDescriptions.Rows.Insert(this.descrRowIndex);
            }
        }

        private void toolStripMenuItemAddRowDown_Click(object sender, EventArgs e)
        {
            if (!this.dataGridDescriptions.Rows[this.descrRowIndex].IsNewRow)
            {
                this.dataGridDescriptions.Rows.Insert(this.descrRowIndex + 1);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void dataGridMarkers_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int column = this.dataGridMarkers.CurrentCell.ColumnIndex;
            string headerText = dataGridMarkers.Columns[column].HeaderText;

            if (headerText.Equals("Value"))
            {
                TextBox tb = e.Control as TextBox;

                if (tb != null)
                {
                    tb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    tb.AutoCompleteCustomSource = GetLocationList();
                    tb.AutoCompleteSource = AutoCompleteSource.CustomSource;
                }
            }
        }
    }
}
