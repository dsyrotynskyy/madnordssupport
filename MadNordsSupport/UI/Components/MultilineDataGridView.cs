﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport
{
    public partial class CustomDataGridViewTextBoxEditingControl  : DataGridViewTextBoxEditingControl
    {
        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode & Keys.KeyCode)
            {
                case Keys.Enter:
                    int oldSelectionStart = this.SelectionStart;
                    string currentText = this.Text;

                    this.Text = String.Format("{0}{1}{2}",
                        currentText.Substring(0, this.SelectionStart),
                        Environment.NewLine,
                        currentText.Substring(this.SelectionStart + this.SelectionLength));

                    this.SelectionStart = oldSelectionStart + Environment.NewLine.Length;
                    break;
                default:
                    break;
            }

            base.OnKeyDown(e);
        }

        public override bool EditingControlWantsInputKey(
    Keys keyData,
    bool dataGridViewWantsInputKey)
        {
            switch (keyData & Keys.KeyCode)
            {
                case Keys.Enter:
                    // Don't let the DataGridView handle the Enter key.
                    return true;
                default:
                    break;
            }

            return base.EditingControlWantsInputKey(keyData, dataGridViewWantsInputKey);
        }
    }

    public partial class CustomDataGridViewTextBoxCell : DataGridViewTextBoxCell
    {
        public override Type EditType
        {
            get
            {
                return typeof(CustomDataGridViewTextBoxEditingControl);
            }
        }
    }
}
