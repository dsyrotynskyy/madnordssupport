﻿namespace MadNordsSupport.UI
{
    partial class SignPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRemoveEntity = new System.Windows.Forms.Button();
            this.buttonCopyEntity = new System.Windows.Forms.Button();
            this.buttonPasteEntity = new System.Windows.Forms.Button();
            this.buttonAddUpdateEntity = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEntityId = new System.Windows.Forms.TextBox();
            this.textBoxLocationFileName = new System.Windows.Forms.TextBox();
            this.entityFileButton = new System.Windows.Forms.Button();
            this.richTextBoxSignText = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // buttonRemoveEntity
            // 
            this.buttonRemoveEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveEntity.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveEntity.Name = "buttonRemoveEntity";
            this.buttonRemoveEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveEntity.TabIndex = 92;
            this.buttonRemoveEntity.Text = "Delete Sign";
            this.buttonRemoveEntity.UseVisualStyleBackColor = true;
            this.buttonRemoveEntity.Click += new System.EventHandler(this.buttonRemoveEntity_Click);
            // 
            // buttonCopyEntity
            // 
            this.buttonCopyEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyEntity.Enabled = false;
            this.buttonCopyEntity.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyEntity.Name = "buttonCopyEntity";
            this.buttonCopyEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyEntity.TabIndex = 91;
            this.buttonCopyEntity.Text = "Copy Sign";
            this.buttonCopyEntity.UseVisualStyleBackColor = true;
            this.buttonCopyEntity.Click += new System.EventHandler(this.buttonCopyEntity_Click);
            // 
            // buttonPasteEntity
            // 
            this.buttonPasteEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteEntity.Enabled = false;
            this.buttonPasteEntity.Location = new System.Drawing.Point(117, 454);
            this.buttonPasteEntity.Name = "buttonPasteEntity";
            this.buttonPasteEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteEntity.TabIndex = 90;
            this.buttonPasteEntity.Text = "Paste Sign";
            this.buttonPasteEntity.UseVisualStyleBackColor = true;
            this.buttonPasteEntity.Click += new System.EventHandler(this.buttonPasteEntity_Click);
            // 
            // buttonAddUpdateEntity
            // 
            this.buttonAddUpdateEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateEntity.Location = new System.Drawing.Point(716, 454);
            this.buttonAddUpdateEntity.Name = "buttonAddUpdateEntity";
            this.buttonAddUpdateEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateEntity.TabIndex = 89;
            this.buttonAddUpdateEntity.Text = "Add Sign";
            this.buttonAddUpdateEntity.UseVisualStyleBackColor = true;
            this.buttonAddUpdateEntity.Click += new System.EventHandler(this.buttonAddUpdateEntity_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 84;
            this.label1.Text = "Text";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(103, 34);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 82;
            this.label6.Text = "Id";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // textBoxEntityId
            // 
            this.textBoxEntityId.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxEntityId.Location = new System.Drawing.Point(128, 31);
            this.textBoxEntityId.Name = "textBoxEntityId";
            this.textBoxEntityId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxEntityId.Size = new System.Drawing.Size(227, 20);
            this.textBoxEntityId.TabIndex = 81;
            this.textBoxEntityId.TextChanged += new System.EventHandler(this.textBoxEntityId_TextChanged);
            // 
            // textBoxLocationFileName
            // 
            this.textBoxLocationFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLocationFileName.Location = new System.Drawing.Point(128, 5);
            this.textBoxLocationFileName.Name = "textBoxLocationFileName";
            this.textBoxLocationFileName.ReadOnly = true;
            this.textBoxLocationFileName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxLocationFileName.Size = new System.Drawing.Size(678, 20);
            this.textBoxLocationFileName.TabIndex = 80;
            this.textBoxLocationFileName.Text = "E";
            this.textBoxLocationFileName.TextChanged += new System.EventHandler(this.textBoxLocationFileName_TextChanged);
            // 
            // entityFileButton
            // 
            this.entityFileButton.Location = new System.Drawing.Point(23, 3);
            this.entityFileButton.Name = "entityFileButton";
            this.entityFileButton.Size = new System.Drawing.Size(99, 23);
            this.entityFileButton.TabIndex = 79;
            this.entityFileButton.Text = "Signs File";
            this.entityFileButton.UseVisualStyleBackColor = true;
            this.entityFileButton.Click += new System.EventHandler(this.entityFileButton_Click);
            // 
            // richTextBoxSignText
            // 
            this.richTextBoxSignText.Location = new System.Drawing.Point(128, 57);
            this.richTextBoxSignText.Name = "richTextBoxSignText";
            this.richTextBoxSignText.Size = new System.Drawing.Size(227, 96);
            this.richTextBoxSignText.TabIndex = 93;
            this.richTextBoxSignText.Text = "";
            this.richTextBoxSignText.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SignPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richTextBoxSignText);
            this.Controls.Add(this.buttonRemoveEntity);
            this.Controls.Add(this.buttonCopyEntity);
            this.Controls.Add(this.buttonPasteEntity);
            this.Controls.Add(this.buttonAddUpdateEntity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxEntityId);
            this.Controls.Add(this.textBoxLocationFileName);
            this.Controls.Add(this.entityFileButton);
            this.Name = "SignPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRemoveEntity;
        private System.Windows.Forms.Button buttonCopyEntity;
        private System.Windows.Forms.Button buttonPasteEntity;
        private System.Windows.Forms.Button buttonAddUpdateEntity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxEntityId;
        private System.Windows.Forms.TextBox textBoxLocationFileName;
        private System.Windows.Forms.Button entityFileButton;
        private System.Windows.Forms.RichTextBox richTextBoxSignText;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
