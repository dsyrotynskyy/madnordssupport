﻿namespace MadNordsSupport
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.questsTabPage = new System.Windows.Forms.TabPage();
            this.questPanel1 = new MadNordsSupport.QuestPanel();
            this.dialogsTabPage = new System.Windows.Forms.TabPage();
            this.dialogPanel1 = new MadNordsSupport.DialogPanel();
            this.addDialogButton = new System.Windows.Forms.Button();
            this.tabPageLocations = new System.Windows.Forms.TabPage();
            this.locationPanel1 = new MadNordsSupport.UI.LocationPanel();
            this.tabPageItems = new System.Windows.Forms.TabPage();
            this.itemPanel1 = new MadNordsSupport.UI.ItemPanel();
            this.tabPageMerchants = new System.Windows.Forms.TabPage();
            this.merchantPanel1 = new MadNordsSupport.UI.MerchantPanel();
            this.tabPageReadings = new System.Windows.Forms.TabPage();
            this.readingsPanel1 = new MadNordsSupport.UI.SignPanel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bookPanel1 = new MadNordsSupport.UI.BookPanel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.markersContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descriptionsContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.questsTabPage.SuspendLayout();
            this.dialogsTabPage.SuspendLayout();
            this.tabPageLocations.SuspendLayout();
            this.tabPageItems.SuspendLayout();
            this.tabPageMerchants.SuspendLayout();
            this.tabPageReadings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.markersContextMenu.SuspendLayout();
            this.descriptionsContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.questsTabPage);
            this.tabControl.Controls.Add(this.dialogsTabPage);
            this.tabControl.Controls.Add(this.tabPageLocations);
            this.tabControl.Controls.Add(this.tabPageItems);
            this.tabControl.Controls.Add(this.tabPageMerchants);
            this.tabControl.Controls.Add(this.tabPageReadings);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(834, 663);
            this.tabControl.TabIndex = 1;
            // 
            // questsTabPage
            // 
            this.questsTabPage.Controls.Add(this.questPanel1);
            this.questsTabPage.Location = new System.Drawing.Point(4, 22);
            this.questsTabPage.Name = "questsTabPage";
            this.questsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.questsTabPage.Size = new System.Drawing.Size(826, 637);
            this.questsTabPage.TabIndex = 0;
            this.questsTabPage.Text = "Quests";
            this.questsTabPage.UseVisualStyleBackColor = true;
            this.questsTabPage.Click += new System.EventHandler(this.tabPage1_Click);
            this.questsTabPage.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.questsTabPage_PreviewKeyDown);
            // 
            // questPanel1
            // 
            this.questPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.questPanel1.Location = new System.Drawing.Point(0, 6);
            this.questPanel1.Name = "questPanel1";
            this.questPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.questPanel1.Size = new System.Drawing.Size(834, 625);
            this.questPanel1.TabIndex = 0;
            this.questPanel1.Load += new System.EventHandler(this.questPanel1_Load);
            // 
            // dialogsTabPage
            // 
            this.dialogsTabPage.Controls.Add(this.dialogPanel1);
            this.dialogsTabPage.Controls.Add(this.addDialogButton);
            this.dialogsTabPage.Location = new System.Drawing.Point(4, 22);
            this.dialogsTabPage.Name = "dialogsTabPage";
            this.dialogsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.dialogsTabPage.Size = new System.Drawing.Size(826, 637);
            this.dialogsTabPage.TabIndex = 1;
            this.dialogsTabPage.Text = "Dialogs";
            this.dialogsTabPage.UseVisualStyleBackColor = true;
            // 
            // dialogPanel1
            // 
            this.dialogPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dialogPanel1.Location = new System.Drawing.Point(0, 6);
            this.dialogPanel1.Name = "dialogPanel1";
            this.dialogPanel1.Size = new System.Drawing.Size(834, 625);
            this.dialogPanel1.TabIndex = 3;
            this.dialogPanel1.Load += new System.EventHandler(this.dialogPanel1_Load);
            // 
            // addDialogButton
            // 
            this.addDialogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addDialogButton.Enabled = false;
            this.addDialogButton.Location = new System.Drawing.Point(744, 887);
            this.addDialogButton.Name = "addDialogButton";
            this.addDialogButton.Size = new System.Drawing.Size(75, 23);
            this.addDialogButton.TabIndex = 1;
            this.addDialogButton.Text = "Add Dialog";
            this.addDialogButton.UseVisualStyleBackColor = true;
            // 
            // tabPageLocations
            // 
            this.tabPageLocations.Controls.Add(this.locationPanel1);
            this.tabPageLocations.Location = new System.Drawing.Point(4, 22);
            this.tabPageLocations.Name = "tabPageLocations";
            this.tabPageLocations.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLocations.Size = new System.Drawing.Size(826, 637);
            this.tabPageLocations.TabIndex = 2;
            this.tabPageLocations.Text = "Locations";
            this.tabPageLocations.UseVisualStyleBackColor = true;
            // 
            // locationPanel1
            // 
            this.locationPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.locationPanel1.Location = new System.Drawing.Point(0, 6);
            this.locationPanel1.Name = "locationPanel1";
            this.locationPanel1.Size = new System.Drawing.Size(834, 625);
            this.locationPanel1.TabIndex = 0;
            this.locationPanel1.Load += new System.EventHandler(this.locationPanel1_Load);
            // 
            // tabPageItems
            // 
            this.tabPageItems.Controls.Add(this.itemPanel1);
            this.tabPageItems.Location = new System.Drawing.Point(4, 22);
            this.tabPageItems.Name = "tabPageItems";
            this.tabPageItems.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageItems.Size = new System.Drawing.Size(826, 637);
            this.tabPageItems.TabIndex = 3;
            this.tabPageItems.Text = "Items";
            this.tabPageItems.UseVisualStyleBackColor = true;
            // 
            // itemPanel1
            // 
            this.itemPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.itemPanel1.Location = new System.Drawing.Point(0, 6);
            this.itemPanel1.Name = "itemPanel1";
            this.itemPanel1.Size = new System.Drawing.Size(834, 625);
            this.itemPanel1.TabIndex = 0;
            // 
            // tabPageMerchants
            // 
            this.tabPageMerchants.Controls.Add(this.merchantPanel1);
            this.tabPageMerchants.Location = new System.Drawing.Point(4, 22);
            this.tabPageMerchants.Name = "tabPageMerchants";
            this.tabPageMerchants.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMerchants.Size = new System.Drawing.Size(826, 637);
            this.tabPageMerchants.TabIndex = 4;
            this.tabPageMerchants.Text = "Merchants";
            this.tabPageMerchants.UseVisualStyleBackColor = true;
            // 
            // merchantPanel1
            // 
            this.merchantPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.merchantPanel1.Location = new System.Drawing.Point(0, 6);
            this.merchantPanel1.Name = "merchantPanel1";
            this.merchantPanel1.Size = new System.Drawing.Size(834, 625);
            this.merchantPanel1.TabIndex = 0;
            // 
            // tabPageReadings
            // 
            this.tabPageReadings.Controls.Add(this.readingsPanel1);
            this.tabPageReadings.Location = new System.Drawing.Point(4, 22);
            this.tabPageReadings.Name = "tabPageReadings";
            this.tabPageReadings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReadings.Size = new System.Drawing.Size(826, 637);
            this.tabPageReadings.TabIndex = 5;
            this.tabPageReadings.Text = "Signs";
            this.tabPageReadings.UseVisualStyleBackColor = true;
            // 
            // readingsPanel1
            // 
            this.readingsPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.readingsPanel1.Location = new System.Drawing.Point(0, 6);
            this.readingsPanel1.Name = "readingsPanel1";
            this.readingsPanel1.Size = new System.Drawing.Size(834, 625);
            this.readingsPanel1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bookPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(826, 637);
            this.tabPage1.TabIndex = 6;
            this.tabPage1.Text = "Books";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bookPanel1
            // 
            this.bookPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bookPanel1.Location = new System.Drawing.Point(0, 6);
            this.bookPanel1.Name = "bookPanel1";
            this.bookPanel1.Size = new System.Drawing.Size(834, 625);
            this.bookPanel1.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // markersContextMenu
            // 
            this.markersContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRowToolStripMenuItem});
            this.markersContextMenu.Name = "markersContextMenu";
            this.markersContextMenu.Size = new System.Drawing.Size(134, 26);
            // 
            // deleteRowToolStripMenuItem
            // 
            this.deleteRowToolStripMenuItem.Name = "deleteRowToolStripMenuItem";
            this.deleteRowToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.deleteRowToolStripMenuItem.Text = "Delete Row";
            this.deleteRowToolStripMenuItem.Click += new System.EventHandler(this.deleteRowToolStripMenuItem_Click);
            // 
            // descriptionsContextMenu
            // 
            this.descriptionsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.descriptionsContextMenu.Name = "markersContextMenu";
            this.descriptionsContextMenu.Size = new System.Drawing.Size(134, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem1.Text = "Delete Row";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 687);
            this.Controls.Add(this.tabControl);
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Mad Nords Support";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl.ResumeLayout(false);
            this.questsTabPage.ResumeLayout(false);
            this.dialogsTabPage.ResumeLayout(false);
            this.tabPageLocations.ResumeLayout(false);
            this.tabPageItems.ResumeLayout(false);
            this.tabPageMerchants.ResumeLayout(false);
            this.tabPageReadings.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.markersContextMenu.ResumeLayout(false);
            this.descriptionsContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage questsTabPage;
        private System.Windows.Forms.TabPage dialogsTabPage;
        private System.Windows.Forms.Button addDialogButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ContextMenuStrip markersContextMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteRowToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip descriptionsContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private QuestPanel questPanel1;
        private DialogPanel dialogPanel1;
        private System.Windows.Forms.TabPage tabPageLocations;
        private UI.LocationPanel locationPanel1;
        private System.Windows.Forms.TabPage tabPageItems;
        private UI.ItemPanel itemPanel1;
        private System.Windows.Forms.TabPage tabPageMerchants;
        private System.Windows.Forms.TabPage tabPageReadings;
        private UI.MerchantPanel merchantPanel1;
        private UI.SignPanel readingsPanel1;
        private System.Windows.Forms.TabPage tabPage1;
        private UI.BookPanel bookPanel1;
    }
}

