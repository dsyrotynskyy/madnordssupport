﻿namespace MadNordsSupport
{
    partial class QuestPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonAddUpdateQuest = new System.Windows.Forms.Button();
            this.isMainQuest = new System.Windows.Forms.CheckBox();
            this.questCountLabel = new System.Windows.Forms.Label();
            this.labelCreateUpdate = new System.Windows.Forms.Label();
            this.dataGridMarkers = new System.Windows.Forms.DataGridView();
            this.dataGridDescriptions = new System.Windows.Forms.DataGridView();
            this.questFileName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxQuestName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxQuestId = new System.Windows.Forms.TextBox();
            this.questFileButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.markersContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRowUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRowDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descriptionsContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddRowUp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddRowDown = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxXPValue = new System.Windows.Forms.NumericUpDown();
            this.buttonPasteQuest = new System.Windows.Forms.Button();
            this.buttonCopyQuest = new System.Windows.Forms.Button();
            this.buttonRemoveQuest = new System.Windows.Forms.Button();
            this.GroupBoxMarkers = new System.Windows.Forms.GroupBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.markersTextBoxColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsLocation = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMarkers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescriptions)).BeginInit();
            this.markersContextMenu.SuspendLayout();
            this.descriptionsContextMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxXPValue)).BeginInit();
            this.GroupBoxMarkers.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAddUpdateQuest
            // 
            this.buttonAddUpdateQuest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateQuest.Location = new System.Drawing.Point(718, 454);
            this.buttonAddUpdateQuest.Name = "buttonAddUpdateQuest";
            this.buttonAddUpdateQuest.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateQuest.TabIndex = 0;
            this.buttonAddUpdateQuest.Text = "Add Quest";
            this.buttonAddUpdateQuest.UseVisualStyleBackColor = true;
            this.buttonAddUpdateQuest.Click += new System.EventHandler(this.addQuestButton_Click);
            // 
            // isMainQuest
            // 
            this.isMainQuest.AutoSize = true;
            this.isMainQuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.isMainQuest.Location = new System.Drawing.Point(6, 41);
            this.isMainQuest.Name = "isMainQuest";
            this.isMainQuest.Size = new System.Drawing.Size(77, 17);
            this.isMainQuest.TabIndex = 27;
            this.isMainQuest.Text = "MainQuest";
            this.isMainQuest.UseVisualStyleBackColor = true;
            this.isMainQuest.CheckedChanged += new System.EventHandler(this.isMainQuest_CheckedChanged);
            // 
            // questCountLabel
            // 
            this.questCountLabel.AutoSize = true;
            this.questCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.questCountLabel.Location = new System.Drawing.Point(192, 32);
            this.questCountLabel.Name = "questCountLabel";
            this.questCountLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.questCountLabel.Size = new System.Drawing.Size(164, 17);
            this.questCountLabel.TabIndex = 26;
            this.questCountLabel.Text = "Total quest count: no file";
            this.questCountLabel.Click += new System.EventHandler(this.questCountLabel_Click);
            // 
            // labelCreateUpdate
            // 
            this.labelCreateUpdate.AutoSize = true;
            this.labelCreateUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCreateUpdate.Location = new System.Drawing.Point(23, 29);
            this.labelCreateUpdate.Name = "labelCreateUpdate";
            this.labelCreateUpdate.Size = new System.Drawing.Size(163, 20);
            this.labelCreateUpdate.TabIndex = 25;
            this.labelCreateUpdate.Text = "Create New Quest";
            // 
            // dataGridMarkers
            // 
            this.dataGridMarkers.AllowDrop = true;
            this.dataGridMarkers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridMarkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMarkers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn3,
            this.IsLocation});
            this.dataGridMarkers.Location = new System.Drawing.Point(20, 19);
            this.dataGridMarkers.Name = "dataGridMarkers";
            this.dataGridMarkers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridMarkers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridMarkers.Size = new System.Drawing.Size(461, 104);
            this.dataGridMarkers.StandardTab = true;
            this.dataGridMarkers.TabIndex = 24;
            this.dataGridMarkers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_1);
            this.dataGridMarkers.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridMarkers_CellMouseUp);
            this.dataGridMarkers.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridMarkers_EditingControlShowing);
            this.dataGridMarkers.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridMarkers_RowsAdded);
            // 
            // dataGridDescriptions
            // 
            this.dataGridDescriptions.AllowDrop = true;
            this.dataGridDescriptions.AllowUserToOrderColumns = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridDescriptions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridDescriptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridDescriptions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dataGridDescriptions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDescriptions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.markersTextBoxColumnId,
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridDescriptions.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridDescriptions.Location = new System.Drawing.Point(23, 193);
            this.dataGridDescriptions.MultiSelect = false;
            this.dataGridDescriptions.Name = "dataGridDescriptions";
            this.dataGridDescriptions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridDescriptions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridDescriptions.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridDescriptions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridDescriptions.Size = new System.Drawing.Size(783, 238);
            this.dataGridDescriptions.StandardTab = true;
            this.dataGridDescriptions.TabIndex = 23;
            this.dataGridDescriptions.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridDescriptions_CellContentClick);
            this.dataGridDescriptions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridDescriptions_CellContentClick);
            this.dataGridDescriptions.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridDescriptions_CellMouseUp);
            this.dataGridDescriptions.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridDescriptions_KeyDown);
            // 
            // questFileName
            // 
            this.questFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.questFileName.Location = new System.Drawing.Point(128, 5);
            this.questFileName.Name = "questFileName";
            this.questFileName.ReadOnly = true;
            this.questFileName.Size = new System.Drawing.Size(678, 20);
            this.questFileName.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Descriptions";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Name";
            // 
            // textBoxQuestName
            // 
            this.textBoxQuestName.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxQuestName.Location = new System.Drawing.Point(64, 137);
            this.textBoxQuestName.Name = "textBoxQuestName";
            this.textBoxQuestName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxQuestName.Size = new System.Drawing.Size(227, 20);
            this.textBoxQuestName.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Value";
            // 
            // textBoxQuestId
            // 
            this.textBoxQuestId.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxQuestId.Location = new System.Drawing.Point(6, 15);
            this.textBoxQuestId.Name = "textBoxQuestId";
            this.textBoxQuestId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxQuestId.Size = new System.Drawing.Size(100, 20);
            this.textBoxQuestId.TabIndex = 2;
            this.textBoxQuestId.Text = "Quest ID";
            this.textBoxQuestId.TextChanged += new System.EventHandler(this.textBoxQuestId_TextChanged);
            this.textBoxQuestId.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.textBoxQuestId_ControlRemoved);
            // 
            // questFileButton
            // 
            this.questFileButton.Location = new System.Drawing.Point(23, 3);
            this.questFileButton.Name = "questFileButton";
            this.questFileButton.Size = new System.Drawing.Size(99, 23);
            this.questFileButton.TabIndex = 1;
            this.questFileButton.Text = "Select Quest File";
            this.questFileButton.UseVisualStyleBackColor = true;
            this.questFileButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // markersContextMenu
            // 
            this.markersContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRowToolStripMenuItem,
            this.addRowUpToolStripMenuItem,
            this.addRowDownToolStripMenuItem});
            this.markersContextMenu.Name = "markersContextMenu";
            this.markersContextMenu.Size = new System.Drawing.Size(157, 70);
            this.markersContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.markersContextMenu_Opening);
            // 
            // deleteRowToolStripMenuItem
            // 
            this.deleteRowToolStripMenuItem.Name = "deleteRowToolStripMenuItem";
            this.deleteRowToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.deleteRowToolStripMenuItem.Text = "Delete Row";
            this.deleteRowToolStripMenuItem.Click += new System.EventHandler(this.deleteRowToolStripMenuItem_Click);
            // 
            // addRowUpToolStripMenuItem
            // 
            this.addRowUpToolStripMenuItem.Name = "addRowUpToolStripMenuItem";
            this.addRowUpToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.addRowUpToolStripMenuItem.Text = "Add Row Up";
            this.addRowUpToolStripMenuItem.Click += new System.EventHandler(this.addRowUpToolStripMenuItem_Click);
            // 
            // addRowDownToolStripMenuItem
            // 
            this.addRowDownToolStripMenuItem.Name = "addRowDownToolStripMenuItem";
            this.addRowDownToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.addRowDownToolStripMenuItem.Text = "Add Row Down";
            this.addRowDownToolStripMenuItem.Click += new System.EventHandler(this.addRowDownToolStripMenuItem_Click);
            // 
            // descriptionsContextMenu
            // 
            this.descriptionsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemDeleteRow,
            this.toolStripMenuItemAddRowUp,
            this.toolStripMenuItemAddRowDown});
            this.descriptionsContextMenu.Name = "markersContextMenu";
            this.descriptionsContextMenu.Size = new System.Drawing.Size(157, 70);
            // 
            // toolStripMenuItemDeleteRow
            // 
            this.toolStripMenuItemDeleteRow.Name = "toolStripMenuItemDeleteRow";
            this.toolStripMenuItemDeleteRow.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItemDeleteRow.Text = "Delete Row";
            this.toolStripMenuItemDeleteRow.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItemAddRowUp
            // 
            this.toolStripMenuItemAddRowUp.Name = "toolStripMenuItemAddRowUp";
            this.toolStripMenuItemAddRowUp.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItemAddRowUp.Text = "Add Row Up";
            this.toolStripMenuItemAddRowUp.Click += new System.EventHandler(this.toolStripMenuItemAddRowUp_Click);
            // 
            // toolStripMenuItemAddRowDown
            // 
            this.toolStripMenuItemAddRowDown.Name = "toolStripMenuItemAddRowDown";
            this.toolStripMenuItemAddRowDown.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItemAddRowDown.Text = "Add Row Down";
            this.toolStripMenuItemAddRowDown.Click += new System.EventHandler(this.toolStripMenuItemAddRowDown_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxQuestId);
            this.groupBox1.Controls.Add(this.isMainQuest);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(25, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(115, 69);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxXPValue);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(146, 52);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 69);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "XP Reward";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // textBoxXPValue
            // 
            this.textBoxXPValue.Location = new System.Drawing.Point(42, 20);
            this.textBoxXPValue.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.textBoxXPValue.Name = "textBoxXPValue";
            this.textBoxXPValue.Size = new System.Drawing.Size(95, 20);
            this.textBoxXPValue.TabIndex = 10;
            // 
            // buttonPasteQuest
            // 
            this.buttonPasteQuest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteQuest.Enabled = false;
            this.buttonPasteQuest.Location = new System.Drawing.Point(119, 454);
            this.buttonPasteQuest.Name = "buttonPasteQuest";
            this.buttonPasteQuest.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteQuest.TabIndex = 30;
            this.buttonPasteQuest.Text = "Paste Quest";
            this.buttonPasteQuest.UseVisualStyleBackColor = true;
            this.buttonPasteQuest.Click += new System.EventHandler(this.buttonPasteQuest_Click);
            // 
            // buttonCopyQuest
            // 
            this.buttonCopyQuest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyQuest.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyQuest.Name = "buttonCopyQuest";
            this.buttonCopyQuest.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyQuest.TabIndex = 31;
            this.buttonCopyQuest.Text = "Copy Quest";
            this.buttonCopyQuest.UseVisualStyleBackColor = true;
            this.buttonCopyQuest.Click += new System.EventHandler(this.buttonCopyQuest_Click);
            // 
            // buttonRemoveQuest
            // 
            this.buttonRemoveQuest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveQuest.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveQuest.Name = "buttonRemoveQuest";
            this.buttonRemoveQuest.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveQuest.TabIndex = 32;
            this.buttonRemoveQuest.Text = "Delete Quest";
            this.buttonRemoveQuest.UseVisualStyleBackColor = true;
            this.buttonRemoveQuest.Click += new System.EventHandler(this.buttonRemoveQuest_Click);
            // 
            // GroupBoxMarkers
            // 
            this.GroupBoxMarkers.Controls.Add(this.dataGridMarkers);
            this.GroupBoxMarkers.Location = new System.Drawing.Point(319, 52);
            this.GroupBoxMarkers.Name = "GroupBoxMarkers";
            this.GroupBoxMarkers.Size = new System.Drawing.Size(487, 129);
            this.GroupBoxMarkers.TabIndex = 33;
            this.GroupBoxMarkers.TabStop = false;
            this.GroupBoxMarkers.Text = "Markers";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 300F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Value";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 630;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 300F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.Width = 500;
            // 
            // markersTextBoxColumnId
            // 
            this.markersTextBoxColumnId.FillWeight = 300F;
            this.markersTextBoxColumnId.HeaderText = "Id";
            this.markersTextBoxColumnId.Name = "markersTextBoxColumnId";
            this.markersTextBoxColumnId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Id";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 30;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Value";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 310;
            // 
            // IsLocation
            // 
            this.IsLocation.HeaderText = "IsLocation";
            this.IsLocation.Name = "IsLocation";
            this.IsLocation.Width = 70;
            // 
            // QuestPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GroupBoxMarkers);
            this.Controls.Add(this.buttonRemoveQuest);
            this.Controls.Add(this.buttonCopyQuest);
            this.Controls.Add(this.buttonPasteQuest);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelCreateUpdate);
            this.Controls.Add(this.questCountLabel);
            this.Controls.Add(this.questFileName);
            this.Controls.Add(this.dataGridDescriptions);
            this.Controls.Add(this.buttonAddUpdateQuest);
            this.Controls.Add(this.questFileButton);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxQuestName);
            this.Name = "QuestPanel";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.QuestPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMarkers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescriptions)).EndInit();
            this.markersContextMenu.ResumeLayout(false);
            this.descriptionsContextMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxXPValue)).EndInit();
            this.GroupBoxMarkers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddUpdateQuest;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxQuestName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxQuestId;
        private System.Windows.Forms.Button questFileButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox questFileName;
        private System.Windows.Forms.DataGridView dataGridDescriptions;
        private System.Windows.Forms.DataGridView dataGridMarkers;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label labelCreateUpdate;
        private System.Windows.Forms.Label questCountLabel;
        private System.Windows.Forms.CheckBox isMainQuest;
        private System.Windows.Forms.ContextMenuStrip markersContextMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteRowToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip descriptionsContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDeleteRow;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonPasteQuest;
        private System.Windows.Forms.Button buttonCopyQuest;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.NumericUpDown textBoxXPValue;
        private System.Windows.Forms.Button buttonRemoveQuest;
        private System.Windows.Forms.ToolStripMenuItem addRowUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRowDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddRowUp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddRowDown;
        private System.Windows.Forms.GroupBox GroupBoxMarkers;
        private System.Windows.Forms.DataGridViewTextBoxColumn markersTextBoxColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsLocation;
    }
}
