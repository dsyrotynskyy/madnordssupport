﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.UI.BasePanels;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport.UI
{
    partial class MerchantPanel : BaseMerchantPanel
    {
        Dictionary<int, TextBox> wareTextBoxes = new Dictionary<int, TextBox> ();
        Dictionary<int, ComboBox> wareComboboxes = new Dictionary<int, ComboBox> ();

        public MerchantPanel()
        {
            InitializeComponent();

            PrepareWareComboboxes();
            PrepareWareTextBoxes();

            TryFileOnStart();          
        }

        const int WARE_COUNT = 7;

        private void PrepareWareComboboxes ()
        {
            wareTextBoxes.Add(0, textBox0);
            wareTextBoxes.Add(1, textBox1);
            wareTextBoxes.Add(2, textBox2);
            wareTextBoxes.Add(3, textBox3);
            wareTextBoxes.Add(4, textBox4);
            wareTextBoxes.Add(5, textBox5);
            wareTextBoxes.Add(6, textBox6);

            foreach (TextBox t in wareTextBoxes.Values)
            {
                t.TextChanged += new System.EventHandler(this.textBoxWareTextChanged);
            }
        }

        private void PrepareWareTextBoxes ()
        {
            wareComboboxes.Add(0, comboBox0);
            wareComboboxes.Add(1, comboBox1);
            wareComboboxes.Add(2, comboBox2);
            wareComboboxes.Add(3, comboBox3);
            wareComboboxes.Add(4, comboBox4);
            wareComboboxes.Add(5, comboBox5);
            wareComboboxes.Add(6, comboBox6);

            foreach (ComboBox c in wareComboboxes.Values)
            {
                c.Items.Clear();
                c.Items.AddRange(ItemTypeDAO.Instance.allData.Keys.ToArray());
                c.TextChanged += new System.EventHandler(this.comboBoxWareTextChanged);
            }
        }

        private void MerchantPanel_Load(object sender, EventArgs e)
        {
            if (dao.allData.Count > 0)
            {
                this.TextBoxDataId.Text = dao.allData.Keys.First();
            }            
        }

        public override TextBox TextBoxDataId
        {
            get
            {
                return this.textBoxEntityId;
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                return this.buttonAddUpdateEntity;
            }
        }

        protected override MerchantDAO dao
        {
            get
            {
                return MerchantDAO.Instance;
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                return this.textBoxLocationFileName;
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                return this.buttonRemoveEntity;
            }
        }

        public override void UpdateDataByFields(Merchant data, bool includeId)
        {
            if (includeId)
            {
                data.Id = this.TextBoxDataId.Text;
            }
            data._answer = this.textBoxAnswer.Text;

            for (int i = 0; i < WARE_COUNT; i++)
            {                
                if (IsWareCorrect (i))
                {
                    ComboBox combo = wareComboboxes[i];
                    TextBox tx = wareTextBoxes[i];

                    if (!combo.Enabled || !tx.Enabled)
                    {
                        return;
                    }

                    ItemType itemType = ItemTypeDAO.Instance.GetItemType(combo.Text);
                    Item item = null;
                    if (itemType != null)
                    {
                        item = itemType.GetItemByTitle(tx.Text);
                    }

                    if (itemType != null && item != null)
                    {
                        data.AddUpdateWare(i, combo.Text, item._numericId);
                    }
                }
            }
        }

        protected override void DisplayData(Merchant data, bool includeId)
        {
            if (includeId)
            {
                this.TextBoxDataId.Text = data.Id;
            }
            this.textBoxAnswer.Text = data._answer;
            for (int i = 0; i < WARE_COUNT; i++)
            {
                if (data.wares.Count > i)
                {
                    DisplayWare(i, data.wares[i]);
                } else
                {
                    DisplayWare(i, new Merchant.Ware ());
                }               
            }
        }

        private void DisplayWare (int pos, Merchant.Ware ware)
        {
            ComboBox comboBox = this.wareComboboxes[pos];
            TextBox tx = this.wareTextBoxes[pos];

            if (ware.IsEmpty)
            {
                tx.Text = "";
                comboBox.Text = "";                
                return;
            }

            ItemType itemType = ItemTypeDAO.Instance.GetItemType(ware.Type);
            if (itemType != null)
            {
                comboBox.Text = itemType.Id;
                tx.Text = itemType.GetItemByNumericId (ware.Item)._title ;
            }
        }

        private void entityFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDataFile(this.openFileDialog1))
            {
                RenewAllFieldsByData();
            }
        }

        private void textBoxEntityId_TextChanged(object sender, EventArgs e)
        {
            TextBoxIdChanged();
        }

        private void buttonAddUpdateEntity_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentData();
        }

        private void buttonRemoveEntity_Click(object sender, EventArgs e)
        {
            RemoveCurrentEntity();
        }

        #region WareUI

        private void UpdateWareUI(int wareNum)
        {
            ComboBox combo = wareComboboxes[wareNum];
            TextBox tx = wareTextBoxes[wareNum];

            ItemType itemType = ItemTypeDAO.Instance.GetItemType(combo.Text);
            UpdateItemsList(tx, itemType);
        }

        private static void UpdateItemsList(TextBox tx, ItemType itemType)
        {
            if (itemType == null)
            {
                tx.Enabled = false;
            }
            else
            {
                tx.Enabled = true;
                AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();
                string[] arr = itemType.AllItemTitles;

                allowedTypes.AddRange(arr);
                tx.AutoCompleteCustomSource = allowedTypes;
                tx.AutoCompleteMode = AutoCompleteMode.Suggest;
                tx.AutoCompleteSource = AutoCompleteSource.CustomSource;
            }
        }

        private void comboBoxWareTextChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                ComboBox cb = (ComboBox)sender;
                foreach (int i in wareComboboxes.Keys)
                {
                    ComboBox c = wareComboboxes[i];
                    if (c == cb)
                    {
                        UpdateWareUI(i);
                        OrderWareForCorrect(i);
                        return;
                    }
                }
            }
        }

        private void textBoxWareTextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox cb = (TextBox)sender;
                foreach (int i in wareTextBoxes.Keys)
                {
                    TextBox c = wareTextBoxes[i];
                    if (c == cb)
                    {
                        OrderWareForCorrect(i);
                        return;
                    }
                }
            }
        }

        private bool isCheckingWareCorrect = false;

        private void OrderWareForCorrect (int pos)
        {
            if (isCheckingWareCorrect || pos == WARE_COUNT - 1)
            {
                return;
            }
            isCheckingWareCorrect = true;

            CheckWareForCorrect(pos);

            isCheckingWareCorrect = false;
        }

        private void CheckWareForCorrect(int pos)
        {
            if (pos < WARE_COUNT - 1)
            {
                bool isCorrect;
                isCorrect = IsWareCorrect(pos);

                if (isCorrect)
                {
                    EnableWare(pos + 1);
                    CheckWareForCorrect(pos + 1);
                }  else
                {
                    DisableWares(pos + 1);
                }              
            }
        }

        bool IsWareCorrect (int pos)
        {
            bool isCorrect = true;
            ComboBox combo = wareComboboxes[pos];
            TextBox tx = wareTextBoxes[pos];
            
            if (!combo.Enabled || !tx.Enabled)
            {
                return false;
            }

            ItemType itemType = ItemTypeDAO.Instance.GetItemType(combo.Text);
            if (itemType == null)
            {
                isCorrect = false;
            }
            else
            {
                
                if (!itemType.HasItemWithTitle(tx.Text))
                {
                    isCorrect = false;
                }
            }
            return isCorrect;
        }

        public void EnableWare (int pos)
        {
            ComboBox combo = wareComboboxes[pos];
            TextBox tx = wareTextBoxes[pos];
            combo.Enabled = true;

            ItemType itemType = ItemTypeDAO.Instance.GetItemType(combo.Text);
            tx.Enabled = itemType != null;
        }

        private void DisableWares (int pos)
        {
            if (pos < WARE_COUNT)
            {
                ComboBox combo = wareComboboxes[pos];
                TextBox tx = wareTextBoxes[pos];
                combo.Enabled = false;
                tx.Enabled = false;
                DisableWares(pos + 1);
            }
        }
        #endregion
    }
}
