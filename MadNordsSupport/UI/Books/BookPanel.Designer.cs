﻿namespace MadNordsSupport.UI
{
    partial class BookPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRemoveEntity = new System.Windows.Forms.Button();
            this.buttonCopyEntity = new System.Windows.Forms.Button();
            this.buttonPasteEntity = new System.Windows.Forms.Button();
            this.buttonAddUpdateEntity = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEntityId = new System.Windows.Forms.TextBox();
            this.textBoxEntityFileName = new System.Windows.Forms.TextBox();
            this.entityFileButton = new System.Windows.Forms.Button();
            this.numericUpDownXP = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.advancedFlowLayoutPanel1 = new MakarovDev.ExpandCollapsePanel.AdvancedFlowLayoutPanel();
            this.expandCollapsePanel1 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.expandCollapsePanel4 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.pagePanel0 = new MadNordsSupport.UI.Books.PagePanel();
            this.expandCollapsePanel2 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.expandCollapsePanel3 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.pagePanel1 = new MadNordsSupport.UI.Books.PagePanel();
            this.expandCollapsePanel5 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.expandCollapsePanel6 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.pagePanel2 = new MadNordsSupport.UI.Books.PagePanel();
            this.expandCollapsePanel7 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.expandCollapsePanel8 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.pagePanel3 = new MadNordsSupport.UI.Books.PagePanel();
            this.expandCollapsePanel9 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.expandCollapsePanel10 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.pagePanel4 = new MadNordsSupport.UI.Books.PagePanel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownXP)).BeginInit();
            this.advancedFlowLayoutPanel1.SuspendLayout();
            this.expandCollapsePanel1.SuspendLayout();
            this.expandCollapsePanel4.SuspendLayout();
            this.expandCollapsePanel2.SuspendLayout();
            this.expandCollapsePanel3.SuspendLayout();
            this.expandCollapsePanel5.SuspendLayout();
            this.expandCollapsePanel6.SuspendLayout();
            this.expandCollapsePanel7.SuspendLayout();
            this.expandCollapsePanel8.SuspendLayout();
            this.expandCollapsePanel9.SuspendLayout();
            this.expandCollapsePanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRemoveEntity
            // 
            this.buttonRemoveEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveEntity.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveEntity.Name = "buttonRemoveEntity";
            this.buttonRemoveEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveEntity.TabIndex = 102;
            this.buttonRemoveEntity.Text = "Delete Item";
            this.buttonRemoveEntity.UseVisualStyleBackColor = true;
            this.buttonRemoveEntity.Click += new System.EventHandler(this.buttonRemoveEntity_Click);
            // 
            // buttonCopyEntity
            // 
            this.buttonCopyEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyEntity.Enabled = false;
            this.buttonCopyEntity.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyEntity.Name = "buttonCopyEntity";
            this.buttonCopyEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyEntity.TabIndex = 101;
            this.buttonCopyEntity.Text = "Copy Item";
            this.buttonCopyEntity.UseVisualStyleBackColor = true;
            // 
            // buttonPasteEntity
            // 
            this.buttonPasteEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteEntity.Enabled = false;
            this.buttonPasteEntity.Location = new System.Drawing.Point(117, 454);
            this.buttonPasteEntity.Name = "buttonPasteEntity";
            this.buttonPasteEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteEntity.TabIndex = 100;
            this.buttonPasteEntity.Text = "Paste Item";
            this.buttonPasteEntity.UseVisualStyleBackColor = true;
            // 
            // buttonAddUpdateEntity
            // 
            this.buttonAddUpdateEntity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateEntity.Location = new System.Drawing.Point(716, 454);
            this.buttonAddUpdateEntity.Name = "buttonAddUpdateEntity";
            this.buttonAddUpdateEntity.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateEntity.TabIndex = 99;
            this.buttonAddUpdateEntity.Text = "Add Item";
            this.buttonAddUpdateEntity.UseVisualStyleBackColor = true;
            this.buttonAddUpdateEntity.Click += new System.EventHandler(this.buttonAddUpdateEntity_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(103, 34);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 97;
            this.label6.Text = "Id";
            // 
            // textBoxEntityId
            // 
            this.textBoxEntityId.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxEntityId.Location = new System.Drawing.Point(128, 31);
            this.textBoxEntityId.Name = "textBoxEntityId";
            this.textBoxEntityId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxEntityId.Size = new System.Drawing.Size(227, 20);
            this.textBoxEntityId.TabIndex = 96;
            this.textBoxEntityId.TextChanged += new System.EventHandler(this.textBoxEntityId_TextChanged);
            // 
            // textBoxEntityFileName
            // 
            this.textBoxEntityFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEntityFileName.Location = new System.Drawing.Point(128, 5);
            this.textBoxEntityFileName.Name = "textBoxEntityFileName";
            this.textBoxEntityFileName.ReadOnly = true;
            this.textBoxEntityFileName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxEntityFileName.Size = new System.Drawing.Size(678, 20);
            this.textBoxEntityFileName.TabIndex = 95;
            this.textBoxEntityFileName.Text = "E";
            // 
            // entityFileButton
            // 
            this.entityFileButton.Location = new System.Drawing.Point(23, 3);
            this.entityFileButton.Name = "entityFileButton";
            this.entityFileButton.Size = new System.Drawing.Size(99, 23);
            this.entityFileButton.TabIndex = 94;
            this.entityFileButton.Text = "Book File";
            this.entityFileButton.UseVisualStyleBackColor = true;
            this.entityFileButton.Click += new System.EventHandler(this.entityFileButton_Click);
            // 
            // numericUpDownXP
            // 
            this.numericUpDownXP.Location = new System.Drawing.Point(391, 32);
            this.numericUpDownXP.Name = "numericUpDownXP";
            this.numericUpDownXP.Size = new System.Drawing.Size(95, 20);
            this.numericUpDownXP.TabIndex = 104;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(361, 34);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 105;
            this.label2.Text = "XP";
            // 
            // advancedFlowLayoutPanel1
            // 
            this.advancedFlowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedFlowLayoutPanel1.AutoScroll = true;
            this.advancedFlowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.advancedFlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel1);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel2);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel5);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel7);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel9);
            this.advancedFlowLayoutPanel1.Location = new System.Drawing.Point(128, 58);
            this.advancedFlowLayoutPanel1.Name = "advancedFlowLayoutPanel1";
            this.advancedFlowLayoutPanel1.Size = new System.Drawing.Size(686, 379);
            this.advancedFlowLayoutPanel1.TabIndex = 106;
            // 
            // expandCollapsePanel1
            // 
            this.expandCollapsePanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel1.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel1.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel1.Controls.Add(this.expandCollapsePanel4);
            this.expandCollapsePanel1.ExpandedHeight = 367;
            this.expandCollapsePanel1.IsExpanded = true;
            this.expandCollapsePanel1.Location = new System.Drawing.Point(3, 3);
            this.expandCollapsePanel1.Name = "expandCollapsePanel1";
            this.expandCollapsePanel1.Size = new System.Drawing.Size(661, 258);
            this.expandCollapsePanel1.TabIndex = 47;
            this.expandCollapsePanel1.Text = "Page 1";
            this.expandCollapsePanel1.UseAnimation = false;
            // 
            // expandCollapsePanel4
            // 
            this.expandCollapsePanel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel4.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel4.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel4.Controls.Add(this.pagePanel0);
            this.expandCollapsePanel4.ExpandedHeight = 367;
            this.expandCollapsePanel4.IsExpanded = true;
            this.expandCollapsePanel4.Location = new System.Drawing.Point(-1, -1);
            this.expandCollapsePanel4.Name = "expandCollapsePanel4";
            this.expandCollapsePanel4.Size = new System.Drawing.Size(653, 280);
            this.expandCollapsePanel4.TabIndex = 46;
            this.expandCollapsePanel4.Text = "\" \"";
            this.expandCollapsePanel4.UseAnimation = false;
            // 
            // pagePanel0
            // 
            this.pagePanel0.Location = new System.Drawing.Point(15, 26);
            this.pagePanel0.Name = "pagePanel0";
            this.pagePanel0.OnValueChangedObserver = null;
            this.pagePanel0.Size = new System.Drawing.Size(650, 230);
            this.pagePanel0.TabIndex = 1;
            // 
            // expandCollapsePanel2
            // 
            this.expandCollapsePanel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel2.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel2.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel2.Controls.Add(this.expandCollapsePanel3);
            this.expandCollapsePanel2.ExpandedHeight = 367;
            this.expandCollapsePanel2.IsExpanded = true;
            this.expandCollapsePanel2.Location = new System.Drawing.Point(3, 267);
            this.expandCollapsePanel2.Name = "expandCollapsePanel2";
            this.expandCollapsePanel2.Size = new System.Drawing.Size(661, 258);
            this.expandCollapsePanel2.TabIndex = 48;
            this.expandCollapsePanel2.Text = "Page 2";
            this.expandCollapsePanel2.UseAnimation = false;
            // 
            // expandCollapsePanel3
            // 
            this.expandCollapsePanel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel3.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel3.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel3.Controls.Add(this.pagePanel1);
            this.expandCollapsePanel3.ExpandedHeight = 367;
            this.expandCollapsePanel3.IsExpanded = true;
            this.expandCollapsePanel3.Location = new System.Drawing.Point(-1, -1);
            this.expandCollapsePanel3.Name = "expandCollapsePanel3";
            this.expandCollapsePanel3.Size = new System.Drawing.Size(653, 280);
            this.expandCollapsePanel3.TabIndex = 46;
            this.expandCollapsePanel3.Text = "\" \"";
            this.expandCollapsePanel3.UseAnimation = false;
            // 
            // pagePanel1
            // 
            this.pagePanel1.Location = new System.Drawing.Point(15, 26);
            this.pagePanel1.Name = "pagePanel1";
            this.pagePanel1.OnValueChangedObserver = null;
            this.pagePanel1.Size = new System.Drawing.Size(650, 230);
            this.pagePanel1.TabIndex = 1;
            // 
            // expandCollapsePanel5
            // 
            this.expandCollapsePanel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel5.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel5.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel5.Controls.Add(this.expandCollapsePanel6);
            this.expandCollapsePanel5.ExpandedHeight = 367;
            this.expandCollapsePanel5.IsExpanded = true;
            this.expandCollapsePanel5.Location = new System.Drawing.Point(3, 531);
            this.expandCollapsePanel5.Name = "expandCollapsePanel5";
            this.expandCollapsePanel5.Size = new System.Drawing.Size(661, 258);
            this.expandCollapsePanel5.TabIndex = 49;
            this.expandCollapsePanel5.Text = "Page 3";
            this.expandCollapsePanel5.UseAnimation = false;
            // 
            // expandCollapsePanel6
            // 
            this.expandCollapsePanel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel6.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel6.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel6.Controls.Add(this.pagePanel2);
            this.expandCollapsePanel6.ExpandedHeight = 367;
            this.expandCollapsePanel6.IsExpanded = true;
            this.expandCollapsePanel6.Location = new System.Drawing.Point(-1, -1);
            this.expandCollapsePanel6.Name = "expandCollapsePanel6";
            this.expandCollapsePanel6.Size = new System.Drawing.Size(653, 280);
            this.expandCollapsePanel6.TabIndex = 46;
            this.expandCollapsePanel6.Text = "\" \"";
            this.expandCollapsePanel6.UseAnimation = false;
            // 
            // pagePanel2
            // 
            this.pagePanel2.Location = new System.Drawing.Point(15, 26);
            this.pagePanel2.Name = "pagePanel2";
            this.pagePanel2.OnValueChangedObserver = null;
            this.pagePanel2.Size = new System.Drawing.Size(650, 230);
            this.pagePanel2.TabIndex = 1;
            // 
            // expandCollapsePanel7
            // 
            this.expandCollapsePanel7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel7.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel7.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel7.Controls.Add(this.expandCollapsePanel8);
            this.expandCollapsePanel7.ExpandedHeight = 367;
            this.expandCollapsePanel7.IsExpanded = true;
            this.expandCollapsePanel7.Location = new System.Drawing.Point(3, 795);
            this.expandCollapsePanel7.Name = "expandCollapsePanel7";
            this.expandCollapsePanel7.Size = new System.Drawing.Size(661, 258);
            this.expandCollapsePanel7.TabIndex = 50;
            this.expandCollapsePanel7.Text = "Page 4";
            this.expandCollapsePanel7.UseAnimation = false;
            // 
            // expandCollapsePanel8
            // 
            this.expandCollapsePanel8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel8.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel8.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel8.Controls.Add(this.pagePanel3);
            this.expandCollapsePanel8.ExpandedHeight = 367;
            this.expandCollapsePanel8.IsExpanded = true;
            this.expandCollapsePanel8.Location = new System.Drawing.Point(-1, -1);
            this.expandCollapsePanel8.Name = "expandCollapsePanel8";
            this.expandCollapsePanel8.Size = new System.Drawing.Size(653, 280);
            this.expandCollapsePanel8.TabIndex = 46;
            this.expandCollapsePanel8.Text = "\" \"";
            this.expandCollapsePanel8.UseAnimation = false;
            // 
            // pagePanel3
            // 
            this.pagePanel3.Location = new System.Drawing.Point(15, 26);
            this.pagePanel3.Name = "pagePanel3";
            this.pagePanel3.OnValueChangedObserver = null;
            this.pagePanel3.Size = new System.Drawing.Size(650, 230);
            this.pagePanel3.TabIndex = 1;
            // 
            // expandCollapsePanel9
            // 
            this.expandCollapsePanel9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel9.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel9.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel9.Controls.Add(this.expandCollapsePanel10);
            this.expandCollapsePanel9.ExpandedHeight = 367;
            this.expandCollapsePanel9.IsExpanded = true;
            this.expandCollapsePanel9.Location = new System.Drawing.Point(3, 1059);
            this.expandCollapsePanel9.Name = "expandCollapsePanel9";
            this.expandCollapsePanel9.Size = new System.Drawing.Size(661, 258);
            this.expandCollapsePanel9.TabIndex = 51;
            this.expandCollapsePanel9.Text = "Page 5";
            this.expandCollapsePanel9.UseAnimation = false;
            // 
            // expandCollapsePanel10
            // 
            this.expandCollapsePanel10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel10.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel10.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel10.Controls.Add(this.pagePanel4);
            this.expandCollapsePanel10.ExpandedHeight = 367;
            this.expandCollapsePanel10.IsExpanded = true;
            this.expandCollapsePanel10.Location = new System.Drawing.Point(-1, -1);
            this.expandCollapsePanel10.Name = "expandCollapsePanel10";
            this.expandCollapsePanel10.Size = new System.Drawing.Size(653, 280);
            this.expandCollapsePanel10.TabIndex = 46;
            this.expandCollapsePanel10.Text = "\" \"";
            this.expandCollapsePanel10.UseAnimation = false;
            // 
            // pagePanel4
            // 
            this.pagePanel4.Location = new System.Drawing.Point(15, 26);
            this.pagePanel4.Name = "pagePanel4";
            this.pagePanel4.OnValueChangedObserver = null;
            this.pagePanel4.Size = new System.Drawing.Size(650, 230);
            this.pagePanel4.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // BookPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.advancedFlowLayoutPanel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownXP);
            this.Controls.Add(this.buttonRemoveEntity);
            this.Controls.Add(this.buttonCopyEntity);
            this.Controls.Add(this.buttonPasteEntity);
            this.Controls.Add(this.buttonAddUpdateEntity);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxEntityId);
            this.Controls.Add(this.textBoxEntityFileName);
            this.Controls.Add(this.entityFileButton);
            this.Name = "BookPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.BookPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownXP)).EndInit();
            this.advancedFlowLayoutPanel1.ResumeLayout(false);
            this.expandCollapsePanel1.ResumeLayout(false);
            this.expandCollapsePanel1.PerformLayout();
            this.expandCollapsePanel4.ResumeLayout(false);
            this.expandCollapsePanel4.PerformLayout();
            this.expandCollapsePanel2.ResumeLayout(false);
            this.expandCollapsePanel2.PerformLayout();
            this.expandCollapsePanel3.ResumeLayout(false);
            this.expandCollapsePanel3.PerformLayout();
            this.expandCollapsePanel5.ResumeLayout(false);
            this.expandCollapsePanel5.PerformLayout();
            this.expandCollapsePanel6.ResumeLayout(false);
            this.expandCollapsePanel6.PerformLayout();
            this.expandCollapsePanel7.ResumeLayout(false);
            this.expandCollapsePanel7.PerformLayout();
            this.expandCollapsePanel8.ResumeLayout(false);
            this.expandCollapsePanel8.PerformLayout();
            this.expandCollapsePanel9.ResumeLayout(false);
            this.expandCollapsePanel9.PerformLayout();
            this.expandCollapsePanel10.ResumeLayout(false);
            this.expandCollapsePanel10.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonRemoveEntity;
        private System.Windows.Forms.Button buttonCopyEntity;
        private System.Windows.Forms.Button buttonPasteEntity;
        private System.Windows.Forms.Button buttonAddUpdateEntity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxEntityId;
        private System.Windows.Forms.TextBox textBoxEntityFileName;
        private System.Windows.Forms.Button entityFileButton;
        private System.Windows.Forms.NumericUpDown numericUpDownXP;
        private System.Windows.Forms.Label label2;
        private MakarovDev.ExpandCollapsePanel.AdvancedFlowLayoutPanel advancedFlowLayoutPanel1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel1;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel4;
        private Books.PagePanel pagePanel0;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel2;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel3;
        private Books.PagePanel pagePanel1;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel5;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel6;
        private Books.PagePanel pagePanel2;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel7;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel8;
        private Books.PagePanel pagePanel3;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel9;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel10;
        private Books.PagePanel pagePanel4;
    }
}
