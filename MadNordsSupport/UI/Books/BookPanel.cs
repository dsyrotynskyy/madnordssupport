﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MadNordsSupport.UI.BasePanels;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using MadNordsSupport.UI.Books;

namespace MadNordsSupport.UI
{
    public partial class BookPanel : BaseBookPanel
    {
        Dictionary<int, PagePanel> _pagePanels;

        public BookPanel()
        {
            InitializeComponent();

            TryFileOnStart();

            foreach (PagePanel pagePanel in this.PagePanels.Values)
            {
                pagePanel.OnValueChangedObserver = (CheckPagePanelsValid);
            }
        }

        public Dictionary<int, PagePanel> PagePanels
        {
            get
            {
                if (_pagePanels == null)
                {
                    _pagePanels = new Dictionary<int, PagePanel>();
                    _pagePanels.Add(0, pagePanel0);
                    _pagePanels.Add(1, pagePanel1);
                    _pagePanels.Add(2, pagePanel2);
                    _pagePanels.Add(3, pagePanel3);
                    _pagePanels.Add(4, pagePanel4);
                }
                return _pagePanels;
            }
        }

        private void BookPanel_Load(object sender, EventArgs e)
        {

        }

        public override TextBox TextBoxDataId
        {
            get
            {
                return this.textBoxEntityId;
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                return this.buttonAddUpdateEntity;
            }
        }

        protected override BookDAO dao
        {
            get
            {
                return BookDAO.Instance;
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                return this.textBoxEntityFileName;
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                return this.buttonRemoveEntity;
            }
        }

        public override void UpdateDataByFields(Book data, bool includeId)
        {
            if (includeId)
            {
                data.Id = this.textBoxEntityId.Text;
            }

            data.XP = (int)this.numericUpDownXP.Value;
            for (int i = 0; i < 5; i++)
            {
                PagePanel p = PagePanels[i];
                p.UpdatePage(data.GetCreatePageById(i));
            }
        }

        protected bool IsProcessingDataDisplay = true;

        protected override void DisplayData(Book data, bool includeId)
        {
            IsProcessingDataDisplay = true;

            if (includeId)
            {
                this.textBoxEntityId.Text = data.Id;
            }
            this.numericUpDownXP.Value = data.XP;
            for (int i = 0; i < 5; i++)
            {
                PagePanel p = PagePanels[i];
                p.Display(data.GetCreatePageById (i));
            }

            IsProcessingDataDisplay = false;

            CheckPagePanelsValid();
        }

        public void CheckPagePanelsValid ()
        {
            if (IsProcessingDataDisplay)
            {
                return;
            }

            Book l = null;
            if (this.dao.allData.ContainsKey(TextBoxDataId.Text))
            {
                l = dao.allData[TextBoxDataId.Text];
            }
            else
            {
                l = dao.CreateNew();
            }

            Book.Page testPage = Book.Page.CreateTest();

            bool prevValid = true;
            int i = 0;
            foreach (PagePanel pagePanel in this.PagePanels.Values)
            {
                prevValid = pagePanel.CheckValidPanel(prevValid, testPage);
                i++;
            }
        }

        private void entityFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDataFile(this.openFileDialog1))
            {
                RenewAllFieldsByData();
            }
        }

        private void buttonAddUpdateEntity_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentData();
        }

        private void buttonRemoveEntity_Click(object sender, EventArgs e)
        {
            RemoveCurrentEntity();
        }

        private void textBoxEntityId_TextChanged(object sender, EventArgs e)
        {
            TextBoxIdChanged();
        }
    }
}
