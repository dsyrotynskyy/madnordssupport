﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport.UI.Books
{
    public partial class PagePanel : UserControl
    {
        public PagePanel()
        {
            InitializeComponent();
        }

        private void Warning_Click(object sender, EventArgs e)
        {
        }

        internal void Display(Book.Page page)
        {
            this.textBoxTitle.Text = page.Title;
            this.richTextBoxText.Text = page.Text;
            this.comboBoxImgType.Text = page.ImageType;
            this.textBoxImgId.Text = page.ImageAnimName;
            this.numericUpDownAnim.Value = page.ImageAnimFrame;
            this.numericUpDownImgStartLine.Value = page.ImageStartLine;
        }

        internal void UpdatePage(Book.Page page)
        {
            page.Title = this.textBoxTitle.Text;
            page.Text = this.richTextBoxText.Text;
            page.ImageType = this.comboBoxImgType.Text;
            page.ImageAnimName = this.textBoxImgId.Text;
            page.ImageAnimFrame = (int)this.numericUpDownAnim.Value;
            page.ImageStartLine = (int)this.numericUpDownImgStartLine.Value;

            RecalculateTextLength();
        }

        private void numericUpDownImgStartLine_ValueChanged(object sender, EventArgs e)
        {
        }

        internal bool CheckValidPanel(bool prevValid, Book.Page page)
        {
            bool valid = prevValid;
            if (prevValid)
            {
                this.UpdatePage(page);
                valid = page.IsSavable;
            }
            else
            {
                valid = false;
            }
            SetupAllFieldsEnabled(prevValid);
            return valid;
        }

        private void SetupAllFieldsEnabled(bool b)
        {
            this.textBoxTitle.Enabled = b;
            this.richTextBoxText.Enabled = b;

            this.comboBoxImgType.Enabled = b;
            this.textBoxImgId.Enabled = b;
            this.numericUpDownAnim.Enabled = b;
            this.numericUpDownImgStartLine.Enabled = b;
        }

        Action onValueChangedObserver = null;
        public Action OnValueChangedObserver { get { return onValueChangedObserver; }
            set { this.onValueChangedObserver = value; }
        }

        private void textBoxTitle_TextChanged(object sender, EventArgs e)
        {
            OnValueChanged();
        }

        bool processingBodyText = false;

        private void richTextBoxText_TextChanged(object sender, EventArgs e)
        {
            if (processingBodyText)
            {
                return;
            }

            processingBodyText = true;
            int selectionStart = this.richTextBoxText.SelectionStart;
            try
            {
                this.richTextBoxText.Text = TextAnalyzer.Instance.RefactorTextBlock(this.richTextBoxText.Text);
            }
            catch (Exception err)
            {
            }
           this.richTextBoxText.SelectionStart = selectionStart;
            OnValueChanged();

            processingBodyText = false;
        }

        private void comboBoxImgType_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnValueChanged();
        }

        private void comboBoxImgType_TextChanged(object sender, EventArgs e)
        {
            OnValueChanged();
        }

        private void textBoxImgId_TextChanged(object sender, EventArgs e)
        {
            OnValueChanged();
        }

        private void numericUpDownAnim_ValueChanged(object sender, EventArgs e)
        {
            OnValueChanged();
        }

        private void OnValueChanged()
        {
            if (OnValueChangedObserver != null)
            {
                OnValueChangedObserver();
            }
        }

        private void RecalculateTextLength ()
        {
            TextAnalyzer TextAnalyzer = TextAnalyzer.Instance;
            TextAnalyzer.ClearData();
            TextAnalyzer.SetupBody(this.richTextBoxText.Text);
            TextAnalyzer.SetupTitle(this.textBoxTitle.Text);
            TextAnalyzer.SetupImageType(this.comboBoxImgType.Text);

            string result = TextAnalyzer.Analyze();

            this.labelWarning.Text = result;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }

        private void PagePanel_Load(object sender, EventArgs e)
        {
        }
    }
}
