﻿using System;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport.UI.Books
{
    partial class PagePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxText = new System.Windows.Forms.RichTextBox();
            this.labelWarning = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxImgType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxImgId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownImgStartLine = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownAnim = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownImgStartLine)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnim)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxText
            // 
            this.richTextBoxText.Location = new System.Drawing.Point(37, 32);
            this.richTextBoxText.Name = "richTextBoxText";
            this.richTextBoxText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.richTextBoxText.Size = new System.Drawing.Size(600, 123);
            this.richTextBoxText.TabIndex = 2;
            this.richTextBoxText.Text = "";
            this.richTextBoxText.WordWrap = false;
            this.richTextBoxText.TextChanged += new System.EventHandler(this.richTextBoxText_TextChanged);
            // 
            // labelWarning
            // 
            this.labelWarning.AutoSize = true;
            this.labelWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWarning.Location = new System.Drawing.Point(304, 0);
            this.labelWarning.Name = "labelWarning";
            this.labelWarning.Size = new System.Drawing.Size(39, 12);
            this.labelWarning.TabIndex = 3;
            this.labelWarning.Text = "Warning";
            this.labelWarning.Click += new System.EventHandler(this.Warning_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Type";
            // 
            // comboBoxImgType
            // 
            this.comboBoxImgType.FormattingEnabled = true;
            this.comboBoxImgType.Items.AddRange(new object[] {
            "full_page",
            "medium",
            "small",
            "extra_small"});
            this.comboBoxImgType.Location = new System.Drawing.Point(43, 24);
            this.comboBoxImgType.Name = "comboBoxImgType";
            this.comboBoxImgType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxImgType.TabIndex = 4;
            this.comboBoxImgType.SelectedIndexChanged += new System.EventHandler(this.comboBoxImgType_SelectedIndexChanged);
            this.comboBoxImgType.TextChanged += new System.EventHandler(this.comboBoxImgType_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(175, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Anim";
            // 
            // textBoxImgId
            // 
            this.textBoxImgId.Location = new System.Drawing.Point(211, 24);
            this.textBoxImgId.Name = "textBoxImgId";
            this.textBoxImgId.Size = new System.Drawing.Size(154, 20);
            this.textBoxImgId.TabIndex = 6;
            this.textBoxImgId.TextChanged += new System.EventHandler(this.textBoxImgId_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(483, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "StartLine";
            // 
            // numericUpDownImgStartLine
            // 
            this.numericUpDownImgStartLine.Location = new System.Drawing.Point(538, 24);
            this.numericUpDownImgStartLine.Name = "numericUpDownImgStartLine";
            this.numericUpDownImgStartLine.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownImgStartLine.TabIndex = 8;
            this.numericUpDownImgStartLine.ValueChanged += new System.EventHandler(this.numericUpDownImgStartLine_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Title";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(37, 6);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(261, 20);
            this.textBoxTitle.TabIndex = 10;
            this.textBoxTitle.TextChanged += new System.EventHandler(this.textBoxTitle_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownAnim);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxImgId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxImgType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numericUpDownImgStartLine);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(37, 161);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(600, 60);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // numericUpDownAnim
            // 
            this.numericUpDownAnim.Location = new System.Drawing.Point(413, 24);
            this.numericUpDownAnim.Name = "numericUpDownAnim";
            this.numericUpDownAnim.Size = new System.Drawing.Size(71, 20);
            this.numericUpDownAnim.TabIndex = 10;
            this.numericUpDownAnim.ValueChanged += new System.EventHandler(this.numericUpDownAnim_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Frame";
            // 
            // PagePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.richTextBoxText);
            this.Controls.Add(this.labelWarning);
            this.Name = "PagePanel";
            this.Size = new System.Drawing.Size(650, 224);
            this.Load += new System.EventHandler(this.PagePanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownImgStartLine)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelWarning;
        private System.Windows.Forms.RichTextBox richTextBoxText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxImgType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxImgId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownImgStartLine;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownAnim;
        private System.Windows.Forms.Label label5;
    }
}
