﻿using System;
using System.Linq;
using System.Windows.Forms;
using MadNordsSupport.UI.BasePanels;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using System.Collections.Generic;

namespace MadNordsSupport.UI
{
    partial class ItemPanel : BaseItemPanel
    {
        List<GroupBox> _allGroupBoxes = new List<GroupBox>();

        public ItemPanel()
        {
            InitializeComponent();
            TryFileOnStart();

            _allGroupBoxes.Add(this.groupBoxMisc);
            _allGroupBoxes.Add(this.groupBoxParameters);
            _allGroupBoxes.Add(this.groupBoxPotion);

            if (CurrentItemType != null)
            {
                ActivateItemTypeParametersGroupBox(CurrentItemType);
            }
        }

        private void ItemPanel_Load(object sender, EventArgs e)
        {
            this.AddUpdateButton.Text = "Update Item";
        }

        protected override Button AddUpdateButton
        {
            get
            {
                return this.buttonAddUpdateEntity;
            }
        }

        protected override ItemTypeDAO itemTypeDAO
        {
            get
            {
                return ItemTypeDAO.Instance;
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                return this.textBoxLocationFileName;
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                return this.buttonRemoveEntity;
            }
        }

        private ItemType prevItemType;

        protected override ItemType CurrentItemType
        {
            get
            {
                string curText = ComboBoxItemType.Text;

                if (prevItemType == null || prevItemType.Id != curText)
                {
                    if (!this.itemTypeDAO.allData.ContainsKey(curText))
                    {
                        if (this.itemTypeDAO.allData.Count > 0)
                        {                            
                            curText = ComboBoxItemType.Text = this.itemTypeDAO.allData.Keys.FirstOrDefault(); ;
                        }
                        else
                        {
                            return null;
                        }
                    }

                    prevItemType = this.itemTypeDAO.allData[curText];
                }

                return prevItemType;
            }
        }

        public override void UpdateDataByFields(Item item, bool includeTitle)
        {
            if (includeTitle)
            {
                item._title = this.textBoxItemTitle.Text;
            }

            item._numericId = (int)this.NumericUpDownItemId.Value;
            item._description = this.textBoxDescription.Text;

            item.CleanAllFields();

            ItemType itemType = item.myOwner;

            switch (itemType.Id)
            {
                case ("quest"): break;
                case ("misc"): UpdateDataByMiscFields (item); break;
                case ("potion"): UpdateDataByPotionFields (item); break;
                default: UpdateDataByCommonFields (item); break;
            }
        }

        void UpdateDataByCommonFields (Item item)
        {
            item._st = (int)this.numericUpDown_parametersST.Value;
            item._ag = (int)this.numericUpDown_parametersAG.Value;
            item._in = (int)this.numericUpDown_parametersIN.Value;
            item._vi = (int)this.numericUpDown_parametersVI.Value;
            item._wi = (int)this.numericUpDown_parametersWI.Value;
            item._ac = (int)this.numericUpDown_parametersAC.Value;
            item._dc = (int)this.numericUpDown_parametersDC.Value;
            item._cc = (int)this.numericUpDown_parametersCC.Value;
            item._pr = (int)this.numericUpDown_parametersPR.Value;
            item._mr = (int)this.numericUpDown_parametersMR.Value;
        }

        void UpdateDataByPotionFields(Item item)
        {
            item._hp = (int)this.numericUpDown_potionHP.Value;
            item._mp = (int)this.numericUpDown_potionMP.Value;

            item._st = (int)this.numericUpDown_potionST.Value;
            item._ag = (int)this.numericUpDown_potionAG.Value;
            item._in = (int)this.numericUpDown_potionIN.Value;
            item._vi = (int)this.numericUpDown_potionVI.Value;
            item._wi = (int)this.numericUpDown_potionWI.Value;
        }

        void UpdateDataByMiscFields (Item item)
        {
            item._cost = (int)this.numericUpDown_miscCost.Value;
        }

        protected override void DisplayData(Item item
            , bool includeTitle
            )
        {
            if (includeTitle)
            {
                this.NumericUpDownItemId.Value = item._numericId;               
            }

            this.textBoxItemTitle.Text = item._title;
            this.textBoxDescription.Text = item._description;

            this.numericUpDown_parametersST.Value = item._st;
            this.numericUpDown_parametersAG.Value = item._ag;
            this.numericUpDown_parametersIN.Value = item._in;
            this.numericUpDown_parametersVI.Value = item._vi;
            this.numericUpDown_parametersWI.Value = item._wi;
            this.numericUpDown_parametersAC.Value = item._ac;
            this.numericUpDown_parametersDC.Value = item._dc;
            this.numericUpDown_parametersCC.Value = item._cc;
            this.numericUpDown_parametersPR.Value = item._pr;
            this.numericUpDown_parametersMR.Value = item._mr;

            this.numericUpDown_miscCost.Value = item._cost;
            
            this.numericUpDown_potionHP.Value = item._hp;
            this.numericUpDown_potionMP.Value = item._mp;
            this.numericUpDown_potionST.Value = item._st;
            this.numericUpDown_potionAG.Value = item._ag;
            this.numericUpDown_potionIN.Value = item._in;
            this.numericUpDown_potionVI.Value = item._vi;
            this.numericUpDown_potionWI.Value = item._wi;
        }

        protected override ComboBox ComboBoxItemType { get { return this.comboBox1; } }
        protected override TextBox TextBoxItemTitle { get { return this.textBoxItemTitle; } }
        protected override NumericUpDown NumericUpDownItemId { get { return this.numericUpItemId; } }

        private void EntityFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDataFile(this.openFileDialog1))
            {
                RenewAllFieldsByData(CurrentItemType);
            }
        }

        private void textBoxItemTitle_TextChanged(object sender, EventArgs e)
        {           
        }

        private void comboBoxItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void numericUpItemId_ValueChanged(object sender, EventArgs e)
        {
            TextBoxItemNumericIdChanged(CurrentItemType);
        }

        private void buttonAddUpdateEntity_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentData(CurrentItemType, (int)this.NumericUpDownItemId.Value, this.textBoxItemTitle.Text);
            DisplayItemCount();
        }

        private void buttonRemoveEntity_Click(object sender, EventArgs e)
        {
            RemoveItem(CurrentItemType, (int)this.NumericUpDownItemId.Value);
            DisplayItemCount();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateTitleTextBoxAutoComplete(CurrentItemType);
            NumericUpDownItemId.Value = 0;
            TextBoxItemNumericIdChanged(CurrentItemType);
        }

        protected override void UpdateTitleTextBoxAutoComplete(ItemType currentItemType)
        {
            AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();
            string[] arr = currentItemType.AllItemTitles;

            allowedTypes.AddRange(arr);
            TextBoxItemTitle.AutoCompleteCustomSource = allowedTypes;
            TextBoxItemTitle.AutoCompleteMode = AutoCompleteMode.Suggest;
            TextBoxItemTitle.AutoCompleteSource = AutoCompleteSource.CustomSource;

            ActivateItemTypeParametersGroupBox(currentItemType);

            DisplayItemCount();
        }

        protected void ActivateItemTypeParametersGroupBox(ItemType itemType)
        {
            List<GroupBox> groupBoxesToMakeVisible = new List<GroupBox>();
            //            foreach (GroupBox g in _allGroupBoxes) { groupBoxesToMakeVisible.Add(g); }

            GroupBox groupBoxToMakeVisible = null;

            switch (itemType.Id)
            {
                case ("quest"): break;
                case ("misc"): groupBoxToMakeVisible = (this.groupBoxMisc); break;
                case ("potion"): groupBoxToMakeVisible = (this.groupBoxPotion); break;
                default: groupBoxToMakeVisible = (this.groupBoxParameters); break;
            }

            foreach (GroupBox g in _allGroupBoxes)
            {
                if (groupBoxToMakeVisible == g)
                {
                    g.Visible = true;
                } else
                {
                    g.Visible = false;
                }
            }
        }

        protected virtual void DisplayItemCount ()
        {
            this.labelItemsCount.Text = "Suggested next id = " + (CurrentItemType.items.Count);
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void groupBoxMisc_Enter(object sender, EventArgs e)
        {

        }
    }
}
