﻿using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI.BasePanels
{
    class BaseItemPanel : UserControl
    {
        #region unsupportedlogic
        protected virtual ItemTypeDAO itemTypeDAO
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public virtual void UpdateDataByFields(Item data, bool isRenewing)
        {
            throw new NotSupportedException();
        }

        protected virtual void DisplayData(Item l, bool isRenewing)
        {
            throw new NotSupportedException();
        }

        protected virtual TextBox FileNameTextBox {
            get
            {
                throw new NotSupportedException();
            }
        }
        protected virtual Button AddUpdateButton {
            get
            {
                throw new NotSupportedException();
            }
        }
        protected virtual Button RemoveButton {
            get
            {
                throw new NotSupportedException();
            }
        }

        protected virtual TextBox TextBoxItemTitle {
            get
            {
                throw new NotSupportedException();
            }
        }

        protected virtual void UpdateTitleTextBoxAutoComplete(ItemType currentItemType)
        {
            throw new NotSupportedException();
        }

        protected virtual ItemType CurrentItemType
        {
            get {
                throw new NotSupportedException();
            }
        }

        protected virtual NumericUpDown NumericUpDownItemId
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        protected virtual ComboBox ComboBoxItemType { get { throw new NotSupportedException(); } }
        #endregion

        protected void TryFileOnStart()
        {
            string file = itemTypeDAO.DataSourceName;
            TryReadFile(file);
        }

        protected bool SelectDataFile(OpenFileDialog openFileDialog)
        {
            bool res = false;
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog.FileName;
                if (TryReadFile(file))
                {
                    res = true;
                }
            }
            return res;
        }

        protected bool TryReadFile(string file)
        {
            try
            {
                if (this.itemTypeDAO.TryReadFrom(file))
                {
                    this.FileNameTextBox.Text = file;
                    this.AddUpdateButton.Enabled = true;
                    DisplayItemTypes();

                    ItemType it = CurrentItemType;
                    RenewAllFieldsByData(it);
                    UpdateTitleTextBoxAutoComplete(it);
                    return true;
                }
            }
            catch (IOException)
            {
            }
            return false;
        }

        private void DisplayItemTypes ()
        {
            this.ComboBoxItemType.Items.Clear();
            this.ComboBoxItemType.Items.AddRange(itemTypeDAO.allData.Keys.ToArray ());
        }

        protected bool isRenewingFields = false;
        protected bool isRenewingId = false;

        protected void RenewAllFieldsByData(ItemType itemType)
        {
            if (isRenewingFields) { return; }
            else { isRenewingFields = true; }

            Item l = null;
            if (itemType.HasItemNumericId ((int)NumericUpDownItemId.Value))
            {
                l = itemType.GetItemByNumericId((int)NumericUpDownItemId.Value);
                DisplayData(l, !isRenewingId);
            }
            else
            {
                //int nextItemId = itemType.BiggestItemId;
                //NumericUpDownItemId.Value = nextItemId;
             }

            isRenewingFields = false;
        }

        protected void RemoveItem(ItemType itemTypeOwner, int numericId)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                this.itemTypeDAO.Delete(itemTypeOwner.GetItemByNumericId (numericId));
                UpdateTitleTextBoxAutoComplete(itemTypeOwner);

                this.TextBoxItemTitle.Text = "new one";
                this.RenewAllFieldsByData(itemTypeOwner);
            }
            else
            {
                // If 'No', do something here.
            }
        }

        protected bool AddUpdateCurrentData(ItemType currentItemType, int numericId, string title)
        {
            bool isNew = !(currentItemType.HasItemNumericId(numericId));

            Item d = currentItemType.GetCreateItemById(numericId, title);
            UpdateDataByFields(d, true);            

            if (this.itemTypeDAO.AddUpdate(d))
            {
                if (isNew)
                {
                    UpdateTitleTextBoxAutoComplete(currentItemType);
                    TextBoxItemNumericIdChanged(currentItemType);
                }
                this.RenewAllFieldsByData(currentItemType);
                return true;
            }
            return false;
        }

        protected bool TextBoxItemNumericIdChanged(ItemType itemType)
        {
            bool newOne = false;
            isRenewingId = true;
            if (itemType.HasItemNumericId((int)NumericUpDownItemId.Value))
            {
                AddUpdateButton.Text = "Update Item";
                RemoveButton.Enabled = true;
            }
            else
            {
                AddUpdateButton.Text = "Add Item";
                RemoveButton.Enabled = false;
                newOne = true;
            }

            RenewAllFieldsByData(itemType);
            isRenewingId = false;
            return newOne;
        }
    }
}
