﻿using MadNordsSupport.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI.BasePanels
{
    class BaseMerchantPanel : TemplatePanel<Merchant, MerchantDAO>
    {
        public override TextBox TextBoxDataId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override MerchantDAO dao
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void UpdateDataByFields(Merchant data, bool includeId)
        {
            throw new NotImplementedException();
        }

        protected override void DisplayData(Merchant l, bool includeId)
        {
            throw new NotImplementedException();
        }
    }
}
