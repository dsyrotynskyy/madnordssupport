﻿using MadNordsSupport.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI.BasePanels
{
    class BaseReadingPanel : TemplatePanel<Sign, SignDAO>
    {
        public override TextBox TextBoxDataId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override SignDAO dao
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void UpdateDataByFields(Sign data, bool includeId)
        {
            throw new NotImplementedException();
        }

        protected override void DisplayData(Sign l, bool includeId)
        {
            throw new NotImplementedException();
        }
    }
}
