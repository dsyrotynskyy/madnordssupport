﻿using MadNordsSupport.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI.BasePanels
{
    public class BaseLocationPanel : TemplatePanel<Location, LocationDAO>
    {
        public override TextBox TextBoxDataId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override LocationDAO dao
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void UpdateDataByFields(Location data, bool includeId)
        {
            throw new NotImplementedException();
        }

        protected override void DisplayData(Location l, bool includeId)
        {
            throw new NotImplementedException();
        }
    }
}
