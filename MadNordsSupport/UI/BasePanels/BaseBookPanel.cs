﻿using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport.UI.BasePanels
{
    public class BaseBookPanel : TemplatePanel<Book, BookDAO>
    {
        public override TextBox TextBoxDataId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override BookDAO dao
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void UpdateDataByFields(Book data, bool includeId)
        {
            throw new NotImplementedException();
        }

        protected override void DisplayData(Book l, bool includeId)
        {
            throw new NotImplementedException();
        }
    }
}
