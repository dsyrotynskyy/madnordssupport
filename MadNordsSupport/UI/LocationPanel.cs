﻿using System;
using System.Windows.Forms;
using MadNordsSupport.Logic;
using MadNordsSupport.UI.BasePanels;

namespace MadNordsSupport.UI
{
    public sealed partial class LocationPanel : BaseLocationPanel
    {
        public LocationPanel()
        {
            InitializeComponent();

            TryFileOnStart();
        }

        protected override LocationDAO dao
        {
            get
            {
                return LocationDAO.Instance;
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                return this.textBoxFileName;
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                return this.buttonAddUpdateLocation;
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                return this.buttonRemoveLocation;
            }
        }

        public override TextBox TextBoxDataId
        {
            get
            {
                return this.textBoxLocationId;
            }
        }

        private void LocationPanel_Load(object sender, EventArgs e)
        {
            this.textBoxLocationId.TextAlign = HorizontalAlignment.Left;
        }

        protected override void DisplayData(Location data, bool includeId)
        {
            if (includeId)
            {
                this.textBoxLocationId.Text = data.Id;
            }
            this.textBoxLocationLocation.Text = data.location;
            this.numericUpDownX.Value = (decimal)data.x;
            this.numericUpDownY.Value = (decimal)data.y;
            this.textBoxName.Text = data.name;
        }

        public override void UpdateDataByFields(Location l, bool includeId)
        {
            if (includeId)
            {
                l.Id = this.textBoxLocationId.Text;
            }

            l.location = this.textBoxLocationLocation.Text;
            l.name = this.textBoxName.Text;
            l.x = (float)this.numericUpDownX.Value;
            l.y = (float)numericUpDownY.Value;
        }

        Location locationTemp;

        private void buttonCopyLocation_Click(object sender, EventArgs e)
        {
            if (locationTemp == null)
            {
                locationTemp = new Location();
            }
            UpdateDataByFields(locationTemp, false);
            this.buttonPasteLocation.Enabled = true;
        }

        private void buttonPasteLocation_Click(object sender, EventArgs e)
        {
            if (locationTemp == null)
            {
                Console.WriteLine("Error! Empty clipboard while trying to paste!");
                return;
            }
            DisplayData(locationTemp, false);
        }

        private void buttonAddUpdateLocation_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentData();
        }

        private void buttonRemoveLocation_Click(object sender, EventArgs e)
        {
            RemoveCurrentEntity();
        }

        private void textBoxLocationId_TextChanged(object sender, EventArgs e)
        {
            TextBoxIdChanged();
        }

        private void questFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDataFile(this.openFileDialog1))
            {
                RenewAllFieldsByData();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBoxLocationLocation_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
