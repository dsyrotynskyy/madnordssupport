﻿namespace MadNordsSupport
{
    partial class TierEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tierIdBox = new System.Windows.Forms.TextBox();
            this.NPC = new System.Windows.Forms.Label();
            this.richTextBoxNPC = new System.Windows.Forms.RichTextBox();
            this.buttonSaveTier = new System.Windows.Forms.Button();
            this.advancedFlowLayoutPanel1 = new MakarovDev.ExpandCollapsePanel.AdvancedFlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAnswer3 = new System.Windows.Forms.TextBox();
            this.expandCollapsePanel1 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.tierAnswerPanel3 = new MadNordsSupport.AnswerPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAnswer2 = new System.Windows.Forms.TextBox();
            this.expandCollapsePanel2 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.tierAnswerPanel2 = new MadNordsSupport.AnswerPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAnswer1 = new System.Windows.Forms.TextBox();
            this.expandCollapsePanel3 = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.tierAnswerPanel1 = new MadNordsSupport.AnswerPanel();
            this.buttonApplyTier = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.advancedFlowLayoutPanel1.SuspendLayout();
            this.expandCollapsePanel1.SuspendLayout();
            this.expandCollapsePanel2.SuspendLayout();
            this.expandCollapsePanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id";
            // 
            // tierIdBox
            // 
            this.tierIdBox.Location = new System.Drawing.Point(47, 11);
            this.tierIdBox.Name = "tierIdBox";
            this.tierIdBox.Size = new System.Drawing.Size(74, 20);
            this.tierIdBox.TabIndex = 1;
            // 
            // NPC
            // 
            this.NPC.AutoSize = true;
            this.NPC.Location = new System.Drawing.Point(12, 37);
            this.NPC.Name = "NPC";
            this.NPC.Size = new System.Drawing.Size(29, 13);
            this.NPC.TabIndex = 2;
            this.NPC.Text = "NPC";
            // 
            // richTextBoxNPC
            // 
            this.richTextBoxNPC.Location = new System.Drawing.Point(47, 37);
            this.richTextBoxNPC.Name = "richTextBoxNPC";
            this.richTextBoxNPC.Size = new System.Drawing.Size(622, 66);
            this.richTextBoxNPC.TabIndex = 3;
            this.richTextBoxNPC.Text = "";
            this.richTextBoxNPC.TextChanged += new System.EventHandler(this.richTextBoxNPC_TextChanged);
            // 
            // buttonSaveTier
            // 
            this.buttonSaveTier.Location = new System.Drawing.Point(594, 8);
            this.buttonSaveTier.Name = "buttonSaveTier";
            this.buttonSaveTier.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveTier.TabIndex = 6;
            this.buttonSaveTier.Text = "Save Tier";
            this.buttonSaveTier.UseVisualStyleBackColor = true;
            this.buttonSaveTier.Click += new System.EventHandler(this.buttonSaveTier_Click);
            // 
            // advancedFlowLayoutPanel1
            // 
            this.advancedFlowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedFlowLayoutPanel1.AutoScroll = true;
            this.advancedFlowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.advancedFlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedFlowLayoutPanel1.Controls.Add(this.label3);
            this.advancedFlowLayoutPanel1.Controls.Add(this.textBoxAnswer3);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel1);
            this.advancedFlowLayoutPanel1.Controls.Add(this.label2);
            this.advancedFlowLayoutPanel1.Controls.Add(this.textBoxAnswer2);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel2);
            this.advancedFlowLayoutPanel1.Controls.Add(this.label4);
            this.advancedFlowLayoutPanel1.Controls.Add(this.textBoxAnswer1);
            this.advancedFlowLayoutPanel1.Controls.Add(this.expandCollapsePanel3);
            this.advancedFlowLayoutPanel1.Location = new System.Drawing.Point(48, 109);
            this.advancedFlowLayoutPanel1.Name = "advancedFlowLayoutPanel1";
            this.advancedFlowLayoutPanel1.Size = new System.Drawing.Size(621, 492);
            this.advancedFlowLayoutPanel1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 44;
            this.label3.Text = "Answer3";
            // 
            // textBoxAnswer3
            // 
            this.textBoxAnswer3.Location = new System.Drawing.Point(3, 20);
            this.textBoxAnswer3.Name = "textBoxAnswer3";
            this.textBoxAnswer3.Size = new System.Drawing.Size(596, 20);
            this.textBoxAnswer3.TabIndex = 43;
            this.textBoxAnswer3.TextChanged += new System.EventHandler(this.textBoxAnswer3_TextChanged);
            // 
            // expandCollapsePanel1
            // 
            this.expandCollapsePanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel1.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel1.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel1.Controls.Add(this.tierAnswerPanel3);
            this.expandCollapsePanel1.ExpandedHeight = 367;
            this.expandCollapsePanel1.IsExpanded = true;
            this.expandCollapsePanel1.Location = new System.Drawing.Point(3, 46);
            this.expandCollapsePanel1.Name = "expandCollapsePanel1";
            this.expandCollapsePanel1.Size = new System.Drawing.Size(596, 790);
            this.expandCollapsePanel1.TabIndex = 45;
            this.expandCollapsePanel1.Text = "\" \"";
            this.expandCollapsePanel1.UseAnimation = false;
            // 
            // tierAnswerPanel3
            // 
            this.tierAnswerPanel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tierAnswerPanel3.Location = new System.Drawing.Point(-5, 50);
            this.tierAnswerPanel3.Name = "tierAnswerPanel3";
            this.tierAnswerPanel3.Size = new System.Drawing.Size(622, 734);
            this.tierAnswerPanel3.TabIndex = 1;
            this.tierAnswerPanel3.Load += new System.EventHandler(this.tierAnswerPanel3_Load);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 839);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 47;
            this.label2.Text = "Answer2";
            // 
            // textBoxAnswer2
            // 
            this.textBoxAnswer2.Location = new System.Drawing.Point(3, 859);
            this.textBoxAnswer2.Name = "textBoxAnswer2";
            this.textBoxAnswer2.Size = new System.Drawing.Size(596, 20);
            this.textBoxAnswer2.TabIndex = 46;
            this.textBoxAnswer2.TextChanged += new System.EventHandler(this.textBoxAnswer2_TextChanged);
            // 
            // expandCollapsePanel2
            // 
            this.expandCollapsePanel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel2.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel2.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel2.Controls.Add(this.tierAnswerPanel2);
            this.expandCollapsePanel2.ExpandedHeight = 0;
            this.expandCollapsePanel2.IsExpanded = true;
            this.expandCollapsePanel2.Location = new System.Drawing.Point(3, 885);
            this.expandCollapsePanel2.Name = "expandCollapsePanel2";
            this.expandCollapsePanel2.Size = new System.Drawing.Size(596, 790);
            this.expandCollapsePanel2.TabIndex = 48;
            this.expandCollapsePanel2.Text = "Заголовок";
            this.expandCollapsePanel2.UseAnimation = false;
            // 
            // tierAnswerPanel2
            // 
            this.tierAnswerPanel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tierAnswerPanel2.Location = new System.Drawing.Point(-5, 50);
            this.tierAnswerPanel2.Name = "tierAnswerPanel2";
            this.tierAnswerPanel2.Size = new System.Drawing.Size(622, 734);
            this.tierAnswerPanel2.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 1678);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 50;
            this.label4.Text = "Answer1";
            // 
            // textBoxAnswer1
            // 
            this.textBoxAnswer1.Location = new System.Drawing.Point(3, 1698);
            this.textBoxAnswer1.Name = "textBoxAnswer1";
            this.textBoxAnswer1.Size = new System.Drawing.Size(596, 20);
            this.textBoxAnswer1.TabIndex = 49;
            this.textBoxAnswer1.TextChanged += new System.EventHandler(this.textBoxAnswer1_TextChanged);
            // 
            // expandCollapsePanel3
            // 
            this.expandCollapsePanel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expandCollapsePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.expandCollapsePanel3.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Small;
            this.expandCollapsePanel3.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Classic;
            this.expandCollapsePanel3.Controls.Add(this.tierAnswerPanel1);
            this.expandCollapsePanel3.ExpandedHeight = 0;
            this.expandCollapsePanel3.IsExpanded = true;
            this.expandCollapsePanel3.Location = new System.Drawing.Point(3, 1724);
            this.expandCollapsePanel3.Name = "expandCollapsePanel3";
            this.expandCollapsePanel3.Size = new System.Drawing.Size(596, 790);
            this.expandCollapsePanel3.TabIndex = 51;
            this.expandCollapsePanel3.Text = "Заголовок";
            this.expandCollapsePanel3.UseAnimation = false;
            // 
            // tierAnswerPanel1
            // 
            this.tierAnswerPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tierAnswerPanel1.Location = new System.Drawing.Point(-5, 50);
            this.tierAnswerPanel1.Name = "tierAnswerPanel1";
            this.tierAnswerPanel1.Size = new System.Drawing.Size(622, 734);
            this.tierAnswerPanel1.TabIndex = 1;
            // 
            // buttonApplyTier
            // 
            this.buttonApplyTier.Location = new System.Drawing.Point(513, 9);
            this.buttonApplyTier.Name = "buttonApplyTier";
            this.buttonApplyTier.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyTier.TabIndex = 7;
            this.buttonApplyTier.Text = "Apply Tier";
            this.buttonApplyTier.UseVisualStyleBackColor = true;
            this.buttonApplyTier.Click += new System.EventHandler(this.buttonApplyTier_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(127, 9);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(75, 23);
            this.buttonPrev.TabIndex = 8;
            this.buttonPrev.Text = "Prev";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(208, 8);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 9;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // TierEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 615);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.buttonApplyTier);
            this.Controls.Add(this.buttonSaveTier);
            this.Controls.Add(this.advancedFlowLayoutPanel1);
            this.Controls.Add(this.richTextBoxNPC);
            this.Controls.Add(this.NPC);
            this.Controls.Add(this.tierIdBox);
            this.Controls.Add(this.label1);
            this.Name = "TierEditForm";
            this.Text = "TierEdit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TierEditForm_FormClosing);
            this.Load += new System.EventHandler(this.TierEditForm_Load);
            this.advancedFlowLayoutPanel1.ResumeLayout(false);
            this.advancedFlowLayoutPanel1.PerformLayout();
            this.expandCollapsePanel1.ResumeLayout(false);
            this.expandCollapsePanel1.PerformLayout();
            this.expandCollapsePanel2.ResumeLayout(false);
            this.expandCollapsePanel2.PerformLayout();
            this.expandCollapsePanel3.ResumeLayout(false);
            this.expandCollapsePanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tierIdBox;
        private System.Windows.Forms.Label NPC;
        private System.Windows.Forms.RichTextBox richTextBoxNPC;
        private MakarovDev.ExpandCollapsePanel.AdvancedFlowLayoutPanel advancedFlowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAnswer3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAnswer2;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel2;
        private AnswerPanel tierAnswerPanel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAnswer1;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel3;
        private AnswerPanel tierAnswerPanel1;
        private System.Windows.Forms.Button buttonSaveTier;
        private System.Windows.Forms.Button buttonApplyTier;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel expandCollapsePanel1;
        private AnswerPanel tierAnswerPanel3;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonNext;
    }
}