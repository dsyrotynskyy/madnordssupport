﻿using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport
{
    public partial class TierEditForm : Form
    {
        private Dialog.Tier tmpTier;
        private Dialog.Tier targetTier;
        private DialogPanel owner;

        private Dialog.Tier.Answer answer1;
        private Dialog.Tier.Answer answer2;
        private Dialog.Tier.Answer answer3;

        public TierEditForm()
        {
            InitializeComponent();
        }

        Dialog.Tier prevTier;
        Dialog.Tier nextTier;

        internal void Init(Dialog.Tier t, DialogPanel owner)
        {
            this.targetTier = t;
            this.tmpTier = t.Clone(null);
            this.owner = owner;

            this.answer1 = this.tmpTier.answers[0];
            this.answer2 = this.tmpTier.answers[1];
            this.answer3 = this.tmpTier.answers[2];

            isAnswerTextInit = true;
            textBoxAnswer1.Text = this.answer1.text;
            textBoxAnswer2.Text = answer2.text;
            textBoxAnswer3.Text = answer3.text;
            isAnswerTextInit = false;

            bool startOpened = false;
            this.expandCollapsePanel1.IsExpanded = startOpened;
            this.expandCollapsePanel2.IsExpanded = startOpened;
            this.expandCollapsePanel3.IsExpanded = startOpened;

            this.tierAnswerPanel3.Init(answer3, this);
            this.tierAnswerPanel2.Init(answer2, this);
            this.tierAnswerPanel1.Init(answer1, this);

            prevTier = this.owner.GetPrevTier(this.targetTier);
            nextTier = this.owner.GetNextTier(this.targetTier);

            if (prevTier == null)
            {
                this.buttonPrev.Enabled = false;
            } else
            {
                this.buttonPrev.Enabled = true;
            }

            if (nextTier == null)
            {
                this.buttonNext.Enabled = false;
            } else
            {
                this.buttonNext.Enabled = true;
            }

            PreLoad();
        }

        private void PreLoad ()
        {
            this.tierIdBox.Text = tmpTier.id;
            this.richTextBoxNPC.Text = targetTier.NPCText;

            this.expandCollapsePanel1.ForeColor = Color.Black;
            this.expandCollapsePanel2.ForeColor = Color.Black;
            this.expandCollapsePanel3.ForeColor = Color.Black;

            string answerTitle = " Properties";

            this.expandCollapsePanel1.Text = "Answer3 " + answerTitle;
            this.expandCollapsePanel2.Text = "Answer2 " + answerTitle;
            this.expandCollapsePanel3.Text = "Answer1 " + answerTitle;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void expandCollapsePanel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void textBoxAnswer1_TextChanged(object sender, EventArgs e)
        {
            CheckFieldsValid();
        }

        private void textBoxAnswer2_TextChanged(object sender, EventArgs e)
        {
            CheckFieldsValid();
        }

        private void textBoxAnswer3_TextChanged(object sender, EventArgs e)
        {
        }

        bool isAnswerTextInit = false;

        public void OrderValidateFields ()
        {
            if (!this.Visible)
            {
                return;
            }
            CheckFieldsValid();
        }

        private void CheckFieldsValid()
        {
            if (isAnswerTextInit)
            {
                return;
            }

            this.buttonApplyTier.Enabled = true;
            this.buttonSaveTier.Enabled = true;

            this.tmpTier.NPCText = this.richTextBoxNPC.Text;
            this.answer1.text = textBoxAnswer1.Text;  
            this.answer2.text = textBoxAnswer2.Text;
            this.answer3.text = textBoxAnswer3.Text;

            this.tierAnswerPanel1.UpdateCurrentAnswerDataByUI();
            this.tierAnswerPanel2.UpdateCurrentAnswerDataByUI();
            this.tierAnswerPanel3.UpdateCurrentAnswerDataByUI();

            bool badAnswerFields = false;

            bool answer1Error = this.answer1.ValidnessState(false).HasError;

            if (answer1Error)
            {
                textBoxAnswer2.Enabled = false;
                textBoxAnswer3.Enabled = false;
            } else {
                if (!tierAnswerPanel1.CheckCorrectFields())
                {
                    badAnswerFields = true;
                }

                textBoxAnswer2.Enabled = true;
                if (string.IsNullOrEmpty(answer2.text))
                {
                    textBoxAnswer3.Enabled = false;
                }
                else
                {
                    textBoxAnswer3.Enabled = true;
                    if (!tierAnswerPanel2.CheckCorrectFields())
                    {
                        badAnswerFields = true;
                    }

                    if (!string.IsNullOrEmpty(answer3.text) && !tierAnswerPanel3.CheckCorrectFields())
                    {
                        badAnswerFields = true;
                    }
                }
            }            
                    
            if (answer1Error || string.IsNullOrEmpty(this.tmpTier.NPCText) || badAnswerFields)
            {
                this.buttonApplyTier.Enabled = false;
                this.buttonSaveTier.Enabled = false;
            }

            tierAnswerPanel2.Enabled = textBoxAnswer2.Enabled;
            tierAnswerPanel3.Enabled = textBoxAnswer3.Enabled;
        }

        private void buttonSaveTier_Click(object sender, EventArgs e)
        {
            ApplyTier();
            this.Hide();
            this.Dispose();
        }

        private void buttonApplyTier_Click(object sender, EventArgs e)
        {
            ApplyTier();
        }

        void ApplyTier()
        {
            this.tierAnswerPanel1.UpdateCurrentAnswerDataByUI();
            this.tierAnswerPanel2.UpdateCurrentAnswerDataByUI();
            this.tierAnswerPanel3.UpdateCurrentAnswerDataByUI();

            string text1 = textBoxAnswer1.Text;
            string text2 = textBoxAnswer2.Text;
            string text3 = textBoxAnswer3.Text;

            if (text2 == null || text2 == "")
            {
                text2 = null;
                text3 = null;
            }

            answer1.text = text1;
            answer2.text = text2;
            answer3.text = text3;

            this.tmpTier.id = this.tierIdBox.Text;
            this.tmpTier.NPCText = this.richTextBoxNPC.Text;
            this.targetTier.id = this.tmpTier.id;
            this.targetTier.CopyFrom(this.tmpTier);
            this.owner.RefreshDataFields();
        }

        private void richTextBoxNPC_TextChanged(object sender, EventArgs e)
        {
            CheckFieldsValid();
        }

        private void tierAnswerPanel3_Load(object sender, EventArgs e)
        {

        }

        private void TierEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ApplyTier();
            this.owner.AddUpdateCurrentDialog(false);
        }

        private void TierEditForm_Load(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            ApplyTier();
            this.Hide();
            this.Dispose();

            TierEditForm t = this.owner.DisplayTierEdit(prevTier);
            t.Location = this.Location;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ApplyTier();
            this.Hide();
            this.Dispose();

            TierEditForm t = this.owner.DisplayTierEdit(nextTier);
            t.Location = this.Location;
        }
    }
}
