﻿using System;

namespace MadNordsSupport.UI
{
    partial class GiveItemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rewardGroupBox = new System.Windows.Forms.GroupBox();
            this.comboBoxGiveItemId = new System.Windows.Forms.ComboBox();
            this.comboBoxGiveItemType = new System.Windows.Forms.ComboBox();
            this.labelItemType = new System.Windows.Forms.Label();
            this.labelItemId = new System.Windows.Forms.Label();
            this.rewardGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // rewardGroupBox
            // 
            this.rewardGroupBox.Controls.Add(this.comboBoxGiveItemId);
            this.rewardGroupBox.Controls.Add(this.comboBoxGiveItemType);
            this.rewardGroupBox.Controls.Add(this.labelItemType);
            this.rewardGroupBox.Controls.Add(this.labelItemId);
            this.rewardGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rewardGroupBox.Location = new System.Drawing.Point(0, 0);
            this.rewardGroupBox.Name = "rewardGroupBox";
            this.rewardGroupBox.Size = new System.Drawing.Size(382, 45);
            this.rewardGroupBox.TabIndex = 43;
            this.rewardGroupBox.TabStop = false;
            this.rewardGroupBox.Text = "Give Item";
            this.rewardGroupBox.Enter += new System.EventHandler(this.groupBox9_Enter);
            // 
            // comboBoxGiveItemId
            // 
            this.comboBoxGiveItemId.FormattingEnabled = true;
            this.comboBoxGiveItemId.Items.AddRange(new object[] {
            "weapon",
            "armor",
            "artefact",
            "potion",
            "misc",
            "quest"});
            this.comboBoxGiveItemId.Location = new System.Drawing.Point(161, 13);
            this.comboBoxGiveItemId.Name = "comboBoxGiveItemId";
            this.comboBoxGiveItemId.Size = new System.Drawing.Size(204, 21);
            this.comboBoxGiveItemId.TabIndex = 34;
            this.comboBoxGiveItemId.TextChanged += new System.EventHandler(this.comboBoxGiveItemId_TextChanged);
            // 
            // comboBoxGiveItemType
            // 
            this.comboBoxGiveItemType.FormattingEnabled = true;
            this.comboBoxGiveItemType.Items.AddRange(new object[] {
            "weapon",
            "armor",
            "artefact",
            "potion",
            "misc",
            "quest"});
            this.comboBoxGiveItemType.Location = new System.Drawing.Point(43, 14);
            this.comboBoxGiveItemType.Name = "comboBoxGiveItemType";
            this.comboBoxGiveItemType.Size = new System.Drawing.Size(79, 21);
            this.comboBoxGiveItemType.TabIndex = 33;
            this.comboBoxGiveItemType.SelectedIndexChanged += new System.EventHandler(this.comboBoxGiveItemType_SelectedIndexChanged);
            this.comboBoxGiveItemType.TextChanged += new System.EventHandler(this.comboBoxGiveItemType_TextChanged);
            // 
            // labelItemType
            // 
            this.labelItemType.AutoSize = true;
            this.labelItemType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelItemType.Location = new System.Drawing.Point(6, 16);
            this.labelItemType.Name = "labelItemType";
            this.labelItemType.Size = new System.Drawing.Size(31, 13);
            this.labelItemType.TabIndex = 18;
            this.labelItemType.Text = "Type";
            // 
            // labelItemId
            // 
            this.labelItemId.AutoSize = true;
            this.labelItemId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelItemId.Location = new System.Drawing.Point(128, 17);
            this.labelItemId.Name = "labelItemId";
            this.labelItemId.Size = new System.Drawing.Size(16, 13);
            this.labelItemId.TabIndex = 21;
            this.labelItemId.Text = "Id";
            // 
            // GiveItemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rewardGroupBox);
            this.Name = "GiveItemPanel";
            this.Size = new System.Drawing.Size(426, 64);
            this.Load += new System.EventHandler(this.GiveItemPanel_Load);
            this.rewardGroupBox.ResumeLayout(false);
            this.rewardGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox rewardGroupBox;
        private System.Windows.Forms.ComboBox comboBoxGiveItemId;
        private System.Windows.Forms.ComboBox comboBoxGiveItemType;
        private System.Windows.Forms.Label labelItemType;
        private System.Windows.Forms.Label labelItemId;
    }
}
