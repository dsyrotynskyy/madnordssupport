﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport.UI
{
    public partial class RewardItemPanel : UserControl
    {
        public RewardItemPanel()
        {
            InitializeComponent();
        }

        private AnswerPanel owner;

        internal void Init(AnswerPanel owner, Dialog.Tier.Answer.RewardItem rewardItem)
        {
            this.owner = owner;
            this.groupBox7.Text = "Item " + rewardItem.arrayPosition;
        }

        internal void UpdateUIByCurrentReward(Dialog.Tier.Answer.RewardItem sourceItem)
        {
            if (sourceItem.IsCorrect)
            {
                this.comboBoxRewardType.Text = sourceItem.type;

                ItemType desiredItemType = ItemTypeDAO.Instance.allData[sourceItem.type];
                string txt = desiredItemType.GetItemNameByNumericId(sourceItem.rewardId);

                this.comboBoxRewardId.Text = txt;
                this.comboBoxRewardEnchanment.Text = sourceItem.rewardEnchant;
            } else
            {
                this.comboBoxRewardType.Text = "";
                this.comboBoxRewardId.Text = "";
                this.comboBoxRewardEnchanment.Text = "";
            }
        }

        internal void UpdateRewardByUI(Dialog.Tier.Answer.RewardItem sourceItem)
        {
            if (IsCorrectAndEnabled)
            {
                sourceItem.rewardId = Item.GetIdFromItemName (comboBoxRewardId.Text);
                sourceItem.type = comboBoxRewardType.Text;
                sourceItem.rewardEnchant = this.comboBoxRewardEnchanment.Text;
            } else
            {
                sourceItem.rewardId = "";
                sourceItem.type = "";
                sourceItem.rewardEnchant = "";
            }
        }

        public void ValidatePanel()
        {

        }

        private void comboBoxRewardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.owner.OrderValidateFields();
        }

        private void comboBoxRewardEnchanment_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.owner.OrderValidateFields();
        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        public bool IsCorrect
        {
            get
            {
                bool typeGood = !string.IsNullOrEmpty(this.comboBoxRewardType.Text);
                bool idGood = !string.IsNullOrEmpty(this.comboBoxRewardId.Text);
                return typeGood && idGood;
            }
        }

        public bool IsCorrectAndEnabled
        {
            get
            {
                return this.Enabled && IsCorrect;
            }
        }

        private void comboBoxRewardType_TextChanged(object sender, EventArgs e)
        {
            this.owner.OrderValidateFields();

            string type = comboBoxRewardType.Text;
            ItemType itemType = ItemTypeDAO.Instance.allData[type];

            comboBoxRewardId.Items.Clear();
            comboBoxRewardId.Items.AddRange(itemType.itemsNumTitlesNames.ToArray());
            if (!comboBoxRewardId.Items.Contains(comboBoxRewardId.Text))
            {
                comboBoxRewardId.Text = comboBoxRewardId.Items[0].ToString ();
                comboBoxRewardEnchanment.Text = "";
            }

            if (type == "potion" || type == "misc" || type == "quest")
            {
                comboBoxRewardEnchanment.Enabled = false;
            } else
            {
                comboBoxRewardEnchanment.Enabled = true;
            }

            if (type == "")
            {
                comboBoxRewardId.Text = "";
                comboBoxRewardEnchanment.Text = "";
            }
        }

        private void comboBoxRewardId_TextChanged(object sender, EventArgs e)
        {
            this.owner.OrderValidateFields();
        }

        internal bool CheckCorrectData()
        {
            bool res = true;

            comboBoxRewardType.ForeColor = Color.Black;
            comboBoxRewardId.ForeColor = Color.Black;
            comboBoxRewardEnchanment.ForeColor = Color.Black;

            if (comboBoxRewardId.Text == "" && comboBoxRewardType.Text == "")
            {
                res = true;
            } else
            {
                if (!comboBoxRewardType.Items.Contains(comboBoxRewardType.Text))
                {
                    res = false;
                }

                if (!comboBoxRewardId.Items.Contains(comboBoxRewardId.Text))
                {
                    res = false;
                }

                if (comboBoxRewardEnchanment.Text != "" && !comboBoxRewardEnchanment.Items.Contains(comboBoxRewardEnchanment.Text))
                {
                   res = false;
                }
            }

            if (!res)
            {
                comboBoxRewardType.ForeColor = Color.Red;
                comboBoxRewardId.ForeColor = Color.Red;
                comboBoxRewardEnchanment.ForeColor = Color.Red;
            }

            return res;
        }

        private void comboBoxRewardEnchanment_TextChanged(object sender, EventArgs e)
        {
            this.owner.OrderValidateFields();
        }
    }
}
