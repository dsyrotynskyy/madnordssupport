﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;

namespace MadNordsSupport
{
    public partial class AnswerPanel : UserControl
    {
        QuestDAO questDAO
        {
            get
            {
                return QuestDAO.Instance;
            }
        }

        public AnswerPanel()
        {
            InitializeComponent();
        }

        Dialog.Tier.Answer answer;
        TierEditForm owner;

        internal void Init(Dialog.Tier.Answer a, TierEditForm formOwner)
        {
            this.owner = formOwner;
            this.answer = a;

            giveItemPanel1.Init(this);
            rewardItemPanel1.Init(this, a.rewardItems[0]);
            rewardItemPanel2.Init(this, a.rewardItems[1]);
            rewardItemPanel3.Init(this, a.rewardItems[2]);
            rewardItemPanel4.Init(this, a.rewardItems[3]);

            UpdateUIByCurrentAnswerData();

            PreLoad();
        }

        private void PreLoad ()
        {
            comboBoxQuestQuestId.Items.Clear();
            comboBoxMarkerQuestId.Items.Clear();

            foreach (Quest q in questDAO.allQuests.Values)
            {
                comboBoxQuestQuestId.Items.Add(q.id);
                comboBoxMarkerQuestId.Items.Add(q.id);
            }

            ValidateMyFields();
        }

        private void TierAnswerPanel_Load(object sender, EventArgs e)
        {

        }

        private void textBoxA1_TextChanged(object sender, EventArgs e)
        {
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        internal void UpdateUIByCurrentAnswerData()
        {
            Dialog.Tier.Answer source = this.answer;

            this.comboBoxAction.Text = source.actionType;
            this.textBoxActionTier.Text = source.actionTier;
            this.comboBoxActionTerrain.Text = source.actionTerrain;
            this.comboBoxActionMonster.Text = source.actionMonster;

            this.textBoxBranchNPCName.Text = source.branchNPCName;
            this.textBoxBranchTier.Text = source.branchTier;
            this.textBoxDestroyNPCName.Text = source.destroyNPCName;
            this.textBoxDestroyLocation.Text = source.destroyLocation;

            this.checkBoxQuestIsMainQuest.Checked = source.questIsMainQuest;
            this.comboBoxQuestQuestId.Text = source.questQuestID;
            this.comboBoxQuestStatus.Text = source.questStatus;
            this.textBoxQuestUpdate.Text = source.questUpdate;

            this.checkBoxMarkerIsMainQuest.Checked = source.markerIsMainQuest;
            this.comboBoxMarkerQuestId.Text = source.markerQuestId;
            this.comboBoxMarkerAction.Text = source.markerActionId;
            this.textBoxMarkerId.Text = source.markerId;

            this.comboBoxCheckType.Text = source.checkType;
            this.numericUpDownCheckValue.Text = source.checkValue;
            this.textBoxCheckSuccess.Text = source.checkSucess;
            this.textBoxCheckFail.Text = source.checkFail;

            this.comboBoxRewardType.Text = source.rewardType;
            this.textBoxRewardValue.Text = source.rewardValue;

            this.textBoxCutsceneId.Text = source.cutSceneId;

            giveItemPanel1.UpdateUIByAnswer(source);

            rewardItemPanel1.UpdateUIByCurrentReward(source.rewardItems[0]);
            rewardItemPanel2.UpdateUIByCurrentReward(source.rewardItems[1]);
            rewardItemPanel3.UpdateUIByCurrentReward(source.rewardItems[2]);
            rewardItemPanel4.UpdateUIByCurrentReward(source.rewardItems[3]);

            this.textBoxSpawnNameLabel.Text = source.spawnName;
            this.textBoxSpawnLocation.Text = source.spawnLocation;
        }

        internal void UpdateCurrentAnswerDataByUI()
        {
            UpdateAnswerDataByUI(this.answer);
        }

        private void UpdateAnswerDataByUI(Dialog.Tier.Answer a)
        {
            a.actionType = this.comboBoxAction.Text;
            a.actionTier = this.textBoxActionTier.Text;
            a.actionTerrain = this.comboBoxActionTerrain.Text;
            a.actionMonster = this.comboBoxActionMonster.Text;

            a.branchNPCName = this.textBoxBranchNPCName.Text;
            a.branchTier = this.textBoxBranchTier.Text;
            a.destroyNPCName = this.textBoxDestroyNPCName.Text;
            a.destroyLocation = this.textBoxDestroyLocation.Text;

            a.questIsMainQuest = this.checkBoxQuestIsMainQuest.Checked;
            a.questQuestID = this.comboBoxQuestQuestId.Text;
            a.questStatus = this.comboBoxQuestStatus.Text;
            a.questUpdate = this.textBoxQuestUpdate.Text;

            a.markerIsMainQuest = this.checkBoxMarkerIsMainQuest.Checked;
            a.markerQuestId = this.comboBoxMarkerQuestId.Text;
            a.markerActionId = this.comboBoxMarkerAction.Text;
            a.markerId = this.textBoxMarkerId.Text;

            a.checkType = this.comboBoxCheckType.Text;
            a.checkValue = this.numericUpDownCheckValue.Text;
            a.checkSucess = this.textBoxCheckSuccess.Text;
            a.checkFail = this.textBoxCheckFail.Text;

            a.rewardType = this.comboBoxRewardType.Text;
            a.rewardValue = this.textBoxRewardValue.Text;
 
            a.cutSceneId = this.textBoxCutsceneId.Text;

            giveItemPanel1.UpdateAnswerByUI(a);
 
            ClearUnusedFields(a);

            rewardItemPanel1.UpdateRewardByUI(a.rewardItems[0]);
            rewardItemPanel2.UpdateRewardByUI(a.rewardItems[1]);
            rewardItemPanel3.UpdateRewardByUI(a.rewardItems[2]);
            rewardItemPanel4.UpdateRewardByUI(a.rewardItems[3]);

            a.spawnName = this.textBoxSpawnNameLabel.Text;
            a.spawnLocation = this.textBoxSpawnLocation.Text;
        }

        void ClearUnusedFields(Dialog.Tier.Answer a)
        {
            if (!textBoxActionTier.Enabled)
            {
                a.actionTier = "";
            }

            if (comboBoxAction.Text != "combat")
            {
                a.actionTerrain = "";
                a.actionMonster = "";
            }

            if (!textBoxBranchTier.Enabled)
            {
                a.branchTier = "";
            }

            if (comboBoxQuestQuestId.Text == "")
            {
                a.questUpdate = "";
                a.questStatus = "";
                a.questIsMainQuest = false;
            }

            if (comboBoxMarkerQuestId.Text == "")
            {
                a.markerId = "";
                a.markerActionId = "";
                a.markerIsMainQuest = false;
            }

            if (comboBoxCheckType.Text == "")
            {
                a.checkValue = "";
                a.checkSucess = "";
                a.checkFail = "";
            }

            if (comboBoxRewardType.Text == "")
            {
                a.rewardValue = "";
            }
        }

        static Dialog.Tier.Answer copyAnswer;

        private void buttonCopyAnswer_Click(object sender, EventArgs e)
        {
            if (copyAnswer == null)
            {
                copyAnswer = new Dialog.Tier.Answer(null);
            }
            UpdateAnswerDataByUI(copyAnswer);
        }

        private void buttonPasteAnswer_Click(object sender, EventArgs e)
        {
            if (copyAnswer != null)
            {
                this.answer.CopyAttributesFrom(copyAnswer);
            }
            UpdateUIByCurrentAnswerData();
        }

        private void timerCheckPasteAnswerAttributes_Tick(object sender, EventArgs e)
        {
            if (copyAnswer != null)
            {
                this.buttonPasteAnswer.Enabled = true;
                timerCheckPasteAnswerAttributes.Stop();
            }
        }

        private void ValidateMyFields ()
        {
            textBoxActionTier.Enabled = comboBoxAction.Text == "next";

            comboBoxActionTerrain.Enabled = comboBoxActionMonster.Enabled = comboBoxAction.Text == "combat";

            textBoxBranchTier.Enabled = textBoxBranchNPCName.Text != "";

            textBoxQuestUpdate.Enabled =
                comboBoxQuestStatus.Enabled =
                checkBoxQuestIsMainQuest.Enabled =
                comboBoxQuestQuestId.Text != "";

            textBoxMarkerId.Enabled =
                comboBoxMarkerAction.Enabled =
                checkBoxMarkerIsMainQuest.Enabled =
                comboBoxMarkerQuestId.Text != "";

            numericUpDownCheckValue.Enabled =
                textBoxCheckSuccess.Enabled =
                textBoxCheckFail.Enabled =
                comboBoxCheckType.Text != "";

            textBoxRewardValue.Enabled =
                comboBoxRewardType.Text != "";

            ValidateReward();

            CheckCorrectFields();
        }

        internal void OrderValidateFields()
        {
            if (!this.Visible)
            {
                return;
            }
            ValidateMyFields();
            if (owner != null)
                owner.OrderValidateFields();
        }

        void ValidateReward ()
        {
            if (comboBoxRewardType.Text == "item")
            {
                textBoxRewardValue.Enabled = false;
                rewardItemPanel1.Enabled = true;
            } else
            {
                textBoxRewardValue.Enabled = true;
                rewardItemPanel1.Enabled = false;
            }

            if (rewardItemPanel1.IsCorrectAndEnabled)
            {
                rewardItemPanel2.Enabled = true;
            } else
            {
                rewardItemPanel2.Enabled = false;
            }

            rewardItemPanel3.Enabled = rewardItemPanel2.IsCorrectAndEnabled;
            rewardItemPanel4.Enabled = rewardItemPanel3.IsCorrectAndEnabled;
        }


        private bool CheckCorrectQuest ()
        {
            bool res = true;

            Quest q1 = null;
            QuestDAO.Instance.allQuests.TryGetValue(comboBoxQuestQuestId.Text, out q1);
            if (q1 != null || string.IsNullOrEmpty(comboBoxQuestQuestId.Text))
            {
                if (q1 != null)
                {
                    checkBoxQuestIsMainQuest.Checked = q1.type == "main";
                }

                comboBoxQuestQuestId.ForeColor = Color.Black;
            }
            else
            {
                comboBoxQuestQuestId.ForeColor = Color.Red;
                res = false;
            }
            checkBoxQuestIsMainQuest.Enabled = false;

            Quest q2 = null;
            QuestDAO.Instance.allQuests.TryGetValue(comboBoxMarkerQuestId.Text, out q2);
            if (q2 != null || string.IsNullOrEmpty(comboBoxMarkerQuestId.Text))
            {
                if (q2 != null)
                {
                    checkBoxMarkerIsMainQuest.Checked = q2.type == "main";
                }

                comboBoxMarkerQuestId.ForeColor = Color.Black;
            }
            else
            {
                comboBoxMarkerQuestId.ForeColor = Color.Red;
                res = false;
            }
            checkBoxMarkerIsMainQuest.Enabled = false;

            return res;
        }

        public bool CheckCorrectFields()
        {
            bool correctQuest = CheckCorrectQuest();

            bool correctGiveItem1 = giveItemPanel1.CheckCorrectItem();

            bool correctReward = true;

            string t = comboBoxRewardType.Text;
            if (t ==
                "item"
                )
            {
                bool correctReward1 = rewardItemPanel1.CheckCorrectData();
                bool correctReward2 = rewardItemPanel2.CheckCorrectData();
                bool correctReward3 = rewardItemPanel3.CheckCorrectData();
                bool correctReward4 = rewardItemPanel4.CheckCorrectData();
                correctReward = 
                    correctReward1 && correctReward2
                    && correctReward3 && correctReward4;
            }
 
            return correctReward && correctQuest && correctGiveItem1;
        }

        private void comboBoxAction_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxQuestQuestId_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxCheckType_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxRewardType_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxMarkerQuestId_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void textBoxBranchNPCName_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxAction_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxQuestQuestId_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void comboBoxQuestQuestId_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxCheckType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxRewardValue_TextChanged(object sender, EventArgs e)
        {
            OrderValidateFields();
        }

        private void comboBoxGiveItemType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGiveItemType_TextChanged(object sender, EventArgs e)
        {
            this.OrderValidateFields();
        }

        private void textBoxGiveItemId_TextChanged(object sender, EventArgs e)
        {
            this.OrderValidateFields();
        }

        private void textBoxCheckValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void rewardItemPanel1_Load(object sender, EventArgs e)
        {

        }

        private void textBoxCutsceneId_TextChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDownRewardValue_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
