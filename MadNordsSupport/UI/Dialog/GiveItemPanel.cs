﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.Logic.Entities;
using MadNordsSupport.Logic;

namespace MadNordsSupport.UI
{
    public partial class GiveItemPanel : UserControl
    {
        public GiveItemPanel()
        {
            InitializeComponent();
        }

        AnswerPanel owner;

        internal void Init(AnswerPanel owner)
        {
            this.owner = owner;
        }

        private void GiveItemPanel_Load(object sender, EventArgs e)
        {

        }

        internal void UpdateUIByAnswer(Dialog.Tier.Answer a)
        {
            if (string.IsNullOrEmpty(a.giveItemType) || string.IsNullOrEmpty (a.giveItemId))
            {
                return;
            }
            this.comboBoxGiveItemType.Text = a.giveItemType;
            ItemType desiredItemType = ItemTypeDAO.Instance.allData[a.giveItemType];
            string txt = desiredItemType.GetItemNameByNumericId(a.giveItemId);
            this.comboBoxGiveItemId.Text = txt;            
        }

        internal void UpdateAnswerByUI(Dialog.Tier.Answer a)
        {
            if (IsCorrect)
            {
                a.giveItemType = this.comboBoxGiveItemType.Text;
                try
                {
                    a.giveItemId = Item.GetIdFromItemName(comboBoxGiveItemId.Text);
                } catch (Exception e)
                {
                    Console.Error.WriteLine(e.ToString ());
                }
                
            }
            else
            {
                a.giveItemType = "";
                a.giveItemId = "";
            }
        }

        public bool IsCorrect
        {
            get
            {
                bool typeGood = !string.IsNullOrEmpty(this.comboBoxGiveItemType.Text);
                bool idGood = !string.IsNullOrEmpty(this.comboBoxGiveItemId.Text);
                return typeGood && idGood;
            }
        }

        private void comboBoxGiveItemType_TextChanged(object sender, EventArgs e)
        {
            owner.OrderValidateFields();

            string type = comboBoxGiveItemType.Text;
            List<string> itemsIds = ItemTypeDAO.Instance.allData[type].itemsNumTitlesNames;
            if (itemsIds == null)
            {
                return;
            }

            comboBoxGiveItemId.Items.Clear();
            comboBoxGiveItemId.Items.AddRange(itemsIds.ToArray ());
            if (!comboBoxGiveItemId.Items.Contains(comboBoxGiveItemId.Text))
            {
                comboBoxGiveItemId.Text = comboBoxGiveItemId.Items[0].ToString ();
            }
        }

        private void comboBoxGiveItemId_TextChanged(object sender, EventArgs e)
        {
            owner.OrderValidateFields();
        }

        internal bool CheckCorrectItem()
        {
            bool res = true;

            comboBoxGiveItemType.ForeColor = Color.Black;
            comboBoxGiveItemId.ForeColor = Color.Black;
  
            if (comboBoxGiveItemType.Text == "" && comboBoxGiveItemId.Text == "")
            {
                res = true;
            }
            else
            {
                if (!comboBoxGiveItemType.Items.Contains(comboBoxGiveItemType.Text))
                {
                    res = false;
                }

                if (!comboBoxGiveItemId.Items.Contains(comboBoxGiveItemId.Text))
                {
                    res = false;
                }
            }

            if (!res)
            {
                comboBoxGiveItemType.ForeColor = Color.Red;
                comboBoxGiveItemId.ForeColor = Color.Red;
            }

            return res;
        }

        private void groupBox9_Enter(object sender, EventArgs e)
        {

        }

        private void comboBoxGiveItemType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
