﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MadNordsSupport.Logic;
using MadNordsSupport.Logic.Entities;
using System.IO;

namespace MadNordsSupport
{
    public partial class DialogPanel : UserControl
    {
        DialogDAO dialogsDAO
        {
            get
            {
                return DialogDAO.Instance;
            }
        }

        internal readonly Dialog currentDialog = new Dialog ();

        public DialogPanel()
        {
            InitializeComponent();

            TryFileOnStart();
        }

        void TryFileOnStart()
        {
            string file = MadNordsSettings.Default.DialogFiIeName;
            TryReadFile(file);
        }

        bool TryReadFile(string file)
        {
            try
            {
                if (this.dialogsDAO.TryReadFrom(file))
                {
                    this.dialogFileName.Text = file;
                    this.dialogsCount.Text = "Dialogs count: " + this.dialogsDAO.allDialogs.Count + "";
                    buttonAddDialog.Enabled = true;
                    RenewAllDialogFields();

                    UpdateQuestTextBoxAutoComplete();
                    return true;
                }
            }
            catch (IOException)
            {
            }
            return false;
        }

        string[] _allDialogKeys = null;

        private void UpdateQuestTextBoxAutoComplete()
        {
            AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();

            _allDialogKeys = this.dialogsDAO.allDialogs.Keys.ToArray();
            Array.Sort(_allDialogKeys, (x, y) => String.Compare(x, y));

            allowedTypes.AddRange(_allDialogKeys);
            textBoxDialogId.AutoCompleteCustomSource = allowedTypes;
            textBoxDialogId.AutoCompleteMode = AutoCompleteMode.Suggest;
            textBoxDialogId.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        bool isRenewingId;
        bool isRenewingFields;

        private void RenewAllDialogFields()
        {
            if (isRenewingFields) { return; }
            else { isRenewingFields = true; }

            Dialog d = null;
            if (this.dialogsDAO.allDialogs.ContainsKey(textBoxDialogId.Text))
            {
                buttonAddDialog.Enabled = false;
                SetUpdateButtonEnabled(true);
                buttonUpdateDialog.Enabled = true;

                d = this.dialogsDAO.allDialogs[textBoxDialogId.Text];
            }
            else
            {
                buttonAddDialog.Enabled = true;
                buttonUpdateDialog.Enabled = false;
                SetUpdateButtonEnabled(false);

                d = new Dialog();
                int questPlus = this.dialogsDAO.allDialogs.Count + 1;
                d.id = "Dialog" + (questPlus) + "";
                if (dialogsDAO.allDialogs.ContainsKey(d.id))
                {
                    questPlus = questPlus + 1;
                    d.id = "Dialog" + (questPlus) + "";
                }
                d.AddNewTier("0", 0);
            }

            if (!isRenewingId)
            {
                textBoxDialogId.Text = d.id;
            }

            currentDialog.CopyFrom (d);

            RefreshDataFields();
            HideAllTierForms();

            isRenewingFields = false;    
        }

        public void RefreshDataFields()
        {
            isProgramaticalRowAdd = true;

            this.textBoxDialogName.Text = currentDialog.name;

            DataGridViewRow currentRow = this.dataGridViewTier.CurrentRow;
            int rowIndex = -1;
            if (currentRow != null)
            {
                rowIndex = currentRow.Index;
            }

            dataGridViewTier.Rows.Clear();
            foreach (Dialog.Tier t in currentDialog.tiers)
            {
                dataGridViewTier.Rows.Add(t.id, t.comment, "Edit", "Copy", "Paste", t.TierBugsMessage);
            }

            ForceRowSelection(rowIndex);

            isProgramaticalRowAdd = false;
            labelTiers.Text = "Tiers. Count = " + currentDialog.tiers.Count;

            CheckValidDialog();
        }

        private void ForceRowSelection (int rowIndex)
        {
            if (rowIndex > 0 && this.dataGridViewTier.RowCount > rowIndex)
            {
                foreach (DataGridViewRow r in this.dataGridViewTier.Rows)
                {
                    if (r.Index != rowIndex)
                    {
                        continue;
                    }
                    foreach (DataGridViewCell c in r.Cells)
                    {
                        if (c.RowIndex == rowIndex)
                        {
                            this.dataGridViewTier.CurrentCell = c;
                            return;
                        }
                    }
                }
            }
        }

        private void questFileName_TextChanged(object sender, EventArgs e)
        {
        }

        private void DialogPanel_Load(object sender, EventArgs e)
        {
        }

        private void dialogFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDialogFile())
            {
                RenewAllDialogFields();
            }
        }

        private bool SelectDialogFile()
        {
            bool res = false;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                if (TryReadFile(file))
                {
                    res = true;
                }
            }
            return res;
        }

        private void textBoxDialogId_TextChanged(object sender, EventArgs e)
        {
            isRenewingId = true;
            if (currentDialog != null && currentDialog.id != null)
            {
                bool isNew = !this.dialogsDAO.allDialogs.ContainsKey(currentDialog.id);
                if (!isNew)
                {
                    AddUpdateCurrentDialog(false, false);
                }
            }

            currentDialog.id = textBoxDialogId.Text;
            RenewAllDialogFields();
            
            isRenewingId = false;
        }

        void AddUpdateCurrentDialog(object sender, EventArgs e)
        {
            AddUpdateCurrentDialog(false);
        }

        public void AddUpdateCurrentDialog (bool createNewDialog, bool renewAllDialogFields = false)
        {
            Dialog d = currentDialog;

            bool isNew = !this.dialogsDAO.allDialogs.ContainsKey(d.id);

            if (this.dialogsDAO.AddUpdateDialog(d))
            {
                if (isNew)
                {
                    UpdateQuestTextBoxAutoComplete();
                }
                if (createNewDialog)
                {
                    this.textBoxDialogId.Text = "new Dialog";
                    this.textBoxDialogName.Text = "New Dialog";
                }
                if (renewAllDialogFields)
                {
                    this.RenewAllDialogFields();
                }
            }

            this.dialogsCount.Text = "Dialogs count: " + this.dialogsDAO.allDialogs.Count + "";
        }

        private void tiersDataGridView_ControlAdded(object sender, ControlEventArgs e)
        {
        }

        bool isProgramaticalRowAdd = false;

        private void dataGridViewTier_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            foreach (DataGridViewColumn col in dataGridViewTier.Columns)
            {
                if (col.Name == "Id")
                {
                    col.ReadOnly = true;
                }
            }

            foreach (DataGridViewRow row in dataGridViewTier.Rows)
            {
                foreach (DataGridViewCell c in row.Cells)
                {                    
                    if (c is DataGridViewDisableButtonCell)
                    {
                        DataGridViewDisableButtonCell b = (DataGridViewDisableButtonCell)c;
                        string s = "";
                        b.Enabled = true;

                        bool isLastRow = row.Index == dataGridViewTier.Rows.Count - 1;

                        switch (c.ColumnIndex) {
                            case (2): {                               
                                s = "Edit";
                                break;
                            }
                            case (3):  {
                                if (isLastRow)
                                {
                                    b.Enabled = false;
                                }

                                s = "Copy";
                                break;
                            }
                            case (4):  {
                                if (copyTier == null)
                                {
                                    s = "Frozen";
                                    b.Enabled = false;
                                }
                                else
                                {
                                    s = "Paste";
                                }
                                break;                             
                            }
                            case (5):
                                {
                                }
                                break;
                        }

                        b.Value = s;
                    }
                }
            }          

            if (!isProgramaticalRowAdd)
            {
                AddNewTier(e.RowIndex);
            }
        }

        internal Dialog.Tier GetNextTier(Dialog.Tier currentTier)
        {
            Dialog.Tier prevTier = null;

            for (int i = 0; i < currentDialog.tiers.Count; i++)
            {
                Dialog.Tier t = currentDialog.tiers[i];

                if (prevTier == currentTier)
                {
                    return t;
                }

                prevTier = t;
            }

            return null;
        }

        internal Dialog.Tier GetPrevTier (Dialog.Tier currentTier)
        {
            Dialog.Tier prevTier = null;

            for (int i = 0; i < currentDialog.tiers.Count; i++)
            {
                Dialog.Tier t = currentDialog.tiers[i];

                if (t == currentTier)
                {
                    return prevTier;
                }

                prevTier = t;
            }

            return null;
        }

        Dialog.Tier AddNewTier(int index)
        {
            string id = "";
            Dialog.Tier t = currentDialog.AddNewTier(id, index);

            dataGridViewTier.Rows[dataGridViewTier.RowCount - 2].Cells[0].Value = id;
            labelTiers.Text = "Tiers. Count = " + currentDialog.tiers.Count;

            return t;
        }

        Dialog copyDialog = new Dialog();

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            buttonPaste.Enabled = true;
            copyDialog.CopyFrom(currentDialog);
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            currentDialog.CopyFrom (copyDialog);
            RefreshDataFields();
        }

        private void labelTier_Click(object sender, EventArgs e)
        {
        }

        Dialog.Tier copyTier = null;

        internal TierEditForm DisplayTierEdit (Dialog.Tier targetTier)
        {
            if (currentDialog.tiers.Contains (targetTier))
            {
                return DisplayTierEdit(targetTier, currentDialog);
            } else
            {
                Console.WriteLine("Error! Tier not exist in current dialog!");
            }
            return null;
        }

        private void dataGridViewTier_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewDisableButtonColumn &&
                e.RowIndex >= 0)
            {
                int rowIndex = e.RowIndex;
                string id = (string)senderGrid.Rows[rowIndex].Cells[0].Value;
                Dialog.Tier t = currentDialog.GetTier(id);
                
                if (e.ColumnIndex == 2)
                {
                    if (t == null)
                    {
                        t = this.AddNewTier(e.RowIndex);
                    }
                    DisplayTierEdit(t, currentDialog);
                }

                if (t != null)
                {                  
                    if (e.ColumnIndex == 3)
                    {
                        CopyTier(t);
                    }
                }

                if (e.ColumnIndex == 4)
                {
                    PasteTier(t);
                }

                RefreshDataFields();
            }
        }

        void CopyTier(Dialog.Tier t)
        {
            if (copyTier == null)
            {
                copyTier = Dialog.Tier.CreateEmpty("template", null);
            }
            this.copyTier.CopyFrom (t);
        }

        void PasteTier(Dialog.Tier target)
        {
            if (copyTier == null)
            {
                return;
            }

            if (target == null)
            {
                target = AddNewTier(dataGridViewTier.Rows.Count);
            }
                
            target.CopyFrom(copyTier);
        }

        Dictionary<string, TierEditForm> tierEditForms = new Dictionary<string, TierEditForm>();

        TierEditForm DisplayTierEdit(Dialog.Tier t, Dialog owner)
        {
            TierEditForm resultForm = null;
            TierEditForm editForm;

            if (tierEditForms.ContainsKey(t.id))
            {
                editForm = tierEditForms[t.id];
                if (editForm.IsDisposed || editForm.Disposing)
                {
                }
                else
                {
                    editForm.Show();
                    resultForm = editForm;
                }                
            }

            if (resultForm == null)
            {
                editForm = new TierEditForm();
                editForm.Init(t, this);
                editForm.Show();
                if (tierEditForms.ContainsKey(t.id))
                {
                    tierEditForms[t.id] = editForm;
                }
                else
                {
                    tierEditForms.Add(t.id, editForm);
                }

                resultForm = editForm;
            }
            return resultForm;
        }

        void HideAllTierForms()
        {
            foreach (TierEditForm eForm in tierEditForms.Values)
            {
                eForm.Hide();
                eForm.Dispose();
            }

            tierEditForms.Clear();
        }

        private void dataGridViewTier_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.dataGridViewTier.Rows[this.tierRowIndex].IsNewRow)
            {
                string id = (string)this.dataGridViewTier.Rows[this.tierRowIndex].Cells[0].Value;
                this.currentDialog.RemoveTier(id);
            }
            this.RefreshDataFields();
        }

        public int tierRowIndex = -1;

        private void dataGridViewTier_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.dataGridViewTier.Rows[e.RowIndex].Selected = true;
                this.tierRowIndex = e.RowIndex;
                this.dataGridViewTier.CurrentCell = this.dataGridViewTier.Rows[e.RowIndex].Cells[1];
                this.contextMenuTier.Show(this.dataGridViewTier, e.Location);
                contextMenuTier.Show(Cursor.Position);
            }
        }

        private void dataGridViewTier_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (e.ColumnIndex == 1 &&
                e.RowIndex >= 0)
            {
                int rowIndex = e.RowIndex;
                string id = (string)senderGrid.Rows[rowIndex].Cells[0].Value;
                Dialog.Tier t = currentDialog.GetTier(id);

                if (t != null)
                {
                    t.comment = (string)senderGrid.Rows[rowIndex].Cells[1].Value;
                }
            }
        }

        void CheckValidDialog()
        {
            bool isValid = !this.currentDialog.CheckErrors;
            if (!isValid)
            {
                buttonAddDialog.Enabled = false;
                SetUpdateButtonEnabled(false);
            }
            else
            {
                if (this.dialogsDAO.allDialogs.ContainsKey(this.currentDialog.id))
                {
                    buttonAddDialog.Enabled = false;
                    SetUpdateButtonEnabled(true);
                }
                else
                {
                    buttonAddDialog.Enabled = true;
                    SetUpdateButtonEnabled(false);
                }
            }

            if (this.dialogsDAO.allDialogs.ContainsKey(this.currentDialog.id))
            {
                buttonDeleteDialog.Enabled = true;
            }
            else
            {
                buttonDeleteDialog.Enabled = false;
            }
        }

        private void buttonDeleteDialog_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                         "Confirm Delete!!",
                         MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                DialogDAO.Instance.DeleteDialog(this.textBoxDialogId.Text);

                if (true)
                {
                    UpdateQuestTextBoxAutoComplete();
                }
                this.textBoxDialogId.Text = "new Dialog";
                this.RenewAllDialogFields();
            }
            else
            {
                // If 'No', do something here.
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!this.dataGridViewTier.Rows[this.tierRowIndex].IsNewRow)
            {
                this.dataGridViewTier.Rows.Insert(this.tierRowIndex + 1);
            }
            this.RefreshDataFields();
        }

        private void toolStripMenuItemAddRowUp_Click(object sender, EventArgs e)
        {
            if (!this.dataGridViewTier.Rows[this.tierRowIndex].IsNewRow)
            {
                this.dataGridViewTier.Rows.Insert(this.tierRowIndex);
            }
            this.RefreshDataFields();
        }

        private void textBoxDialogName_TextChanged(object sender, EventArgs e)
        {
            isRenewingId = true;

            currentDialog.name = textBoxDialogName.Text;
 
            isRenewingId = false;
        }

        private void buttonSortXML_Click(object sender, EventArgs e)
        {
            this.dialogsDAO.SortXML();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentDialog(false);
            Dialog d = GetPrevDialog();
            if (d != null)
            {
                textBoxDialogId.Text = d.id;
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentDialog(false);
            Dialog d = GetNextDialog();
            if (d != null)
            {
                textBoxDialogId.Text = d.id;
            }
        }

        private Dialog GetPrevDialog()
        {
            if (_allDialogKeys == null)
            {
                return null;
            }

            string prevDialog = null;

            for (int i = 0; i < _allDialogKeys.Length; i++)
            {
                string d = _allDialogKeys[i];

                if (d == currentDialog.id)
                {
                    if (prevDialog == null)
                    {
                        return this.dialogsDAO.allDialogs[_allDialogKeys[_allDialogKeys.Length - 1]];
                    }
                    return this.dialogsDAO.allDialogs[prevDialog];
                }

                prevDialog = d;
            }

            return null;
        }

        private Dialog GetNextDialog ()
        {
            if (_allDialogKeys == null)
            {
                return null;
            }

            string prevDialog = null;

            for (int i = 0; i < _allDialogKeys.Length; i++)
            {
                string d = _allDialogKeys[i];

                if (prevDialog == currentDialog.id)
                {
                    return this.dialogsDAO.allDialogs[d];
                }

                prevDialog = d;
            }

            if (prevDialog == _allDialogKeys[_allDialogKeys.Length - 1])
            {
                return this.dialogsDAO.allDialogs[_allDialogKeys[0]];
            }

            return null;
        }

        private void SetUpdateButtonEnabled (bool enabled)
        {
            this.buttonUpdateDialog.Enabled = enabled;

            this.buttonPrev.Enabled = enabled;
            this.buttonNext.Enabled = enabled;
        }
    }
}
