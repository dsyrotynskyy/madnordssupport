﻿namespace MadNordsSupport
{
    partial class AnswerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxCutsceneId = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBoxRewardValue = new System.Windows.Forms.TextBox();
            this.rewardItemPanel4 = new MadNordsSupport.UI.RewardItemPanel();
            this.rewardItemPanel3 = new MadNordsSupport.UI.RewardItemPanel();
            this.rewardItemPanel2 = new MadNordsSupport.UI.RewardItemPanel();
            this.rewardItemPanel1 = new MadNordsSupport.UI.RewardItemPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBoxRewardType = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericUpDownCheckValue = new System.Windows.Forms.NumericUpDown();
            this.textBoxCheckFail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxCheckSuccess = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxCheckType = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxMarkerId = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxMarkerAction = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxMarkerQuestId = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBoxMarkerIsMainQuest = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxQuestUpdate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxQuestStatus = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxQuestQuestId = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxQuestIsMainQuest = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxDestroyLocation = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxDestroyNPCName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxBranchNPCName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxBranchTier = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxActionMonster = new System.Windows.Forms.ComboBox();
            this.comboBoxActionTerrain = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxActionTier = new System.Windows.Forms.TextBox();
            this.comboBoxAction = new System.Windows.Forms.ComboBox();
            this.buttonCopyAnswer = new System.Windows.Forms.Button();
            this.buttonPasteAnswer = new System.Windows.Forms.Button();
            this.timerCheckPasteAnswerAttributes = new System.Windows.Forms.Timer(this.components);
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBoxSpawnLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSpawnNameLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.giveItemPanel1 = new MadNordsSupport.UI.GiveItemPanel();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCheckValue)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this.textBoxCutsceneId);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox8.Location = new System.Drawing.Point(12, 404);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(143, 45);
            this.groupBox8.TabIndex = 39;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Cutscene";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(10, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Id";
            // 
            // textBoxCutsceneId
            // 
            this.textBoxCutsceneId.Location = new System.Drawing.Point(42, 15);
            this.textBoxCutsceneId.Name = "textBoxCutsceneId";
            this.textBoxCutsceneId.Size = new System.Drawing.Size(96, 20);
            this.textBoxCutsceneId.TabIndex = 20;
            this.textBoxCutsceneId.TextChanged += new System.EventHandler(this.textBoxCutsceneId_TextChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBoxRewardValue);
            this.groupBox7.Controls.Add(this.rewardItemPanel4);
            this.groupBox7.Controls.Add(this.rewardItemPanel3);
            this.groupBox7.Controls.Add(this.rewardItemPanel2);
            this.groupBox7.Controls.Add(this.rewardItemPanel1);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.comboBoxRewardType);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(12, 455);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(599, 293);
            this.groupBox7.TabIndex = 38;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Reward";
            // 
            // textBoxRewardValue
            // 
            this.textBoxRewardValue.Location = new System.Drawing.Point(190, 16);
            this.textBoxRewardValue.Name = "textBoxRewardValue";
            this.textBoxRewardValue.Size = new System.Drawing.Size(179, 20);
            this.textBoxRewardValue.TabIndex = 46;
            // 
            // rewardItemPanel4
            // 
            this.rewardItemPanel4.Location = new System.Drawing.Point(9, 223);
            this.rewardItemPanel4.Name = "rewardItemPanel4";
            this.rewardItemPanel4.Size = new System.Drawing.Size(573, 54);
            this.rewardItemPanel4.TabIndex = 45;
            // 
            // rewardItemPanel3
            // 
            this.rewardItemPanel3.Location = new System.Drawing.Point(9, 163);
            this.rewardItemPanel3.Name = "rewardItemPanel3";
            this.rewardItemPanel3.Size = new System.Drawing.Size(573, 54);
            this.rewardItemPanel3.TabIndex = 44;
            // 
            // rewardItemPanel2
            // 
            this.rewardItemPanel2.Location = new System.Drawing.Point(7, 103);
            this.rewardItemPanel2.Name = "rewardItemPanel2";
            this.rewardItemPanel2.Size = new System.Drawing.Size(573, 54);
            this.rewardItemPanel2.TabIndex = 43;
            // 
            // rewardItemPanel1
            // 
            this.rewardItemPanel1.Location = new System.Drawing.Point(6, 43);
            this.rewardItemPanel1.Name = "rewardItemPanel1";
            this.rewardItemPanel1.Size = new System.Drawing.Size(573, 54);
            this.rewardItemPanel1.TabIndex = 42;
            this.rewardItemPanel1.Load += new System.EventHandler(this.rewardItemPanel1_Load);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(149, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "Value";
            // 
            // comboBoxRewardType
            // 
            this.comboBoxRewardType.FormattingEnabled = true;
            this.comboBoxRewardType.Items.AddRange(new object[] {
            "money",
            "xp",
            "item",
            "stat_strength",
            "stat_agility",
            "stat_intellect",
            "stat_vitality",
            "stat_wisdom",
            "stat_HP",
            "stat_MP",
            "key"});
            this.comboBoxRewardType.Location = new System.Drawing.Point(43, 16);
            this.comboBoxRewardType.Name = "comboBoxRewardType";
            this.comboBoxRewardType.Size = new System.Drawing.Size(100, 21);
            this.comboBoxRewardType.TabIndex = 32;
            this.comboBoxRewardType.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            this.comboBoxRewardType.TextChanged += new System.EventHandler(this.comboBoxRewardType_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(6, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Type";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericUpDownCheckValue);
            this.groupBox6.Controls.Add(this.textBoxCheckFail);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.textBoxCheckSuccess);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.comboBoxCheckType);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox6.Location = new System.Drawing.Point(12, 345);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(526, 53);
            this.groupBox6.TabIndex = 37;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Check";
            // 
            // numericUpDownCheckValue
            // 
            this.numericUpDownCheckValue.Location = new System.Drawing.Point(187, 17);
            this.numericUpDownCheckValue.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownCheckValue.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownCheckValue.Name = "numericUpDownCheckValue";
            this.numericUpDownCheckValue.Size = new System.Drawing.Size(60, 20);
            this.numericUpDownCheckValue.TabIndex = 40;
            // 
            // textBoxCheckFail
            // 
            this.textBoxCheckFail.Location = new System.Drawing.Point(432, 17);
            this.textBoxCheckFail.Name = "textBoxCheckFail";
            this.textBoxCheckFail.Size = new System.Drawing.Size(77, 20);
            this.textBoxCheckFail.TabIndex = 39;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(389, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Fail";
            // 
            // textBoxCheckSuccess
            // 
            this.textBoxCheckSuccess.Location = new System.Drawing.Point(304, 16);
            this.textBoxCheckSuccess.Name = "textBoxCheckSuccess";
            this.textBoxCheckSuccess.Size = new System.Drawing.Size(75, 20);
            this.textBoxCheckSuccess.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(251, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Sucess";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(149, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Value";
            // 
            // comboBoxCheckType
            // 
            this.comboBoxCheckType.FormattingEnabled = true;
            this.comboBoxCheckType.Items.AddRange(new object[] {
            "money",
            "diplomacy_sum",
            "lockpick_sum",
            "strength_sum",
            "agility_sum",
            "intellect_sum",
            "HP",
            "MP"});
            this.comboBoxCheckType.Location = new System.Drawing.Point(43, 16);
            this.comboBoxCheckType.Name = "comboBoxCheckType";
            this.comboBoxCheckType.Size = new System.Drawing.Size(100, 21);
            this.comboBoxCheckType.TabIndex = 32;
            this.comboBoxCheckType.SelectedIndexChanged += new System.EventHandler(this.comboBoxCheckType_SelectedIndexChanged);
            this.comboBoxCheckType.TextChanged += new System.EventHandler(this.comboBoxCheckType_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(6, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Type";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxMarkerId);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.comboBoxMarkerAction);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.comboBoxMarkerQuestId);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.checkBoxMarkerIsMainQuest);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.Location = new System.Drawing.Point(12, 287);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(526, 53);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Marker";
            // 
            // textBoxMarkerId
            // 
            this.textBoxMarkerId.Location = new System.Drawing.Point(433, 16);
            this.textBoxMarkerId.Name = "textBoxMarkerId";
            this.textBoxMarkerId.Size = new System.Drawing.Size(76, 20);
            this.textBoxMarkerId.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(389, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Id";
            // 
            // comboBoxMarkerAction
            // 
            this.comboBoxMarkerAction.FormattingEnabled = true;
            this.comboBoxMarkerAction.Items.AddRange(new object[] {
            "create",
            "next",
            "multi"});
            this.comboBoxMarkerAction.Location = new System.Drawing.Point(304, 15);
            this.comboBoxMarkerAction.Name = "comboBoxMarkerAction";
            this.comboBoxMarkerAction.Size = new System.Drawing.Size(75, 21);
            this.comboBoxMarkerAction.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(251, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Action";
            // 
            // comboBoxMarkerQuestId
            // 
            this.comboBoxMarkerQuestId.FormattingEnabled = true;
            this.comboBoxMarkerQuestId.Items.AddRange(new object[] {
            "next",
            "end",
            "merchant",
            "combat"});
            this.comboBoxMarkerQuestId.Location = new System.Drawing.Point(152, 15);
            this.comboBoxMarkerQuestId.Name = "comboBoxMarkerQuestId";
            this.comboBoxMarkerQuestId.Size = new System.Drawing.Size(95, 21);
            this.comboBoxMarkerQuestId.TabIndex = 32;
            this.comboBoxMarkerQuestId.TextChanged += new System.EventHandler(this.comboBoxMarkerQuestId_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(88, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Quest ID";
            // 
            // checkBoxMarkerIsMainQuest
            // 
            this.checkBoxMarkerIsMainQuest.AutoSize = true;
            this.checkBoxMarkerIsMainQuest.Enabled = false;
            this.checkBoxMarkerIsMainQuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxMarkerIsMainQuest.Location = new System.Drawing.Point(9, 19);
            this.checkBoxMarkerIsMainQuest.Name = "checkBoxMarkerIsMainQuest";
            this.checkBoxMarkerIsMainQuest.Size = new System.Drawing.Size(77, 17);
            this.checkBoxMarkerIsMainQuest.TabIndex = 28;
            this.checkBoxMarkerIsMainQuest.Text = "MainQuest";
            this.checkBoxMarkerIsMainQuest.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxQuestUpdate);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.comboBoxQuestStatus);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.comboBoxQuestQuestId);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.checkBoxQuestIsMainQuest);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(12, 234);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(526, 51);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Quest";
            // 
            // textBoxQuestUpdate
            // 
            this.textBoxQuestUpdate.Location = new System.Drawing.Point(432, 17);
            this.textBoxQuestUpdate.Name = "textBoxQuestUpdate";
            this.textBoxQuestUpdate.Size = new System.Drawing.Size(76, 20);
            this.textBoxQuestUpdate.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(385, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Update";
            // 
            // comboBoxQuestStatus
            // 
            this.comboBoxQuestStatus.FormattingEnabled = true;
            this.comboBoxQuestStatus.Items.AddRange(new object[] {
            "get",
            "finished",
            "failed"});
            this.comboBoxQuestStatus.Location = new System.Drawing.Point(304, 16);
            this.comboBoxQuestStatus.Name = "comboBoxQuestStatus";
            this.comboBoxQuestStatus.Size = new System.Drawing.Size(75, 21);
            this.comboBoxQuestStatus.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(251, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Status";
            // 
            // comboBoxQuestQuestId
            // 
            this.comboBoxQuestQuestId.FormattingEnabled = true;
            this.comboBoxQuestQuestId.Items.AddRange(new object[] {
            "next",
            "end",
            "merchant",
            "combat"});
            this.comboBoxQuestQuestId.Location = new System.Drawing.Point(152, 16);
            this.comboBoxQuestQuestId.Name = "comboBoxQuestQuestId";
            this.comboBoxQuestQuestId.Size = new System.Drawing.Size(95, 21);
            this.comboBoxQuestQuestId.TabIndex = 27;
            this.comboBoxQuestQuestId.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            this.comboBoxQuestQuestId.SelectionChangeCommitted += new System.EventHandler(this.comboBoxQuestQuestId_SelectionChangeCommitted);
            this.comboBoxQuestQuestId.SelectedValueChanged += new System.EventHandler(this.comboBoxQuestQuestId_SelectedValueChanged);
            this.comboBoxQuestQuestId.TextChanged += new System.EventHandler(this.comboBoxQuestQuestId_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(89, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Quest ID";
            // 
            // checkBoxQuestIsMainQuest
            // 
            this.checkBoxQuestIsMainQuest.AutoSize = true;
            this.checkBoxQuestIsMainQuest.Enabled = false;
            this.checkBoxQuestIsMainQuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxQuestIsMainQuest.Location = new System.Drawing.Point(9, 18);
            this.checkBoxQuestIsMainQuest.Name = "checkBoxQuestIsMainQuest";
            this.checkBoxQuestIsMainQuest.Size = new System.Drawing.Size(77, 17);
            this.checkBoxQuestIsMainQuest.TabIndex = 28;
            this.checkBoxQuestIsMainQuest.Text = "MainQuest";
            this.checkBoxQuestIsMainQuest.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxDestroyLocation);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBoxDestroyNPCName);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(12, 193);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(391, 41);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Destroy";
            // 
            // textBoxDestroyLocation
            // 
            this.textBoxDestroyLocation.Location = new System.Drawing.Point(226, 9);
            this.textBoxDestroyLocation.Name = "textBoxDestroyLocation";
            this.textBoxDestroyLocation.Size = new System.Drawing.Size(153, 20);
            this.textBoxDestroyLocation.TabIndex = 23;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(171, 14);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Location";
            // 
            // textBoxDestroyNPCName
            // 
            this.textBoxDestroyNPCName.Location = new System.Drawing.Point(45, 13);
            this.textBoxDestroyNPCName.Name = "textBoxDestroyNPCName";
            this.textBoxDestroyNPCName.Size = new System.Drawing.Size(116, 20);
            this.textBoxDestroyNPCName.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "NPC";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textBoxBranchNPCName);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBoxBranchTier);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(12, 99);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(391, 41);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Branch";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "NPC ";
            // 
            // textBoxBranchNPCName
            // 
            this.textBoxBranchNPCName.Location = new System.Drawing.Point(45, 13);
            this.textBoxBranchNPCName.Name = "textBoxBranchNPCName";
            this.textBoxBranchNPCName.Size = new System.Drawing.Size(116, 20);
            this.textBoxBranchNPCName.TabIndex = 19;
            this.textBoxBranchNPCName.TextChanged += new System.EventHandler(this.textBoxBranchNPCName_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(194, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Tier";
            // 
            // textBoxBranchTier
            // 
            this.textBoxBranchTier.Location = new System.Drawing.Point(226, 13);
            this.textBoxBranchTier.Name = "textBoxBranchTier";
            this.textBoxBranchTier.Size = new System.Drawing.Size(153, 20);
            this.textBoxBranchTier.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBoxActionMonster);
            this.groupBox1.Controls.Add(this.comboBoxActionTerrain);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxActionTier);
            this.groupBox1.Controls.Add(this.comboBoxAction);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(526, 55);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(324, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Monster";
            // 
            // comboBoxActionMonster
            // 
            this.comboBoxActionMonster.FormattingEnabled = true;
            this.comboBoxActionMonster.Items.AddRange(new object[] {
            "eye_sack",
            "nibbler",
            "worms",
            "headless",
            "ghost",
            "zombion",
            "hooded",
            "grumpass",
            "steve",
            "obelisk",
            "giant",
            "golem",
            "fish",
            "pyramid",
            "cube"});
            this.comboBoxActionMonster.Location = new System.Drawing.Point(375, 20);
            this.comboBoxActionMonster.Name = "comboBoxActionMonster";
            this.comboBoxActionMonster.Size = new System.Drawing.Size(134, 21);
            this.comboBoxActionMonster.TabIndex = 21;
            this.comboBoxActionMonster.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // comboBoxActionTerrain
            // 
            this.comboBoxActionTerrain.FormattingEnabled = true;
            this.comboBoxActionTerrain.Items.AddRange(new object[] {
            "grass",
            "forest",
            "city",
            "mountain",
            "cave",
            "dungeon"});
            this.comboBoxActionTerrain.Location = new System.Drawing.Point(226, 19);
            this.comboBoxActionTerrain.Name = "comboBoxActionTerrain";
            this.comboBoxActionTerrain.Size = new System.Drawing.Size(92, 21);
            this.comboBoxActionTerrain.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(112, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Tier";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(186, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Terrain";
            // 
            // textBoxActionTier
            // 
            this.textBoxActionTier.Location = new System.Drawing.Point(143, 20);
            this.textBoxActionTier.Name = "textBoxActionTier";
            this.textBoxActionTier.Size = new System.Drawing.Size(42, 20);
            this.textBoxActionTier.TabIndex = 17;
            // 
            // comboBoxAction
            // 
            this.comboBoxAction.FormattingEnabled = true;
            this.comboBoxAction.Items.AddRange(new object[] {
            "next",
            "end",
            "merchant",
            "combat"});
            this.comboBoxAction.Location = new System.Drawing.Point(6, 19);
            this.comboBoxAction.Name = "comboBoxAction";
            this.comboBoxAction.Size = new System.Drawing.Size(100, 21);
            this.comboBoxAction.TabIndex = 16;
            this.comboBoxAction.SelectedIndexChanged += new System.EventHandler(this.comboBoxAction_SelectedIndexChanged);
            this.comboBoxAction.TextChanged += new System.EventHandler(this.comboBoxAction_TextChanged);
            // 
            // buttonCopyAnswer
            // 
            this.buttonCopyAnswer.Location = new System.Drawing.Point(12, 9);
            this.buttonCopyAnswer.Name = "buttonCopyAnswer";
            this.buttonCopyAnswer.Size = new System.Drawing.Size(86, 23);
            this.buttonCopyAnswer.TabIndex = 40;
            this.buttonCopyAnswer.Text = "Copy Answer";
            this.buttonCopyAnswer.UseVisualStyleBackColor = true;
            this.buttonCopyAnswer.Click += new System.EventHandler(this.buttonCopyAnswer_Click);
            // 
            // buttonPasteAnswer
            // 
            this.buttonPasteAnswer.Enabled = false;
            this.buttonPasteAnswer.Location = new System.Drawing.Point(104, 9);
            this.buttonPasteAnswer.Name = "buttonPasteAnswer";
            this.buttonPasteAnswer.Size = new System.Drawing.Size(83, 23);
            this.buttonPasteAnswer.TabIndex = 41;
            this.buttonPasteAnswer.Text = "Paste Answer";
            this.buttonPasteAnswer.UseVisualStyleBackColor = true;
            this.buttonPasteAnswer.Click += new System.EventHandler(this.buttonPasteAnswer_Click);
            // 
            // timerCheckPasteAnswerAttributes
            // 
            this.timerCheckPasteAnswerAttributes.Enabled = true;
            this.timerCheckPasteAnswerAttributes.Interval = 50;
            this.timerCheckPasteAnswerAttributes.Tick += new System.EventHandler(this.timerCheckPasteAnswerAttributes_Tick);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBoxSpawnLocation);
            this.groupBox9.Controls.Add(this.label2);
            this.groupBox9.Controls.Add(this.textBoxSpawnNameLabel);
            this.groupBox9.Controls.Add(this.label1);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox9.Location = new System.Drawing.Point(12, 146);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(391, 41);
            this.groupBox9.TabIndex = 43;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Spawn";
            // 
            // textBoxSpawnLocation
            // 
            this.textBoxSpawnLocation.Location = new System.Drawing.Point(226, 13);
            this.textBoxSpawnLocation.Name = "textBoxSpawnLocation";
            this.textBoxSpawnLocation.Size = new System.Drawing.Size(153, 20);
            this.textBoxSpawnLocation.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(172, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Location";
            // 
            // textBoxSpawnNameLabel
            // 
            this.textBoxSpawnNameLabel.Location = new System.Drawing.Point(45, 13);
            this.textBoxSpawnNameLabel.Name = "textBoxSpawnNameLabel";
            this.textBoxSpawnNameLabel.Size = new System.Drawing.Size(116, 20);
            this.textBoxSpawnNameLabel.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "NPC";
            // 
            // giveItemPanel1
            // 
            this.giveItemPanel1.Location = new System.Drawing.Point(156, 404);
            this.giveItemPanel1.Name = "giveItemPanel1";
            this.giveItemPanel1.Size = new System.Drawing.Size(382, 60);
            this.giveItemPanel1.TabIndex = 42;
            // 
            // AnswerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.giveItemPanel1);
            this.Controls.Add(this.buttonPasteAnswer);
            this.Controls.Add(this.buttonCopyAnswer);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "AnswerPanel";
            this.Size = new System.Drawing.Size(622, 734);
            this.Load += new System.EventHandler(this.TierAnswerPanel_Load);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCheckValue)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBoxCutsceneId;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBoxRewardType;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxCheckFail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxCheckSuccess;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxCheckType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBoxMarkerId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxMarkerAction;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxMarkerQuestId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBoxMarkerIsMainQuest;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxQuestUpdate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxQuestStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxQuestQuestId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxQuestIsMainQuest;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxDestroyNPCName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxBranchNPCName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxBranchTier;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxActionMonster;
        private System.Windows.Forms.ComboBox comboBoxActionTerrain;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxActionTier;
        private System.Windows.Forms.ComboBox comboBoxAction;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button buttonCopyAnswer;
        private System.Windows.Forms.Button buttonPasteAnswer;
        private System.Windows.Forms.Timer timerCheckPasteAnswerAttributes;
        private System.Windows.Forms.NumericUpDown numericUpDownCheckValue;
        private UI.RewardItemPanel rewardItemPanel1;
        private UI.RewardItemPanel rewardItemPanel4;
        private UI.RewardItemPanel rewardItemPanel3;
        private UI.RewardItemPanel rewardItemPanel2;
        private UI.GiveItemPanel giveItemPanel1;
        private System.Windows.Forms.TextBox textBoxRewardValue;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBoxSpawnLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSpawnNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDestroyLocation;
        private System.Windows.Forms.Label label20;
    }
}
