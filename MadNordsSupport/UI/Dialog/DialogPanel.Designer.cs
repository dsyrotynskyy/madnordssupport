﻿namespace MadNordsSupport
{
    partial class DialogPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonUpdateDialog = new System.Windows.Forms.Button();
            this.textBoxDialogId = new System.Windows.Forms.TextBox();
            this.buttonAddDialog = new System.Windows.Forms.Button();
            this.labelTiers = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dialogFileName = new System.Windows.Forms.TextBox();
            this.dialogFileButton = new System.Windows.Forms.Button();
            this.dataGridViewTier = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditTierColumn = new DataGridViewDisableButtonColumn();
            this.CopyTierColumn = new DataGridViewDisableButtonColumn();
            this.PasteTierColumn = new DataGridViewDisableButtonColumn();
            this.TierBugsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dialogsCount = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.buttonPaste = new System.Windows.Forms.Button();
            this.contextMenuTier = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddRowUp = new System.Windows.Forms.ToolStripMenuItem();
            this.AddRowDown = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonDeleteDialog = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewDisableButtonColumn1 = new DataGridViewDisableButtonColumn();
            this.dataGridViewDisableButtonColumn2 = new DataGridViewDisableButtonColumn();
            this.dataGridViewDisableButtonColumn3 = new DataGridViewDisableButtonColumn();
            this.textBoxDialogName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSortXML = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTier)).BeginInit();
            this.contextMenuTier.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdateDialog
            // 
            this.buttonUpdateDialog.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonUpdateDialog.Location = new System.Drawing.Point(716, 453);
            this.buttonUpdateDialog.Name = "buttonUpdateDialog";
            this.buttonUpdateDialog.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdateDialog.TabIndex = 34;
            this.buttonUpdateDialog.Text = "Update";
            this.buttonUpdateDialog.UseVisualStyleBackColor = true;
            this.buttonUpdateDialog.Click += new System.EventHandler(this.AddUpdateCurrentDialog);
            // 
            // textBoxDialogId
            // 
            this.textBoxDialogId.Location = new System.Drawing.Point(93, 55);
            this.textBoxDialogId.Name = "textBoxDialogId";
            this.textBoxDialogId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxDialogId.Size = new System.Drawing.Size(100, 20);
            this.textBoxDialogId.TabIndex = 33;
            this.textBoxDialogId.TextChanged += new System.EventHandler(this.textBoxDialogId_TextChanged);
            // 
            // buttonAddDialog
            // 
            this.buttonAddDialog.Location = new System.Drawing.Point(363, 53);
            this.buttonAddDialog.Name = "buttonAddDialog";
            this.buttonAddDialog.Size = new System.Drawing.Size(90, 23);
            this.buttonAddDialog.TabIndex = 32;
            this.buttonAddDialog.Text = "Add";
            this.buttonAddDialog.UseVisualStyleBackColor = true;
            this.buttonAddDialog.Click += new System.EventHandler(this.AddUpdateCurrentDialog);
            // 
            // labelTiers
            // 
            this.labelTiers.AutoSize = true;
            this.labelTiers.Location = new System.Drawing.Point(24, 82);
            this.labelTiers.Name = "labelTiers";
            this.labelTiers.Size = new System.Drawing.Size(30, 13);
            this.labelTiers.TabIndex = 31;
            this.labelTiers.Text = "Tiers";
            this.labelTiers.Click += new System.EventHandler(this.labelTier_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 30;
            this.label1.Text = "Dialog Id";
            // 
            // dialogFileName
            // 
            this.dialogFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dialogFileName.Location = new System.Drawing.Point(128, 5);
            this.dialogFileName.Name = "dialogFileName";
            this.dialogFileName.ReadOnly = true;
            this.dialogFileName.Size = new System.Drawing.Size(678, 20);
            this.dialogFileName.TabIndex = 29;
            this.dialogFileName.TextChanged += new System.EventHandler(this.questFileName_TextChanged);
            // 
            // dialogFileButton
            // 
            this.dialogFileButton.Location = new System.Drawing.Point(23, 3);
            this.dialogFileButton.Name = "dialogFileButton";
            this.dialogFileButton.Size = new System.Drawing.Size(99, 23);
            this.dialogFileButton.TabIndex = 28;
            this.dialogFileButton.Text = "Select Dialog File";
            this.dialogFileButton.UseVisualStyleBackColor = true;
            this.dialogFileButton.Click += new System.EventHandler(this.dialogFileButton_Click);
            // 
            // dataGridViewTier
            // 
            this.dataGridViewTier.AllowDrop = true;
            this.dataGridViewTier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewTier.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dataGridViewTier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.CommentColumn,
            this.EditTierColumn,
            this.CopyTierColumn,
            this.PasteTierColumn,
            this.TierBugsColumn});
            this.dataGridViewTier.Location = new System.Drawing.Point(23, 98);
            this.dataGridViewTier.MultiSelect = false;
            this.dataGridViewTier.Name = "dataGridViewTier";
            this.dataGridViewTier.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewTier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewTier.Size = new System.Drawing.Size(783, 322);
            this.dataGridViewTier.StandardTab = true;
            this.dataGridViewTier.TabIndex = 27;
            this.dataGridViewTier.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTier_CellContentClick);
            this.dataGridViewTier.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewTier_CellMouseUp);
            this.dataGridViewTier.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTier_CellValueChanged);
            this.dataGridViewTier.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewTier_RowsAdded);
            // 
            // IdColumn
            // 
            this.IdColumn.Frozen = true;
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Width = 75;
            // 
            // CommentColumn
            // 
            this.CommentColumn.Frozen = true;
            this.CommentColumn.HeaderText = "Comment";
            this.CommentColumn.Name = "CommentColumn";
            this.CommentColumn.Width = 200;
            // 
            // EditTierColumn
            // 
            this.EditTierColumn.Frozen = true;
            this.EditTierColumn.HeaderText = "Edit Tier";
            this.EditTierColumn.Name = "EditTierColumn";
            this.EditTierColumn.Width = 75;
            // 
            // CopyTierColumn
            // 
            this.CopyTierColumn.Frozen = true;
            this.CopyTierColumn.HeaderText = "Copy Tier";
            this.CopyTierColumn.Name = "CopyTierColumn";
            this.CopyTierColumn.Width = 75;
            // 
            // PasteTierColumn
            // 
            this.PasteTierColumn.FillWeight = 80F;
            this.PasteTierColumn.Frozen = true;
            this.PasteTierColumn.HeaderText = "Paste Tier";
            this.PasteTierColumn.Name = "PasteTierColumn";
            this.PasteTierColumn.Width = 75;
            // 
            // TierBugsColumn
            // 
            this.TierBugsColumn.HeaderText = "Tier Bugs";
            this.TierBugsColumn.Name = "TierBugsColumn";
            this.TierBugsColumn.ReadOnly = true;
            this.TierBugsColumn.Width = 200;
            // 
            // dialogsCount
            // 
            this.dialogsCount.AutoSize = true;
            this.dialogsCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dialogsCount.Location = new System.Drawing.Point(24, 29);
            this.dialogsCount.Name = "dialogsCount";
            this.dialogsCount.Size = new System.Drawing.Size(16, 17);
            this.dialogsCount.TabIndex = 36;
            this.dialogsCount.Text = "0";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // buttonCopy
            // 
            this.buttonCopy.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopy.Location = new System.Drawing.Point(23, 453);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(90, 23);
            this.buttonCopy.TabIndex = 37;
            this.buttonCopy.Text = "Copy Dialog";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // buttonPaste
            // 
            this.buttonPaste.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPaste.Enabled = false;
            this.buttonPaste.Location = new System.Drawing.Point(119, 453);
            this.buttonPaste.Name = "buttonPaste";
            this.buttonPaste.Size = new System.Drawing.Size(90, 23);
            this.buttonPaste.TabIndex = 38;
            this.buttonPaste.Text = "Paste Dialog";
            this.buttonPaste.UseVisualStyleBackColor = true;
            this.buttonPaste.Click += new System.EventHandler(this.buttonPaste_Click);
            // 
            // contextMenuTier
            // 
            this.contextMenuTier.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.toolStripMenuItemAddRowUp,
            this.AddRowDown});
            this.contextMenuTier.Name = "contextMenuTier";
            this.contextMenuTier.Size = new System.Drawing.Size(157, 70);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.deleteToolStripMenuItem.Text = "Delete Row";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripMenuItemAddRowUp
            // 
            this.toolStripMenuItemAddRowUp.Name = "toolStripMenuItemAddRowUp";
            this.toolStripMenuItemAddRowUp.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItemAddRowUp.Text = "Add Row Up";
            this.toolStripMenuItemAddRowUp.Click += new System.EventHandler(this.toolStripMenuItemAddRowUp_Click);
            // 
            // AddRowDown
            // 
            this.AddRowDown.Name = "AddRowDown";
            this.AddRowDown.Size = new System.Drawing.Size(156, 22);
            this.AddRowDown.Text = "Add Row Down";
            this.AddRowDown.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // buttonDeleteDialog
            // 
            this.buttonDeleteDialog.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonDeleteDialog.Location = new System.Drawing.Point(622, 454);
            this.buttonDeleteDialog.Name = "buttonDeleteDialog";
            this.buttonDeleteDialog.Size = new System.Drawing.Size(90, 23);
            this.buttonDeleteDialog.TabIndex = 39;
            this.buttonDeleteDialog.Text = "Delete";
            this.buttonDeleteDialog.UseVisualStyleBackColor = true;
            this.buttonDeleteDialog.Click += new System.EventHandler(this.buttonDeleteDialog_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 75;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Comment";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 400;
            // 
            // dataGridViewDisableButtonColumn1
            // 
            this.dataGridViewDisableButtonColumn1.Frozen = true;
            this.dataGridViewDisableButtonColumn1.HeaderText = "Edit Tier";
            this.dataGridViewDisableButtonColumn1.Name = "dataGridViewDisableButtonColumn1";
            this.dataGridViewDisableButtonColumn1.Width = 75;
            // 
            // dataGridViewDisableButtonColumn2
            // 
            this.dataGridViewDisableButtonColumn2.Frozen = true;
            this.dataGridViewDisableButtonColumn2.HeaderText = "CopyTier";
            this.dataGridViewDisableButtonColumn2.Name = "dataGridViewDisableButtonColumn2";
            this.dataGridViewDisableButtonColumn2.Width = 75;
            // 
            // dataGridViewDisableButtonColumn3
            // 
            this.dataGridViewDisableButtonColumn3.Frozen = true;
            this.dataGridViewDisableButtonColumn3.HeaderText = "PasteTier";
            this.dataGridViewDisableButtonColumn3.Name = "dataGridViewDisableButtonColumn3";
            this.dataGridViewDisableButtonColumn3.Width = 75;
            // 
            // textBoxDialogName
            // 
            this.textBoxDialogName.Location = new System.Drawing.Point(257, 55);
            this.textBoxDialogName.Name = "textBoxDialogName";
            this.textBoxDialogName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxDialogName.Size = new System.Drawing.Size(100, 20);
            this.textBoxDialogName.TabIndex = 41;
            this.textBoxDialogName.TextChanged += new System.EventHandler(this.textBoxDialogName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(206, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 40;
            this.label2.Text = "Name";
            // 
            // buttonSortXML
            // 
            this.buttonSortXML.Location = new System.Drawing.Point(716, 53);
            this.buttonSortXML.Name = "buttonSortXML";
            this.buttonSortXML.Size = new System.Drawing.Size(90, 23);
            this.buttonSortXML.TabIndex = 42;
            this.buttonSortXML.Text = "Sort XML";
            this.buttonSortXML.UseVisualStyleBackColor = true;
            this.buttonSortXML.Click += new System.EventHandler(this.buttonSortXML_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(592, 53);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(90, 23);
            this.buttonNext.TabIndex = 44;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(496, 53);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(90, 23);
            this.buttonPrev.TabIndex = 43;
            this.buttonPrev.Text = "Prev";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // DialogPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.buttonSortXML);
            this.Controls.Add(this.textBoxDialogName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonDeleteDialog);
            this.Controls.Add(this.buttonPaste);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.dialogsCount);
            this.Controls.Add(this.buttonUpdateDialog);
            this.Controls.Add(this.textBoxDialogId);
            this.Controls.Add(this.buttonAddDialog);
            this.Controls.Add(this.labelTiers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dialogFileName);
            this.Controls.Add(this.dialogFileButton);
            this.Controls.Add(this.dataGridViewTier);
            this.Name = "DialogPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.DialogPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTier)).EndInit();
            this.contextMenuTier.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonUpdateDialog;
        private System.Windows.Forms.TextBox textBoxDialogId;
        private System.Windows.Forms.Button buttonAddDialog;
        private System.Windows.Forms.Label labelTiers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dialogFileName;
        private System.Windows.Forms.Button dialogFileButton;
        private System.Windows.Forms.DataGridView dataGridViewTier;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label dialogsCount;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.Button buttonPaste;
        private System.Windows.Forms.ContextMenuStrip contextMenuTier;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button buttonDeleteDialog;
        private System.Windows.Forms.ToolStripMenuItem AddRowDown;
        private DataGridViewDisableButtonColumn dataGridViewDisableButtonColumn1;
        private DataGridViewDisableButtonColumn dataGridViewDisableButtonColumn2;
        private DataGridViewDisableButtonColumn dataGridViewDisableButtonColumn3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddRowUp;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommentColumn;
        private DataGridViewDisableButtonColumn EditTierColumn;
        private DataGridViewDisableButtonColumn CopyTierColumn;
        private DataGridViewDisableButtonColumn PasteTierColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TierBugsColumn;
        private System.Windows.Forms.TextBox textBoxDialogName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSortXML;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
    }
}
