﻿namespace MadNordsSupport.UI
{
    partial class RewardItemPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.comboBoxRewardId = new System.Windows.Forms.ComboBox();
            this.comboBoxRewardEnchanment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Id = new System.Windows.Forms.Label();
            this.comboBoxRewardType = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.comboBoxRewardId);
            this.groupBox7.Controls.Add(this.comboBoxRewardEnchanment);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.Id);
            this.groupBox7.Controls.Add(this.comboBoxRewardType);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(3, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(488, 46);
            this.groupBox7.TabIndex = 39;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Reward Id";
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // comboBoxRewardId
            // 
            this.comboBoxRewardId.FormattingEnabled = true;
            this.comboBoxRewardId.Items.AddRange(new object[] {
            "Eyes of the Eye Sack",
            "Dead Eye Sack",
            "Nibbler\'s Tongue",
            "Broken Teeth",
            "Ectoplasm",
            "Brainacles",
            "Zombion\'s Sting",
            "Human Flesh",
            "Bandages",
            "Piece of the Monolith",
            "Egg Sack of the Dragon"});
            this.comboBoxRewardId.Location = new System.Drawing.Point(171, 16);
            this.comboBoxRewardId.Name = "comboBoxRewardId";
            this.comboBoxRewardId.Size = new System.Drawing.Size(137, 21);
            this.comboBoxRewardId.TabIndex = 38;
            this.comboBoxRewardId.TextChanged += new System.EventHandler(this.comboBoxRewardId_TextChanged);
            // 
            // comboBoxRewardEnchanment
            // 
            this.comboBoxRewardEnchanment.FormattingEnabled = true;
            this.comboBoxRewardEnchanment.Items.AddRange(new object[] {
            "stat_strength",
            "stat_agility",
            "stat_intellect",
            "stat_vitality",
            "stat_wisdom",
            "armor_class",
            "chance_dodge",
            "chance_crit",
            "resist_phys",
            "resist_mag"});
            this.comboBoxRewardEnchanment.Location = new System.Drawing.Point(365, 16);
            this.comboBoxRewardEnchanment.Name = "comboBoxRewardEnchanment";
            this.comboBoxRewardEnchanment.Size = new System.Drawing.Size(107, 21);
            this.comboBoxRewardEnchanment.TabIndex = 36;
            this.comboBoxRewardEnchanment.SelectedIndexChanged += new System.EventHandler(this.comboBoxRewardEnchanment_SelectedIndexChanged);
            this.comboBoxRewardEnchanment.TextChanged += new System.EventHandler(this.comboBoxRewardEnchanment_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(312, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Enchant";
            // 
            // Id
            // 
            this.Id.AutoSize = true;
            this.Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Id.Location = new System.Drawing.Point(149, 19);
            this.Id.Name = "Id";
            this.Id.Size = new System.Drawing.Size(16, 13);
            this.Id.TabIndex = 35;
            this.Id.Text = "Id";
            // 
            // comboBoxRewardType
            // 
            this.comboBoxRewardType.FormattingEnabled = true;
            this.comboBoxRewardType.Items.AddRange(new object[] {
            "weapon",
            "armor",
            "artefact",
            "potion",
            "misc",
            "quest"});
            this.comboBoxRewardType.Location = new System.Drawing.Point(43, 16);
            this.comboBoxRewardType.Name = "comboBoxRewardType";
            this.comboBoxRewardType.Size = new System.Drawing.Size(100, 21);
            this.comboBoxRewardType.TabIndex = 32;
            this.comboBoxRewardType.SelectedIndexChanged += new System.EventHandler(this.comboBoxRewardType_SelectedIndexChanged);
            this.comboBoxRewardType.TextChanged += new System.EventHandler(this.comboBoxRewardType_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(6, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Type";
            // 
            // RewardItemPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox7);
            this.Name = "RewardItemPanel";
            this.Size = new System.Drawing.Size(499, 55);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox comboBoxRewardEnchanment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Id;
        private System.Windows.Forms.ComboBox comboBoxRewardType;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxRewardId;
    }
}
