﻿namespace MadNordsSupport.UI
{
    partial class LocationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.locationFileButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxLocationId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLocationLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownX = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownY = new System.Windows.Forms.NumericUpDown();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonRemoveLocation = new System.Windows.Forms.Button();
            this.buttonCopyLocation = new System.Windows.Forms.Button();
            this.buttonPasteLocation = new System.Windows.Forms.Button();
            this.buttonAddUpdateLocation = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileName.Location = new System.Drawing.Point(128, 5);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.ReadOnly = true;
            this.textBoxFileName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxFileName.Size = new System.Drawing.Size(678, 20);
            this.textBoxFileName.TabIndex = 20;
            this.textBoxFileName.Text = "E";
            // 
            // locationFileButton
            // 
            this.locationFileButton.Location = new System.Drawing.Point(23, 3);
            this.locationFileButton.Name = "locationFileButton";
            this.locationFileButton.Size = new System.Drawing.Size(99, 23);
            this.locationFileButton.TabIndex = 19;
            this.locationFileButton.Text = " Location File";
            this.locationFileButton.UseVisualStyleBackColor = true;
            this.locationFileButton.Click += new System.EventHandler(this.questFileButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(103, 34);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Id";
            // 
            // textBoxLocationId
            // 
            this.textBoxLocationId.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxLocationId.Location = new System.Drawing.Point(128, 31);
            this.textBoxLocationId.Name = "textBoxLocationId";
            this.textBoxLocationId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxLocationId.Size = new System.Drawing.Size(227, 20);
            this.textBoxLocationId.TabIndex = 21;
            this.textBoxLocationId.TextChanged += new System.EventHandler(this.textBoxLocationId_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Location";
            // 
            // textBoxLocationLocation
            // 
            this.textBoxLocationLocation.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxLocationLocation.Location = new System.Drawing.Point(128, 83);
            this.textBoxLocationLocation.Name = "textBoxLocationLocation";
            this.textBoxLocationLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxLocationLocation.Size = new System.Drawing.Size(227, 20);
            this.textBoxLocationLocation.TabIndex = 23;
            this.textBoxLocationLocation.TextChanged += new System.EventHandler(this.textBoxLocationLocation_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "X";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Y";
            // 
            // numericUpDownX
            // 
            this.numericUpDownX.Location = new System.Drawing.Point(128, 108);
            this.numericUpDownX.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.numericUpDownX.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDownX.Name = "numericUpDownX";
            this.numericUpDownX.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDownX.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownX.TabIndex = 27;
            // 
            // numericUpDownY
            // 
            this.numericUpDownY.Location = new System.Drawing.Point(128, 134);
            this.numericUpDownY.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.numericUpDownY.Minimum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            -2147483648});
            this.numericUpDownY.Name = "numericUpDownY";
            this.numericUpDownY.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericUpDownY.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownY.TabIndex = 28;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buttonRemoveLocation
            // 
            this.buttonRemoveLocation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRemoveLocation.Location = new System.Drawing.Point(622, 454);
            this.buttonRemoveLocation.Name = "buttonRemoveLocation";
            this.buttonRemoveLocation.Size = new System.Drawing.Size(90, 23);
            this.buttonRemoveLocation.TabIndex = 36;
            this.buttonRemoveLocation.Text = "Delete Location";
            this.buttonRemoveLocation.UseVisualStyleBackColor = true;
            this.buttonRemoveLocation.Click += new System.EventHandler(this.buttonRemoveLocation_Click);
            // 
            // buttonCopyLocation
            // 
            this.buttonCopyLocation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCopyLocation.Location = new System.Drawing.Point(23, 454);
            this.buttonCopyLocation.Name = "buttonCopyLocation";
            this.buttonCopyLocation.Size = new System.Drawing.Size(90, 23);
            this.buttonCopyLocation.TabIndex = 35;
            this.buttonCopyLocation.Text = "Copy Location";
            this.buttonCopyLocation.UseVisualStyleBackColor = true;
            this.buttonCopyLocation.Click += new System.EventHandler(this.buttonCopyLocation_Click);
            // 
            // buttonPasteLocation
            // 
            this.buttonPasteLocation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonPasteLocation.Enabled = false;
            this.buttonPasteLocation.Location = new System.Drawing.Point(117, 454);
            this.buttonPasteLocation.Name = "buttonPasteLocation";
            this.buttonPasteLocation.Size = new System.Drawing.Size(90, 23);
            this.buttonPasteLocation.TabIndex = 34;
            this.buttonPasteLocation.Text = "Paste Location";
            this.buttonPasteLocation.UseVisualStyleBackColor = true;
            this.buttonPasteLocation.Click += new System.EventHandler(this.buttonPasteLocation_Click);
            // 
            // buttonAddUpdateLocation
            // 
            this.buttonAddUpdateLocation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAddUpdateLocation.Location = new System.Drawing.Point(716, 454);
            this.buttonAddUpdateLocation.Name = "buttonAddUpdateLocation";
            this.buttonAddUpdateLocation.Size = new System.Drawing.Size(90, 23);
            this.buttonAddUpdateLocation.TabIndex = 33;
            this.buttonAddUpdateLocation.Text = "Add Location";
            this.buttonAddUpdateLocation.UseVisualStyleBackColor = true;
            this.buttonAddUpdateLocation.Click += new System.EventHandler(this.buttonAddUpdateLocation_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 60);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Name";
            // 
            // textBoxName
            // 
            this.textBoxName.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.textBoxName.Location = new System.Drawing.Point(128, 57);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxName.Size = new System.Drawing.Size(227, 20);
            this.textBoxName.TabIndex = 37;
            this.textBoxName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // LocationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonRemoveLocation);
            this.Controls.Add(this.buttonCopyLocation);
            this.Controls.Add(this.buttonPasteLocation);
            this.Controls.Add(this.buttonAddUpdateLocation);
            this.Controls.Add(this.numericUpDownY);
            this.Controls.Add(this.numericUpDownX);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxLocationLocation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxLocationId);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.locationFileButton);
            this.Name = "LocationPanel";
            this.Size = new System.Drawing.Size(825, 480);
            this.Load += new System.EventHandler(this.LocationPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Button locationFileButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxLocationId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLocationLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownX;
        private System.Windows.Forms.NumericUpDown numericUpDownY;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonRemoveLocation;
        private System.Windows.Forms.Button buttonCopyLocation;
        private System.Windows.Forms.Button buttonPasteLocation;
        private System.Windows.Forms.Button buttonAddUpdateLocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxName;
    }
}
