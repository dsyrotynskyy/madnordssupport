﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadNordsSupport.UI.BasePanels;
using MadNordsSupport.Logic;

namespace MadNordsSupport.UI
{
    partial class SignPanel : BaseReadingPanel
    {
        public SignPanel()
        {
            InitializeComponent();

            TryFileOnStart();
        }

        public override TextBox TextBoxDataId
        {
            get
            {
                return this.textBoxEntityId;
            }
        }

        protected override Button AddUpdateButton
        {
            get
            {
                return this.buttonAddUpdateEntity;
            }
        }

        protected override SignDAO dao
        {
            get
            {
                return SignDAO.Instance;
            }
        }

        protected override TextBox FileNameTextBox
        {
            get
            {
                return this.textBoxLocationFileName;
            }
        }

        protected override Button RemoveButton
        {
            get
            {
                return this.buttonRemoveEntity;
            }
        }

        public override void UpdateDataByFields(Sign data, bool includeId)
        {
            if (includeId)
            {
                data.Id = this.textBoxEntityId.Text;
            }

            data.text = this.richTextBoxSignText.Text;
        }

        protected override void DisplayData(Sign l, bool includeId)
        {
            if (includeId)
            {
                this.textBoxEntityId.Text = l.Id;
            }
            this.richTextBoxSignText.Text = l.text;
        }

        private void entityFileButton_Click(object sender, EventArgs e)
        {
            if (SelectDataFile(this.openFileDialog1))
            {
                RenewAllFieldsByData();
            }
        }



        private void buttonCopyEntity_Click(object sender, EventArgs e)
        {

        }

        private void buttonPasteEntity_Click(object sender, EventArgs e)
        {

        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBoxEntityId_TextChanged(object sender, EventArgs e)
        {
            TextBoxIdChanged();
        }

        private void textBoxLocationFileName_TextChanged(object sender, EventArgs e)
        {
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void buttonAddUpdateEntity_Click(object sender, EventArgs e)
        {
            AddUpdateCurrentData();
        }

        private void buttonRemoveEntity_Click(object sender, EventArgs e)
        {
            RemoveCurrentEntity();
        }
    }
}
