﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MadNordsSupport {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class MadNordsSettings : global::System.Configuration.ApplicationSettingsBase {
        
        private static MadNordsSettings defaultInstance = ((MadNordsSettings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new MadNordsSettings())));
        
        public static MadNordsSettings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Users\\Dan\\Documents\\visual studio 2013\\Projects\\MadNordsSupport\\quests.xml")]
        public string QuestFileName {
            get {
                return ((string)(this["QuestFileName"]));
            }
            set {
                this["QuestFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Users\\Dan\\Documents\\visual studio 2013\\Projects\\MadNordsSupport\\dialogs(1).xml" +
            "")]
        public string DialogFiIeName {
            get {
                return ((string)(this["DialogFiIeName"]));
            }
            set {
                this["DialogFiIeName"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring>Metal Plated Log</string>\r\n  <string>Blessed Boulder</string>\r\n  <string>P" +
            "etrified Fish</string>\r\n  <string>Gnome House</string>\r\n  <string>Troll\'s Food</" +
            "string>\r\n  <string>Fork of Doom</string>\r\n  <string>Dead Leg</string>\r\n  <string" +
            ">Giant\'s Tooth</string>\r\n  <string>Flesheating Glove</string>\r\n  <string>Kebab S" +
            "word</string>\r\n  <string>Pink Sword</string>\r\n  <string>Sword of Insanity</strin" +
            "g>\r\n  <string>Rainbow Sword</string>\r\n  <string>Imaginary Sword</string>\r\n  <str" +
            "ing>Blood Sword</string>\r\n  <string>Stinky Boot</string>\r\n  <string>Your Favouri" +
            "te Glove</string>\r\n  <string>Mad Beaver</string>\r\n  <string>Cheat Scroll</string" +
            ">\r\n  <string>Death Tea</string>\r\n  <string>Stolen Mummy</string>\r\n  <string>Cata" +
            "rang</string>\r\n  <string>Noose</string>\r\n  <string>Paper Dagger</string>\r\n  <str" +
            "ing>Bag of Stolen Useless Goods</string>\r\n  <string>Honest Hands</string>\r\n  <st" +
            "ring>Bribe</string>\r\n  <string>Dark Pact</string>\r\n  <string>Allseeing Eye</stri" +
            "ng>\r\n  <string>Lunch Break Sign</string>\r\n  <string>Book of Seizures</string>\r\n " +
            " <string>Summoning Wardrobe</string>\r\n  <string>Portal Plate</string>\r\n  <string" +
            ">11,5 Dimensional Cube</string>\r\n  <string>Odd Shroom</string>\r\n  <string>Nick\'s" +
            " Tombstone</string>\r\n  <string>Nick\'s Dried Head</string>\r\n  <string>Nick\'s Brid" +
            "e\'s Spine</string>\r\n  <string>Nick\'s Grimoire of Brown Magic</string>\r\n  <string" +
            ">Nick\'s Jar of Parasites</string>\r\n  <string>Square of Infinite Blackness</strin" +
            "g>\r\n  <string>Craid\'s Pocket Altar</string>\r\n  <string>Toe of Darkness</string>\r" +
            "\n  <string>Torch of Antilight</string>\r\n  <string>Legendary Black Obelisk Action" +
            " Figure</string>\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Weapon_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Weapon_Items"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring>Iron Barrel</string>\r\n  <string>Stone Bag</string>\r\n  <string>Squid Hat</s" +
            "tring>\r\n  <string>Bearded Helm</string>\r\n  <string>Cuirass of Troll\'s Calluses</" +
            "string>\r\n  <string>Pot of Inevitability</string>\r\n  <string>Rib Vest</string>\r\n " +
            " <string>Giant\'s Nail</string>\r\n  <string>Itchy Sweater</string>\r\n  <string>Meat" +
            " Armor</string>\r\n  <string>Pink Dress</string>\r\n  <string>Glass Shield</string>\r" +
            "\n  <string>Unicorn\'s Horn</string>\r\n  <string>Imaginary Armor</string>\r\n  <strin" +
            "g>Bloody Mask</string>\r\n  <string>Darned Stained Cloak</string>\r\n  <string>Your " +
            "Favourite Leather Armor with Fancy Tracery</string>\r\n  <string>Beaver\'s Dam</str" +
            "ing>\r\n  <string>Helm from Unreleased DLC</string>\r\n  <string>Monocle of Meanness" +
            "</string>\r\n  <string>Sarcophagus</string>\r\n  <string>Cat\'s Fur Balls Vest</strin" +
            "g>\r\n  <string>Clothes of the Hanged Man</string>\r\n  <string>Paper Hat</string>\r\n" +
            "  <string>Stranger\'s Misfitting Vest</string>\r\n  <string>Shoulder Pads</string>\r" +
            "\n  <string>Burning Hat</string>\r\n  <string>Sheep\'s Clothing</string>\r\n  <string>" +
            "Foil Hat</string>\r\n  <string>Dressing Gown</string>\r\n  <string>Acid Coloured Rob" +
            "e</string>\r\n  <string>Monster\'s Mane</string>\r\n  <string>Portal Hood</string>\r\n " +
            " <string>Dodecaedro</string>\r\n  <string>Grass Cloak</string>\r\n  <string>Vlad\'s R" +
            "otten Clothes</string>\r\n  <string>Vlad\'s Scalp</string>\r\n  <string>Nick\'s Bride\'" +
            "s Wedding Dress</string>\r\n  <string>Vlad\'s Mantle of the Brown Magic Order Maste" +
            "r</string>\r\n  <string>Vlad\'s Helminth Belt</string>\r\n  <string>Triangle Cloak of" +
            " Infinite Blackness</string>\r\n  <string>Kraid\'s Ritual Rag</string>\r\n  <string>T" +
            "hongs of Darkness</string>\r\n  <string>Party Robe</string>\r\n  <string>Legendary B" +
            "lack Obelisk Cosplayer\'s Kit</string>\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Armor_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Armor_Items"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring>Iron Acorn</string>\r\n  <string>Philosophical Brick</string>\r\n  <string>Jel" +
            "lyfish Talisman</string>\r\n  <string>Pocket Gnome</string>\r\n  <string>Troll\'s War" +
            "t</string>\r\n  <string>Garnish of Despair</string>\r\n  <string>Animated Eye</strin" +
            "g>\r\n  <string>Giant\'s Booger</string>\r\n  <string>Moth</string>\r\n  <string>Flesh " +
            "Parasite</string>\r\n  <string>Lucky Pink Bowknot</string>\r\n  <string>Toe Ring</st" +
            "ring>\r\n  <string>Amulet of Joy</string>\r\n  <string>Imaginary Artefact</string>\r\n" +
            "  <string>Medal of Honourable Donor</string>\r\n  <string>Rock-solid Sock</string>" +
            "\r\n  <string>Your Favourite Toy</string>\r\n  <string>Beaver\'s Tail</string>\r\n  <st" +
            "ring>Imbalanced Ring</string>\r\n  <string>Bow-tie of Foxery</string>\r\n  <string>A" +
            "nkh</string>\r\n  <string>Cat\'s Claw</string>\r\n  <string>Voodoo Scaffold</string>\r" +
            "\n  <string>Origami</string>\r\n  <string>Overweighted Amulet</string>\r\n  <string>A" +
            "gitation Scroll</string>\r\n  <string>Counterfeit Coin</string>\r\n  <string>Grim Co" +
            "at of Arms</string>\r\n  <string>Proving Tapestry</string>\r\n  <string>Golden Tooth" +
            "pick</string>\r\n  <string>Flower Necklace</string>\r\n  <string>Bag of Magic Dust</" +
            "string>\r\n  <string>Miniature Finger Portal</string>\r\n  <string>Pi Number</string" +
            ">\r\n  <string>Magic Moss</string>\r\n  <string>Dan\'s Family Relic</string>\r\n  <stri" +
            "ng>Dan\'s Set of False Teeth</string>\r\n  <string>Nick\'s Bride\'s Wedding Ring</str" +
            "ing>\r\n  <string>Oleh\'s Brown Magic for Dummies</string>\r\n  <string>Dan\'s Enchant" +
            "ed Tick</string>\r\n  <string>Hexagonal Amulet of Infinite Blackness</string>\r\n  <" +
            "string>Replica of Craid\'s Relic</string>\r\n  <string>Ritual Bowl of Darkness</str" +
            "ing>\r\n  <string>Bottle of Dark Beer</string>\r\n  <string>Legendary Black Obelisk " +
            "Trinket</string>\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Artefact_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Artefact_Items"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <string>Small Health Potion</string>
  <string>Small Magic Potion</string>
  <string>Medium Health Potion</string>
  <string>Medium Magic Potion</string>
  <string>Large Health Potion</string>
  <string>Large Magic Potion</string>
</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Potion_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Potion_Items"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <string>Amber</string>
  <string>Sapphire</string>
  <string>Emerald</string>
  <string>Ruby</string>
  <string>Diamond</string>
  <string>Knight's Broken Sword</string>
  <string>Knight's Broken Shield</string>
  <string>Knight's Broken Boot</string>
  <string>Knight's Broken Gauntlet</string>
  <string>Knight's Broken Helmet</string>
  <string>Evergrowing Fern</string>
  <string>Prehistoric Fossil</string>
  <string>Malfunctioning Magic Scroll</string>
  <string>Petrified Chest</string>
  <string>Ancient Balls</string>
  <string>Golden Festive Fork</string>
  <string>Golden Chalice</string>
  <string>Golden Broken Heart</string>
  <string>Golden Cucumbers</string>
  <string>Souvenir Sword</string>
  <string>Half of the Silver Ring</string>
  <string>Stone Pillar</string>
  <string>DLC</string>
  <string>Offensive Idol</string>
  <string>Skull of King</string>
</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Misc_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Misc_Items"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <string>Eyes of the Eye Sack</string>
  <string>Dead Eye Sack</string>
  <string>Nibbler's Tongue</string>
  <string>Broken Teeth</string>
  <string>Ectoplasm</string>
  <string>Brainacles</string>
  <string>Zombion's Sting</string>
  <string>Human Flesh</string>
  <string>Bandages</string>
  <string>Piece of the Monolith</string>
  <string>Egg Sack of the Dragon</string>
</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Quest_Items {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Quest_Items"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Users\\Dan\\Documents\\visual studio 2013\\Projects\\MadNordsSupport\\reading")]
        public string SignFileName {
            get {
                return ((string)(this["SignFileName"]));
            }
            set {
                this["SignFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Users\\Dan\\Documents\\visual studio 2013\\Projects\\MadNordsSupport\\merchants")]
        public string MerchantFileName {
            get {
                return ((string)(this["MerchantFileName"]));
            }
            set {
                this["MerchantFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Users\\Dan\\Documents\\visual studio 2013\\Projects\\MadNordsSupport\\location")]
        public string LocationFileName {
            get {
                return ((string)(this["LocationFileName"]));
            }
            set {
                this["LocationFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string InventoryFileName {
            get {
                return ((string)(this["InventoryFileName"]));
            }
            set {
                this["InventoryFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string BookFileName {
            get {
                return ((string)(this["BookFileName"]));
            }
            set {
                this["BookFileName"] = value;
            }
        }
    }
}
