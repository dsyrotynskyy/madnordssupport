﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MadNordsSupport
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            int size = TextAnalyzer.Instance.AnalyzeTextSize("Ether. ");
            int size1 = TextAnalyzer.Instance.AnalyzeTextSize("many ");
            int size2 = TextAnalyzer.Instance.AnalyzeTextSize("created ");
            int size3 = TextAnalyzer.Instance.AnalyzeTextSize("worlds ");
            int size4 = TextAnalyzer.Instance.AnalyzeTextSize("escaped");

            int sizeFull = TextAnalyzer.Instance.AnalyzeTextSize("Ether. many created worlds escaped");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

    public class StringPair
    {
        public string Val1;
        public string Val2;

        public StringPair() { }
        public StringPair(string Val1, string Val2)
        {
            this.Val1 = Val1;
            this.Val2 = Val2;
        }

        public StringPair(object Val1, object Val2)
        {
            this.Val1 = (string)Val1;
            this.Val2 = (string)Val2;
        }
    }
}
